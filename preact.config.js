//const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const SWPrecacheWebpackPlugin = require('sw-precache-webpack-plugin');
const preactCliSwPrecachePlugin = require('preact-cli-sw-precache');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
//const DotenvPlugin = require('webpack-dotenv-plugin');
//const LoaderOptionsPlugin = require('webpack/lib/LoaderOptionsPlugin.js');
//const CommonsChunkPlugin = require('webpack/lib/optimize/CommonsChunkPlugin');
//------------------------------------------------------------------------------
/**
 * Function that mutates original webpack config.
 * Supports asynchronous changes when promise is returned.
 *
 * @param {object} config - original webpack config.
 * @param {object} env - options passed to CLI.
 * @param {WebpackConfigHelpers} helpers - object with useful helpers when working with config.
 **/
export default function (config, env, helpers) {
	// // https://stackoverflow.com/questions/45742982/set-base-url-for-preact-cli
	// config.output.publicPath = '/relativepath/';
	// // use the public path in your app as 'process.env.PUBLIC_PATH'
	// config.plugins.push(
	// 	new helpers.webpack.DefinePlugin({
	// 		'process.env.PUBLIC_PATH': JSON.stringify(config.output.publicPath || '/')
	// 	})
	// );

	// {
	// 	let { rule } = helpers.getLoadersByName(config, 'babel-loader')[0];
	// 	let babelConfig = rule.options;
	// 	console.log(babelConfig);
	// 	console.log(babelConfig.presets[0]);
	// }
	{
		let container = helpers.getPluginsByName(config, 'UglifyJsPlugin')[0];
		if (container) {
			let { plugin } = container;
			//plugin.options.mangle = false;
			//plugin.options.sourceMap = false;
			//plugin.options.compress = {};
			//console.log(plugin.options);
		}
	}

	const precacheConfig = {
		verbose: true,
		minify: true,
		maximumFileSizeToCacheInBytes: 16 * 1024 * 1024,
		//importScripts: [
		//	'assets/sw-runtime.js'
		//],
		staticFileGlobs: [],
		//staticFileGlobsIgnorePatterns: [/\.map$/], // use this to ignore sourcemap files
		runtimeCaching: [
			{
				urlPattern: /^https:\/\/shintorg48.ru\/mpreorders\/api\/backend/,
				// need patched sw-toolbox: cd node_modules/sw-toolbox && patch -p1 -i ./src/patches/sw-toolbox.patch
				handler: 'cacheFirst',
				options: {
					debug: false,
					networkTimeoutSeconds: 30,
					cache: {
						maxEntries: 1000000,
						maxAgeSeconds: 86400 * 7,
						name: 'backend-cache'
					}
				}
			},
			{
				urlPattern: /^https*:\/\/(.*)\/(.*)\/hs\/preact/,
				// need patched sw-toolbox: cd node_modules/sw-toolbox && patch -p1 -i ./src/patches/sw-toolbox.patch
				handler: 'cacheFirst',
				options: {
					debug: true,
					networkTimeoutSeconds: 30,
					cache: {
						maxEntries: 1000000,
						maxAgeSeconds: 86400 * 7,
						name: 'local-backend-cache'
					}
				}
			}/*,
			{
				urlPattern: /^.*$/,// /^\/(.*)(\.js|\.json|\.css|\.ico|\.html|\.map)$/i,
				handler: 'fastest',
				options: {
					debug: false,
					cache: {
						maxEntries: 1000,
						maxAgeSeconds: 86400,
						name: 'runtime-cache'
					}
				}
			}*/
		]
	};

	let cfg = preactCliSwPrecachePlugin(config, precacheConfig);

	// avoid error: Can't resolve 'fs'
	cfg.node.fs = 'empty';

	if (cfg.performance) {
		cfg.performance.maxAssetSize = 2000000;
		cfg.performance.maxEntrypointSize = 2000000;
	}

	return cfg;
}
