# !/bin/bash
# https://deliciousbrains.com/ssl-certificate-authority-for-local-https-development/
# Creating CA-Signed Certificates for Your Dev Sites

host=srv-terminal.shintorg48.local
openssl=/usr/bin/openssl
subj="/C=RU/ST=Lipetsk/L=Lipetsk/O=SHINTORG48/OU=IT Department/CN=shintorg48.local"

${openssl} genrsa -des3 -out myCA.key 2048 -config openssl.cnf
${openssl} req -x509 -new -nodes -key myCA.key -sha256 -days 3650 -out myCA.pem -config openssl.cnf

${openssl} genrsa -out ${host}.key 2048 -subj "${subj}" -config openssl.cnf
${openssl} req -new -key ${host}.key -out ${host}.csr -subj "${subj}" -config openssl.cnf
${openssl} x509 -req -in ${host}.csr -CA myCA.pem -CAkey myCA.key -CAcreateserial \
	-out ${host}.crt -days 3650 -sha256 -extfile ${host}.ext

exit 0
