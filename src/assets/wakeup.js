//------------------------------------------------------------------------------
/*eslint no-var:*/
/*eslint func-style:*/
/*eslint no-array-constructor:*/
//------------------------------------------------------------------------------
(function () {
	var openRequest = indexedDB.open('memstate', 1);
	openRequest.onerror = window.__initialize;
	openRequest.onsuccess = function (event) {
		var database = event.target.result, tx;

		try {
			tx = database.transaction('state_tree', 'readonly');
		}
		catch (e) {
			// One of the specified object stores was not found.
			if (e instanceof DOMException && e.name === 'NotFoundError') {
				window.__initialize();
				return;
			}
		}

		var store = tx.objectStore('state_tree');

		if (store.getAll) {
			store.getAll().onsuccess = function (event) {
				for (const row of event.target.result)
					if (row.parent === 1 && row.name === 'backend')
						window.__backend_url = row.value;

				window.__state_tree = {
					rows: event.target.result,
					exclude: [
						'header.spinner'
					]
				};
				database.close();
				window.__initialize();
			};
		}
		else {
			var state = [];
			store.openCursor().onsuccess = function (event) {
				var cursor = event.target.result;

				if (cursor) {
					// cursor.value contains the current record being iterated through
					// this is where you'd do something with the result
					var row = cursor.value;

					if (row.parent === 1 && row.name === 'backend')
						window.__backend_url = row.value;

					state.push(row);

					cursor.continue();
				}
				else {
					// no more results
					window.__state_tree = {
						rows: state,
						exclude: [
							'header.spinner'
						]
					};
					database.close();
					window.__initialize();
				}
			};
		}
	};

	openRequest.onupgradeneeded = function (event) {
		const db = openRequest.result;

		if (event.oldVersion < 1) {
			// Version 1 is the first version of the database.
			db.createObjectStore('state_tree', { keyPath: 'pkey' });
		}
	};
})();
//------------------------------------------------------------------------------
