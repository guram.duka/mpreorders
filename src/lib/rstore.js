//------------------------------------------------------------------------------
import Deque from 'double-ended-queue';
import setZeroTimeout from './zerotimeout';
import RedBlackTree from './rbt';
import { copy, equal } from './util';
import { newEnum } from './enum';
import root from './root';
//------------------------------------------------------------------------------
const LogActions = newEnum({ PUT: 1, DELETE: 2 });
//------------------------------------------------------------------------------
const collator = new Intl.Collator();
//------------------------------------------------------------------------------
function compareByParent(a, b) {
	return a.parent - b.parent;
}
//------------------------------------------------------------------------------
function compareByParentAndName(a, b) {
	let c = a.parent - b.parent;

	if (c === 0) {
		if (Number.isFinite(a) || Number.isFinite(b))
			c = a.name - b.name;
		else
			c = collator.compare(a.name, b.name);
	}

	return c;
}
//------------------------------------------------------------------------------
function transformPath(path) {
	if (Array.isArray(path)) {
		path = Array.from(path);
	}
	else if (path && (path.constructor === String || path instanceof String)) {
		const s = path.split('.');
		path = s.length === 0 ? (path.length === 0 ? [] : [path]) : s;
	}
	else
		path = [];

	for (let i = path.length - 1; i >= 0; i--) {
		const n = +path[i];

		if (!Number.isNaN(n))
			path[i] = n;

		const p = path[i];

		if (p === null || p === undefined ||
			!(
				Number.isInteger(p)
				|| p.constructor === String
				|| p instanceof String
			)
		)
			throw new Error('Invalid path entry');
	}

	if (path.length === 0)
		throw new Error('Invalid path');

	return path;
}
//------------------------------------------------------------------------------
function pullState() {
	let pkey = 0;
	const tree = new RedBlackTree(compareByParentAndName);
	const treeMap = new Map();
	let deletions;

	if (root.__state_tree) {
		const { rows, exclude } = root.__state_tree;

		if (rows && rows.length !== 0) {
			for (const object of rows) {
				const { value } = object;

				//delete object.timestamp;
				delete object.value;

				tree.set(object, value);
				
				let pSet = treeMap.get(object.parent);

				if (pSet === undefined)
					treeMap.set(object.parent, pSet = new Set());

				pSet.add(object.name);

				if (object.pkey > pkey)
					pkey = object.pkey;
			}

		}

		delete root.__state_tree;
		deletions = exclude;
	}

	return [tree, treeMap, pkey + 1, deletions];
}
//------------------------------------------------------------------------------
function getDb(obj, callback) {
	if (obj.__db) {
		callback(obj.__db);
	}
	else {
		const request = indexedDB.open('memstate', 1); // Request version 1.

		request.onerror = function (event) {
			root.console.log('Why didn\'t you allow my web app to use IndexedDB?!');
		};

		request.onsuccess = event => {
			obj.__db = event.target.result;
			obj.__db.onerror = event => {
				// Generic error handler for all errors targeted at this database's
				// requests!
				root.console.log('Database error: ' + event.target.errorCode);
			};

			callback(obj.__db);
		};

		request.onupgradeneeded = event => {
			obj.__db = request.result;

			if (event.oldVersion < 1) {
				// Version 1 is the first version of the database.
				this.__db.createObjectStore('state_tree', { keyPath: 'pkey' });
				//store.createIndex('by_parent', 'parent', { unique: false, autoIncrement: false });
			}
		};
	}
}
//------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------------------
class State {
	constructor() {
		this.__actions = new Deque(16);
		this.__topics = new Deque(16);
		this.__subscribers = [];
		this.__subscribersBindings = new Map();

		[this.__tree, this.__treeMap, this.__nextPKey, this.__nostore] = pullState();

		if (this.__nostore) {
			for (const path of this.__nostore)
				this.delete(path, true, 0);
			delete this.__nostore;
		}
	}

	__performNodeActions = () => this.__pushState()
	__pushState(force) {
		const actions = [];
		// with large updates, allow the interface to be responsive
		const maxActions = 100;

		while (!this.__actions.isEmpty() && (actions.length < maxActions || force))
			actions.push(this.__actions.shift());

		if (this.__actions.isEmpty())
			delete this.__performingNodeActions;
		else
			// allow processing interface messages
			setZeroTimeout(this.__performNodeActions);

		getDb(this, db => {
			const tx = db.transaction('state_tree', 'readwrite');
			const store = tx.objectStore('state_tree');

			for (const { action, key, value } of actions)
				switch (action) {
					case LogActions.PUT:
						store.put(Object.assign(
							{
								value
								//timestamp: (new Date()).getTime()
							},
							key
						));
						break;
					case LogActions.DELETE:
						store.delete(IDBKeyRange.only(key.pkey));
						break;
					default:
						throw new Error('Invalid action');
				}

			// tx.oncomplete = () => {
			// 	// All requests have succeeded and the transaction has committed.
			// };
		});
	}

	__scheduleNodeAction(node, deletion) {
		const entry = {
			action: deletion ? LogActions.DELETE : LogActions.PUT,
			key: node.key
		};

		if (!deletion)
			entry.value = copy(node.value);

		this.__actions.push(entry);

		if (!this.__performingNodeActions) {
			setZeroTimeout(this.__performNodeActions);
			this.__performingNodeActions = true;
		}

		return this;
	}

	__createNode(key) {
		this.__tree.set(key);
		const node = this.__tree.__search(key);
		this.__scheduleNodeAction(node);
		this.__nextPKey++;

		let pSet = this.__treeMap.get(key.parent);

		if (pSet === undefined)
			this.__treeMap.set(key.parent, pSet = new Set());

		pSet.add(key.name);

		return node;
	}

	__getVPath(path, create = true) {
		let key = { pkey: this.__nextPKey, parent: 0, name: '' };
		let node = this.__tree.__search(key);

		// only if root node not created
		if (node) {
			key = node.key;
		}
		else if (create) {
			node = this.__createNode(key);
		}
		else
			return undefined;

		for (const k of path) {
			key = { pkey: this.__nextPKey, parent: key.pkey, name: k };
			node = this.__tree.__search(key);

			if (node) {
				key = node.key;
			}
			else if (create) {
				node = this.__createNode(key);
			}
			else
				return undefined;
		}

		return node;
	}

	set(path, value, pubLevels = 1, equ = equal) {
		path = transformPath(path);
		const node = this.__getVPath(path);

		if (equ(node.value, value))
			return this;

		node.value = value;
		return this.__scheduleNodeAction(node).__pub(path, pubLevels);
	}

	replace(path, value, pubLevels = 1) {
		path = transformPath(path);
		const node = this.__getVPath(path);

		node.value = value;

		return this.__scheduleNodeAction(node).__pub(path, pubLevels);
	}

	transform(path, transformator, pubLevels = 1) {
		path = transformPath(path);
		const node = this.__getVPath(path);

		node.value = transformator(node.value);

		return this.__scheduleNodeAction(node).__pub(path, pubLevels);
	}

	delete(path, deep = true, pubLevels = 1) {
		path = transformPath(path);
		const node = this.__getVPath(path, false);

		if (node) {
			this.__scheduleNodeAction(node, true);
			const { key } = node;

			if (deep) {
				let pSet = this.__treeMap.get(key.pkey);

				if (pSet)
					for (const n of Array.from(pSet))
						this.delete(path.concat(n), deep, pubLevels);
			}

			const deleted = this.__tree.delete(key);

			if (!deleted)
				throw new Error('Undefined behavior');

			let pSet = this.__treeMap.get(key.parent);

			if (pSet === undefined)
				throw new Error('Undefined behavior');

			pSet.delete(key.name);

			if (pSet.size === 0)
				this.__treeMap.delete(key.parent);
		
			return this.__pub(path, pubLevels);
		}

		return this;
	}

	undef(path, value, pubLevels = 1) {
		path = transformPath(path);
		const node = this.__getVPath(path, value !== undefined);

		if (node) {
			if (value !== undefined) {
				if (node.value !== value) {
					node.value = value;
					return this.__scheduleNodeAction(node).__pub(path, pubLevels);
				}
			}
			else {
				this.__scheduleNodeAction(node, true);
				const deleted = this.__tree.delete(node.key);

				if (!deleted)
					throw new Error('Undefined behavior');

				return this.__pub(path, pubLevels);
			}
		}

		return this;
	}

	toggle(path, pubLevels = 1) {
		path = transformPath(path);
		const node = this.__getVPath(path);

		node.value = !node.value;
		return this.__scheduleNodeAction(node).__pub(path, pubLevels);
	}

	inc(path, value = 1, pubLevels = 1) {
		path = transformPath(path);
		const node = this.__getVPath(path);

		node.value = ~~node.value + value;
		return this.__scheduleNodeAction(node).__pub(path, pubLevels);
	}

	dec(path, value = 1, pubLevels = 1) {
		path = transformPath(path);
		const node = this.__getVPath(path);

		node.value = ~~node.value - value;
		return this.__scheduleNodeAction(node).__pub(path, pubLevels);
	}

	has(path) {
		path = transformPath(path);
		const node = this.__getVPath(path, false);
		return node !== undefined;
	}

	get(path, defaultValue) {
		path = transformPath(path);
		const node = this.__getVPath(path, false);

		if (node !== undefined)
			return node.value;

		return defaultValue;
	}

	__pub(path, levels = 1) {
		let l = levels;

		while (l !== 0) {
			l--;
			this.__topics.push(path.slice(0, path.length - l).join('.'));
		}

		if (!this.__performingPublishing) {
			setZeroTimeout(this.__performPublishing);
			this.__performingPublishing = true;
		}

		return this;
	}

	pub(path, levels = 1) {
		if (levels < 0)
			throw new Error('Invalid value');

		if (levels === 0)
			return this;

		return this.__pub(transformPath(path), levels);
	}

	publish() {
		this.__publishing();
	}

	__performPublishing = () => this.__publishing()
	__publishing() {
		const topics = this.__topics.toArray();
		this.__topics.clear();

		for (const subscriber of this.__subscribers) {
			const object = this.__subscribersBindings.get(subscriber);

			if (object)
				subscriber.call(object, topics);
			else
				subscriber(topics);
		}

		delete this.__performingPublishing;
	}

	subscribe(subscriber, object) {
		if (this.__subscribersBindings.has(subscriber))
			throw new Error('Already subscribed');

		this.__subscribers.push(subscriber);

		if (object)
			this.__subscribersBindings.set(subscriber, object);
	}

	unsubscribe(subscriber) {
		if (!this.__subscribersBindings.has(subscriber))
			throw new Error('Not subscribed');

		this.__subscribers.splice(this.__subscribers.indexOf(subscriber), 1);
		this.__subscribersBindings.delete(subscriber);
	}

	__print(parent = { parent: 1 }, path, s = []) {
		const range = new RedBlackTree.KeyRange(parent);

		for (const [k, v] of this.__tree.entries(range, compareByParent)) {
			const p = (path ? path + '.' : '') + k.name;

			if (v !== undefined)
				s.push([p, v]);

			this.__print({ parent: k.pkey }, p, s);
		}

		return s;
	}
}
//------------------------------------------------------------------------------
const store = new State();
//------------------------------------------------------------------------------
export default store;
//------------------------------------------------------------------------------
