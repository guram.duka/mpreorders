//------------------------------------------------------------------------------
/*
 * This file is part of HTML Barcode SDK.
 *
 *
 * ConnectCode provides its HTML Barcode SDK under a dual license model designed
 * to meet the development and distribution needs of both commercial application
 * distributors and open source projects.
 *
 * For open source projects, please see the GNU GPL notice below.
 *
 * For Commercial Application Distributors (OEMs, ISVs and VARs),
 * please see <http://www.barcoderesource.com/duallicense.shtml> for more information.
 *
 *
 *
 *
 * GNU GPL v3.0 License
 *
 * HTML Barcode SDK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HTML Barcode SDK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Source: http://www.barcoderesource.com/opensource/ean13/csshtmlEAN13Barcode.html
 * Modified 25.08.2016: Guram Duka guram.duka@gmail.com
 * Modified 18.09.2018: Guram Duka guram.duka@gmail.com
 */
//------------------------------------------------------------------------------
import root from './root';
//------------------------------------------------------------------------------
const parityMap = [
	[0, 0, 0, 0, 0, 0],
	[0, 0, 1, 0, 1, 1],
	[0, 0, 1, 1, 0, 1],
	[0, 0, 1, 1, 1, 0],
	[0, 1, 0, 0, 1, 1],
	[0, 1, 1, 0, 0, 1],
	[0, 1, 1, 1, 0, 0],
	[0, 1, 0, 1, 0, 1],
	[0, 1, 0, 1, 1, 0],
	[0, 1, 1, 0, 1, 0]
];
//------------------------------------------------------------------------------
const pattern = function () {
	const a = new Array(121);

	for (let i = 0; i < a.length; i++)
		a[i] = '';

	a[45] = 'wbwbw';
	a[48] = 'wwwbbwb';
	a[49] = 'wwbbwwb';
	a[50] = 'wwbwwbb';
	a[51] = 'wbbbbwb';
	a[52] = 'wbwwwbb';
	a[53] = 'wbbwwwb';
	a[54] = 'wbwbbbb';
	a[55] = 'wbbbwbb';
	a[56] = 'wbbwbbb';
	a[57] = 'wwwbwbb';
	a[111] = 'wbwwbbb';
	a[112] = 'wbbwwbb';
	a[113] = 'wwbbwbb';
	a[114] = 'wbwwwwb';
	a[115] = 'wwbbbwb';
	a[116] = 'wbbbwwb';
	a[117] = 'wwwwbwb';
	a[118] = 'wwbwwwb';
	a[119] = 'wwwbwwb';
	a[120] = 'wwbwbbb';
	a[97] = 'bbbwwbw';
	a[98] = 'bbwwbbw';
	a[99] = 'bbwbbww';
	a[100] = 'bwwwwbw';
	a[101] = 'bwbbbww';
	a[102] = 'bwwbbbw';
	a[103] = 'bwbwwww';
	a[104] = 'bwwwbww';
	a[105] = 'bwwbwww';
	a[106] = 'bbbwbww';
	a[91] = 'bwb';
	a[93] = 'bwb';

	return a;
}();
//------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------------------
export default class BarcodeEAN13Render {
	constructor(params) {
		const def = {
			humanReadable: true,
			units: 'cm',
			minBarWidth: 0,
			width: 6.35,
			height: 2.54,
			textLocation: 'bottom',
			textAlignment: 'center',
			textStyle: '',
			color: 'black',
			background: 'white',
			mode: 'html',
			attributes: ''
		};

		const pKeys = new Set(Object.keys(params));

		for (const [k, v] of Object.entries(def))
			this[`_${k}`] = pKeys.has(k) ? params[k] : v;

		this._checkParams();
	}

	_2cm(v) {
		return this._units === 'in' ? v * 2.54 : v;
	}

	_2in(v) {
		return this._units === 'in' ? v : v / 2.54;
	}

	_checkParams() {
		const {
			_humanReadable,
			_units,
			_minBarWidth,
			_width,
			_height,
			_textLocation,
			_textAlignment,
			_mode
		} = this;

		if (_textLocation !== 'bottom' && _textLocation !== 'top')
			this._textLocation = 'bottom';

		if (_textAlignment !== 'center' && _textAlignment !== 'left' && _textAlignment !== 'right')
			this._textAlignment = 'center';

		if (_units !== 'in' && _units !== 'cm')
			this._units = 'cm';

		if (_height <= 0 || this._2cm(_height) > 38.1)
			this._height = 2.54;

		if (_width <= 0 || this._2cm(_width) > 38.1)
			this._width = 6.35;

		if (_minBarWidth < 0 || this._2cm(_minBarWidth) > 5.08)
			this._minBarWidth = 0;

		if (_humanReadable !== true && _humanReadable !== false)
			this._humanReadable = true;

		if (_mode !== 'html' && _mode !== 'text')
			this._mode = 'html';
	}

	_filterInput(data) {
		let result = '';

		for (let i = 0; i < data.length; i++)
			if (data.charCodeAt(i) >= 48 && data.charCodeAt(i) <= 57)
				result += data.substr(i, 1);

		return result;
	}

	_generateCheckDigit(data) {
		let sum = 0;

		for (let i = data.length - 1; i >= 0; i--) {
			let barcodeValue = data.charCodeAt(i) - 48;
			sum += (i % 2) === 0 ? barcodeValue : barcodeValue * 3;
		}

		let result = sum % 10;

		if (result !== 0)
			result = 10 - result;

		return String.fromCharCode(result + 48);
	}

	_htmlEscape(data) {
		let result = '';

		for (let i = 0; i < data.length; i++)
			result += '&#' + data.charCodeAt(i).toString() + ';';

		return result;
	}

	_htmlDecode(str) {
		let ta = root.document.createElement('textarea');
		ta.innerHTML = str.replace(/</g, '&lt;').replace(/>/g, '&gt;');
		return ta.value;
	}

	_connectCodeEncode(data, hr) {
		let filteredData = this._filterInput(data);

		if (filteredData.length > 12)
			filteredData = filteredData.substr(0, 12);

		if (filteredData.length < 12)
			for (let i = 12 - filteredData.length; i > 0; i--)
				filteredData = '0' + filteredData;

		filteredData += this._generateCheckDigit(filteredData);

		let parityBit = 0;
		let firstDigit = 0;
		let transformDataLeft = '';

		for (let i = 0; i < 7; i++) {
			if (i === 0) {
				firstDigit = filteredData.charCodeAt(i) - 48;
			}
			else {
				parityBit = parityMap[firstDigit][i - 1];

				if (parityBit === 0)
					transformDataLeft += filteredData.substr(i, 1);
				else
					transformDataLeft += String.fromCharCode(filteredData.charCodeAt(i) + 49 + 14);
			}
		}

		let result = '';
		let transformDataRight = '';
		let transformChar = '';

		for (let i = 7; i < 13; i++) {
			transformChar = String.fromCharCode(filteredData.charCodeAt(i) + 49);
			transformDataRight += transformChar;
		}

		if (hr !== 0) {
			result = String.fromCharCode(firstDigit + '!'.charCodeAt(0)) + '[' + transformDataLeft + '-' + transformDataRight + ']';
		}
		else {
			result = '[' + transformDataLeft + '-' + transformDataRight + ']';
		}

		return this._htmlDecode(this._htmlEscape(result));
	}

	_encode(data) {
		let fontOutput = this._connectCodeEncode(data, 0);
		let output = '';

		for (let i = 0; i < fontOutput.length; i++) {
			let c = fontOutput.substr(i, 1).charCodeAt(0);
			output += c < pattern.length ? pattern[c] : '';
		}

		return output;
	}

	_getHumanText(data) {
		let filteredData = this._filterInput(data);

		if (filteredData.length > 12)
			filteredData = filteredData.substr(0, 12);

		if (filteredData.length < 12)
			for (let i = 12 - filteredData.length; i > 0; i--)
				filteredData = '0' + filteredData;

		filteredData += this._generateCheckDigit(filteredData);

		return this._htmlDecode(this._htmlEscape(filteredData));
	}

	render(data) {
		const encodedData = this._encode(data);
		// let thin_length = 0;
		// let thick_length = 0.0;
		// let incrementWidth = 0.0;
		// let swing = 1;
		let result = '';
		let barWidth = 0;
		let width = this._width;
		// let thick_width = 0.0;
		// let svg;

		let encodedLength = encodedData.length;
		let totalLength = encodedLength;

		if (this._minBarWidth > 0) {
			barWidth = this._minBarWidth.toFixed(2);
			width = barWidth * totalLength;
		}
		else {
			barWidth = (width / totalLength).toFixed(2);
		}

		const getHumanSpan = data => {
			let s = this._getHumanText(data);
			let fontSize = 1.141;
			let textTop = 0.870;
			let k = width / 6.35;

			fontSize *= k;
			textTop *= k;

			if (this._textLocation_ === 'bottom')
				textTop = -textTop;

			let fontStyle = `font-family: arial; font-size: ${fontSize}em; font-weight: normal; font-style: normal; font-stretch: normal`;
			let humanReadableText = '';
			let space = `<font style="${fontStyle}; width: auto; height: auto; background: transparent">&nbsp;&nbsp;</font>`;
			let digit = `<font style="${fontStyle}; width: auto; height: auto; background: ${this._background}">`;

			humanReadableText += `${digit}${s.substr(0, 1)}</font>`;
			humanReadableText += `${space}${space}`;
			humanReadableText += `${digit}${s.substr(1, 6)}</font>`;
			humanReadableText += `${space}${space}`;
			humanReadableText += `${digit}${s.substr(7, 6)}</font>`;
			humanReadableText += `${space}${space}`;

			let humanTextStyle = `z-index: 200; position: relative; top: ${textTop}em; width: auto; height: auto; ${fontStyle}; background: transparent; ${this._textStyle}`;

			return `<span human_readable_text style="${humanTextStyle}">${humanReadableText}</span><br />`;
		};

		if (this._mode === 'html') {
			let attributes = `barcode="${data}"`;

			if (this._attributes.length !== 0 && this._attributes.trim())
				attributes += ' ' + this._attributes.trim();

			if (this._textAlignment === 'center')
				result = `<div ${attributes} style="text-align: center">`;
			else if (this._textAlignment === 'left')
				result = `<div ${attributes} style="text-align: left">`;
			else if (this._textAlignment === 'right')
				result = `<div ${attributes} style="text-align: right">`;

			let humanSpan = '';

			if (this._humanReadable === true && this._textLocation === 'top')
				humanSpan = `${getHumanSpan(data)}<br />`;

			result += humanSpan;
		}

		let masterStyle = 'display: inline-block; width: auto; z-index: 100; background: transparent';

		for (let i = 0; i < encodedData.length; i++) {
			let brush = encodedData.substr(i, 1) === 'b' ? this._color : this._background;

			if (this._mode === 'html')
				result += `<span vbar style="border-left: ${barWidth}${this._units} solid ${brush}; height: ${this._height}${this._units}; ${masterStyle}"></span>`;

			//incrementWidth += barWidth;
		}

		if (this._mode === 'html') {
			let humanSpan = '';

			if (this._humanReadable === true && this._textLocation === 'bottom')
				humanSpan = `<br />${getHumanSpan(data)}`;

			result += humanSpan + '</div>';
		}

		return result;
	}
}
//------------------------------------------------------------------------------
