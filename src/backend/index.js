//------------------------------------------------------------------------------
//import AbortController from 'abortcontroller-polyfill';
import root from '../lib/root';
import store from '../lib/rstore';
import {
	encodeURIParameter,
	serializeURIParams
} from '../lib/util';
import { stringify, destringify } from '../lib/json';
//------------------------------------------------------------------------------
// nginx proxy configuration
// proxy_cache_path /var/cache/nginx/ram use_temp_path=off keys_zone=ram:4M inactive=1d max_size=1024M;

// upstream mpreorders_backend_upstream {
// 	keepalive 8;
// 	server 31.210.212.158:65480 weight=1;
// 	server 213.109.6.191:65480 weight=5;
// 	#server 178.210.36.54:65480;
// }

// location ~ ^/mpreorders/api/backend {

// 	rewrite ^/mpreorders/api/backend(.*)$ /opt/hs/preact$1 break;
// 	proxy_pass http://mpreorders_backend_upstream;

// 	keepalive_disable msie6;
// 	keepalive_requests 1000;
// 	keepalive_timeout 600;

// 	proxy_set_header Host $host;
// 	proxy_set_header X-Real-IP $remote_addr;
// 	proxy_set_header X-Real-Port $remote_port;
// 	proxy_set_header X-Server-Addr $server_addr;
// 	proxy_set_header X-Server-Name $server_name;
// 	proxy_set_header X-Server-Port $server_port;

// 	# https://nginx.ru/ru/docs/http/ngx_http_upstream_module.html#keepalive
// 	proxy_http_version 1.1;
// 	#proxy_set_header Connection "Keep-Alive";
// 	proxy_set_header Connection "";
// 	proxy_read_timeout     600;
// 	proxy_connect_timeout  600;

// 	proxy_redirect off;
// 	proxy_buffering on;
// 	sendfile on;
// 	tcp_nopush on;
// 	proxy_cache_lock on;
// 	proxy_cache_lock_timeout 1h;
// 	proxy_cache_use_stale updating;
// 	proxy_cache_methods GET HEAD;
// 	proxy_cache ram;
// 	proxy_cache_key "$proxy_host$uri$is_args$args";
// 	proxy_cache_revalidate off;
// 	proxy_max_temp_file_size 4m;

// 	#gzip               off;
// 	#gzip_http_version  1.0;
// 	#gzip_vary          on;
// 	#gzip_min_length    512;
// 	#gzip_comp_level    3;
// 	#gzip_proxied       any;
// 	#gzip_types         text/plain text/css text/javascript application/javascript application/json application/x-javascript text/xml application/xml application/xml+rss;

// 	#proxy_set_header 'Service-Worker-Allowed' '/';
// 	#add_header 'Service-Worker-Allowed' '/';
// 	#add_header 'Access-Control-Allow-Origin' '*';
// 	#proxy_set_header 'Access-Control-Allow-Origin' '*';
// }
//------------------------------------------------------------------------------
//const BACKEND_URL = 'http://31.210.212.158:65480/opt/hs/preact';
//const BACKEND_URL = 'http://srv-terminal:65480/roznbaza/hs/preact';
const BACKEND_URL = root.__backend_url
	? root.__backend_url
	: 'https://www.shintorg48.ru/mpreorders/api/backend';
//------------------------------------------------------------------------------
const mustRefreshUrlsItemName = 'must-refresh-urls';
const mustRefreshUrls = (() => {
	const u = root.localStorage
		&& root.localStorage.getItem(mustRefreshUrlsItemName);

	if (u)
		return destringify(u);

	return {};
})();
//------------------------------------------------------------------------------
function removeExpiredMustRefreshUrls() {
	if (!root.localStorage)
		return;

	const now = Date.now();
	const keys = Object.keys(mustRefreshUrls);

	for (const k of keys)
		if (mustRefreshUrls[k].t <= now)
			delete mustRefreshUrls[k];

	const k = Object.keys(mustRefreshUrls);

	if (k.length === 0)
		root.localStorage.removeItem(mustRefreshUrlsItemName);
	else if (k.length !== keys.length)
		root.localStorage.setItem(
			mustRefreshUrlsItemName,
			stringify(mustRefreshUrls));
}
//------------------------------------------------------------------------------
const ma0 = /max-age/gi;
const ma1 = /max-age|[= ]/gi;
//------------------------------------------------------------------------------
function getMaxAge(opts) {
	let xMaxAge = opts.responseHeaders.get('cache-control');

	if (xMaxAge) {
		xMaxAge = xMaxAge.split(',').find(v => v.match(ma0));
		if (xMaxAge) {
			xMaxAge = xMaxAge.replace(ma1, '');
			if (xMaxAge)
				xMaxAge = ~~xMaxAge; // fast convert string to integer
		}
	}

	return xMaxAge;
}
//------------------------------------------------------------------------------
export function burl(opts) {
	if (opts.method === undefined)
		opts.method = 'GET';

	if (opts.credentials === undefined)
		opts.credentials = 'omit';

	if (opts.mode === undefined)
		opts.mode = 'cors';//BACKEND_URL.toLowerCase().startsWith('https') ? 'cors' : 'no-cors';

	if (opts.cache === undefined)
		opts.cache = 'default';

	const r = {};

	// // antipattern, but only as an exception and it is the fastest method
	// const store = getStore();

	// // send auth data
	// const auth = store.getIn('auth');

	// need for caching authorized request separate from regular not authorized
	if (opts.r)
		r.r = opts.r;

	if (opts.rmod)
		opts.rmod(r);

	opts.url = BACKEND_URL;

	// resource cached by key of unique url and headers
	if (opts.method === 'GET') {
		let u = '';

		// if (opts.a === '')
		// 	u += auth && auth.link ? '&' + encodeURIParameter('a', auth.link) : '';
		// else
		if (opts.a !== undefined)
			u += '&' + encodeURIParameter('a', opts.a);

		// if (opts.e === '')
		// 	u += auth && auth.employee ? '&' + encodeURIParameter('e', auth.employee) : '';
		// else
		if (opts.e !== undefined)
			u += '&' + encodeURIParameter('e', opts.e);

		removeExpiredMustRefreshUrls();

		opts.url += '?' + serializeURIParams(r);
		// special handling for data updated in previous requests, need fetch fresh data
		const refresh = mustRefreshUrls[opts.url];

		if (refresh !== undefined)
			u += '&u=' + refresh.u;

		if (u.length !== 0)
			opts.u = u;

		if (opts.auth && opts.auth.authorized && opts.a !== undefined) {
			if (!opts.headers)
				opts.headers = new Headers();

			opts.headers.append('X-Access-Data', opts.auth.link + ', ' + opts.auth.hash);
		}
	}
	else if (opts.method === 'PUT') {
		opts.body = JSON.stringify(r.r);

		if (opts.auth && opts.auth.authorized) {
			if (!opts.headers)
				opts.headers = new Headers();

			opts.headers.append('X-Access-Data', opts.auth.link + ', ' + opts.auth.hash);
		}
	}

	//if (headers && !headers.entries().next().done)

	try {
		opts.controller = new root.AbortController();
	}
	catch (e) {
		opts.controller = { abort: () => true };
	}
	
	opts.signal = opts.controller.signal;

	return opts.url;
}
//------------------------------------------------------------------------------
export function bfetch(opts, success, fail, start) {
	let url = burl(opts);

	if (opts.u)
		url += opts.u;

	opts.promise = fetch(url, opts).then(response => {
		const contentType = response.headers.get('content-type');
		// check if access denied
		// if (!opts.noauth && opts.r && (opts.r.a || opts.r.e)) {
		// 	let xaLink = response.headers.get('x-access-data'), xaEmployee;

		// 	if (xaLink) {
		// 		[xaLink, xaEmployee] = xaLink.split(',');
		// 		xaLink = xaLink.length !== 0 ? xaLink.trim() : undefined;

		// 		if (xaEmployee !== undefined)
		// 			xaEmployee = xaEmployee.trim();
		// 	}

		// 	if (xaLink === null)
		// 		xaLink = undefined;

		// 	// antipattern, but only as an exception and it is the fastest method
		// 	const auth = getStore().getIn('auth');

		// 	if (!auth || auth.link !== xaLink || auth.employee !== xaEmployee)
		// 		disp(store => store.deleteIn('auth.authorized', 2));
		// }

		opts.responseHeaders = response.headers;

		if (response.ok && contentType) {
			if (contentType.includes('application/json'))
				return response.json();
			if (contentType.includes('text/'))
				return response.text();
			if (contentType.includes('image/'))
				return opts.blob
					? response.blob()
					: response.arrayBuffer();
		}

		// will be caught below
		throw new TypeError('Oops, we haven\'t right type of response! Status: '
			+ response.status + ', ' + response.statusText + ', content-type: ' + contentType);
	}).then(result => {
		if (result === undefined || result === null ||
			!(result.constructor === Object || result instanceof Object
				|| Array.isArray(result)
				|| result.constructor === ArrayBuffer || result instanceof ArrayBuffer))
			throw new TypeError('Oops, we haven\'t got data! ' + result);

		const xDate = opts.responseHeaders.get('date');

		if (xDate)
			result.date = new Date(xDate);

		const xMaxAge = getMaxAge(opts);

		if (xMaxAge)
			result.maxAge = xMaxAge;

		const xEol = opts.responseHeaders.get('End-Of-Life');

		if (xEol) {
			result.endOfLife = new Date(xEol);
		}
		else if (xDate && xMaxAge) {
			const t = new Date(xDate);
			t.setTime(t.getTime() + xMaxAge * 1000);
			result.endOfLife = t;
		}

		if (opts.refreshUrls) {
			const now = Date.now();

			for (const [k, v] of Object.entries(opts.refreshUrls)) {
				let u = mustRefreshUrls[k];

				if (!u)
					mustRefreshUrls[k] = u = { u: 0 };

				u.u++;
				u.t = now + v * 1000; // seconds to miliseconds
			}

			root.localStorage.setItem(
				mustRefreshUrlsItemName,
				stringify(mustRefreshUrls));
		}

		if (result.errorCode === 401) {
			store.transform('auth', v => delete v.authorized);
			fail && fail(result.errorText, opts);
		}
		else {
			success && success(result, opts);
		}
	}).catch(error => {
		fail && fail(error, opts);
	});

	start && start(opts);

	return opts.promise;
}
//------------------------------------------------------------------------------
export function imgReq(...args) {
	const [arg0] = args;
	const r = { m: 'image' };

	if (arg0.constructor === Object || arg0 instanceof Object) {
		for (const k of Object.keys(arg0))
			if (arg0[k] !== undefined)
				r[k] = arg0[k];
	}
	else
		[r.u, r.w, r.h] = args;

	return { r };
}
//------------------------------------------------------------------------------
export function imgUrl(...args) {
	const [arg0] = args;
	const s = BACKEND_URL + '?';
	const r = {};

	if (arg0.constructor === Object || arg0 instanceof Object) {
		for (const k of Object.keys(arg0))
			r[k] = arg0[k];
	}
	else
		[r.u, r.w, r.h] = args;

	return s + serializeURIParams(imgReq(r));
}
//------------------------------------------------------------------------------
export function imgKey(...args) {
	const r = imgReq(...args).r;
	const { u, w, h } = r;

	delete r.m;
	delete r.u;
	delete r.w;
	delete r.h;

	let key = 'i' + u.replace(/-/g, '');

	if (w !== undefined)
		key += '_' + w;

	if (h !== undefined) {
		if (w === undefined)
			key += '_';
		key += 'x' + h;
	}

	for (const k of Object.keys(r))
		if (r[k] !== undefined)
			key += '_' + k + '_' + r[k];

	return key;
}
//------------------------------------------------------------------------------
