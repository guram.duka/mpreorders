//------------------------------------------------------------------------------
import { Component as PreactComponent } from 'preact';
import { equal } from '../../lib/util';
import store from '../../lib/rstore';
//------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------------------
export default class Component extends PreactComponent {
	// public

	// constructor(props, context) {
	// 	super(props, context);
	// }

	//if (force || !shallowEqual(this.state, state))

	setState(state, callback) {
		const { willSetState, didSetState } = this;
		const keys = {};

		for (const k of Object.keys(state))
			keys[k] = true;

		if (willSetState)
			willSetState.call(this, state, keys);

		if (didSetState) {
			const cb = callback;
			callback = cb
				? () => (didSetState.call(this, keys), cb())
				: () => didSetState.call(this, keys)
			;
		}

		return super.setState.call(this, state, callback);
	}

	// prior to removal from the DOM
	componentWillUnmount() {
		if (this.willUnmount)
			this.willUnmount();

		store.unsubscribe(this.__storeConfigSubscriber);

		if (this.storeSubscriber)
			store.unsubscribe(this.storeSubscriber);
	}

	__storeConfigMount(state) {
		let { storeConfig } = this;

		if (!storeConfig)
			return;

		if (storeConfig.constructor === Function || storeConfig instanceof Function)
			storeConfig = storeConfig.call(this);

		for (const e of storeConfig) {
			const { path, alias, owner, defaultValue } = e;

			if (Object.keys(e).indexOf('mount') !== -1) {
				let { levels, equ } = e;

				if (levels === undefined)
					levels = 1;
				if (equ === undefined)
					equ = equal;

				if (e.mount !== undefined)
					store.set(path, e.mount, levels, equ);
				else
					store.delete(path, levels);
			}

			if (alias) {
				const v = store.get(path, defaultValue);

				if (owner)
					owner[alias] = v;
				else
					state[alias] = v;
			}
		}
	}

	__storeConfigSubscriber = topics => {
		let { storeConfig } = this;

		if (!storeConfig)
			return;

		if (storeConfig.constructor === Function || storeConfig instanceof Function)
			storeConfig = storeConfig.call(this);

		if (storeConfig.length !== 0) {
			const state = {};

			for (const topic of topics)
				for (const { path, alias, owner, reaction } of storeConfig)
					if (path === topic && alias) {
						const v = store.get(topic);

						if (owner)
							owner[alias] = v;
						else
							state[alias] = v;

						if (reaction)
							reaction.call(this);
					}

			if (Object.keys(state).length !== 0)
				this.setState(state);
		}
	}

	// 	before the component gets mounted to the DOM
	componentWillMount() {
		if (this.decodeProps)
			this.decodeProps(this.props);

		// important order, first must be called this.__storeConfigSubscriber
		store.subscribe(this.__storeConfigSubscriber, this);
		this.__storeConfigMount(this.state);

		// important order, this.storeSubscriber must be called after this.__storeConfigSubscriber
		if (this.storeSubscriber)
			store.subscribe(this.storeSubscriber, this);

		if (this.willMount)
			this.willMount();
	}

	// after the component gets mounted to the DOM
	componentDidMount() {
		if (this.didMount)
			this.didMount(this.props, this.state);
	}

	// before new props get accepted
	componentWillReceiveProps(props, context) {
		if (this.willReceiveProps)
			this.willReceiveProps(props);
	}

	// before render(). Return false to skip render
	shouldComponentUpdate(props, state, context) {
		if (this.shouldUpdate)
			return this.shouldUpdate(props, state, context);

		return true;
	}

	// before render()
	componentWillUpdate(props, state, context) {
		if (this.decodeProps)
			this.decodeProps(props);

		this.__storeConfigMount(state);

		if (this.willUpdate)
			this.willUpdate(props, state, context);
	}

	// render(props, state, context) {
	// 	return super.render.call(this, props, state, context);
	// }

	// after render()
	componentDidUpdate(previousProps, previousState, previousContext) {
		if (this.didUpdate)
			this.didUpdate(previousProps, previousState, previousContext);
	}

	// mergeState(state, callback) {
	// 	if (shallowEqual(this.state, state))
	// 		return;

	// 	this.setState(state, callback);
	// }
}
//------------------------------------------------------------------------------
