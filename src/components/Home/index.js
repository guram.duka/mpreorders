//------------------------------------------------------------------------------
import Button from 'preact-material-components/Button';
import 'preact-material-components/Button/style.css';
import LayoutGrid from 'preact-material-components/LayoutGrid';
import 'preact-material-components/LayoutGrid/style.css';
import Component from '../Component';
import Vab from '../Material/VerticalActionBar';
import ProductCard from '../Products/Card';
import Divider from '../Material/Divider';
import { goCategory } from '../Categories/link';
import loader from './loader';
import style from './style.scss';
import {
	homeTitle,
	headerTitleStorePath,
	headerSearchStorePath,
	iconsLosslessStorePath,
	iconsQualityStorePath,
	kioskStorePath
} from '../../const';
import { imgUrl } from '../../backend';
import { plinkRoute } from '../../lib/util';
import version from '../../version';
//import { LogIn, LogOut, UserPlus } from 'preact-feather';
//import FaBeer from 'react-icons/fa/beer';
//------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------------------
export default class Home extends Component {
	storeConfig = [
		{ path: kioskStorePath, alias: 'kiosk' },
		{ path: iconsLosslessStorePath, alias: 'iconsLossless' },
		{ path: iconsQualityStorePath, alias: 'iconsQuality' },
		{ path: headerTitleStorePath, mount: homeTitle },
		{ path: headerSearchStorePath, mount: undefined },
		...['cat', 'top', 'rnd'].map(p => ({
			path: `home.${p}`,
			alias: p,
			defaultValue: { rows: [], style: style.invis }
		}))
	]

	willMount() {
		loader.call(this);
	}

	linkTo = path => ({ href: path, onClick: plinkRoute(path) })

	goCart = this.linkTo('/cart')

	render(props, state) {
		const { cat, top, rnd, kiosk } = state;
		const catsClasses = [style.container, cat.style].join(' ');
		let cats;

		if (kiosk)
			cats = (
				<div style={{ overflow: 'hidden', paddingTop: 4 }}>
					<div class={catsClasses} style={kiosk ? { marginBottom: '-16px' } : undefined}>
						{cat.rows.map(v => {
							if (!v || !v.name)
								return;

							return (
								<Button {...goCategory(v.link)} className={style.kioskCatButton}>
									{v.ico ? <img src={imgUrl({ u: v.ico, type: 'png' })} /> : undefined}
									{v.name}
								</Button>);
						})}
					</div>
				</div>);
		else
			cats = (
				<div class={catsClasses}>
					<Divider horizontal />
					{cat.rows.map(v => v ?
						<Button {...goCategory(v.link)}>
							{v.name}
						</Button>
						: <Divider inline vertical />
					)}
					<Divider horizontal />
				</div>);

		const { iconsLossless, iconsQuality } = state;

		return (
			<div class={style.home}>
				{cats}
				<div class={[style.hbar, rnd.style].join(' ')}>
					<Divider horizontal />
					<div>СЕЗОННЫЕ</div>
					<Divider horizontal />
				</div>
				<LayoutGrid class={rnd.style}>
					<LayoutGrid.Inner>
						{rnd.rows.map(row => (
							<LayoutGrid.Cell cols="2">
								<ProductCard mini data={row}
									iconLossless={iconsLossless}
									iconQuality={iconsQuality}
								/>
							</LayoutGrid.Cell>))}
					</LayoutGrid.Inner>
				</LayoutGrid>
				<div class={[style.hbar, top.style].join(' ')}>
					<Divider horizontal />
					<div>ТОП</div>
					<Divider horizontal />
				</div>
				<LayoutGrid class={top.style}>
					<LayoutGrid.Inner>
						{top.rows.map(row => (
							<LayoutGrid.Cell cols="2">
								<ProductCard mini data={row}
									iconLossless={iconsLossless}
									iconQuality={iconsQuality}
								/>
							</LayoutGrid.Cell>))}
					</LayoutGrid.Inner>
				</LayoutGrid>
				<Vab fixed>
					{kiosk ?
						<Vab.Fab {...this.goCart}>
							<Vab.Fab.Icon>
								shopping_cart
							</Vab.Fab.Icon>
						</Vab.Fab> : undefined}
					<Vab.ScrollUp />
				</Vab>
				{/*
				<FaBeer size={24} />
				<LogIn style={{ verticalAlign: 'middle' }} color="black" />
				<LogOut style={{ verticalAlign: 'middle' }} color="black" />
				<UserPlus style={{ verticalAlign: 'middle' }} color="black" />*/}
				<p className={style.v}>
					{version}
				</p>
			</div>);
	}
}
//------------------------------------------------------------------------------
