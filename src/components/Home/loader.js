//------------------------------------------------------------------------------
import store from '../../lib/rstore';
import { deepEqual } from '../../lib/util';
import { bfetch } from '../../backend';
import { successor, failer, starter } from '../load';
import categoriesLoader from '../Categories/loader';
//------------------------------------------------------------------------------
const opts = {
	r: {
		m: 'pkg',
		r: [
			{
				alias: 'top',
				m: 'category',
				f: 'top',
				r: { target: 'products' }
			},
			{
				alias: 'rnd',
				m: 'category',
				f: 'rnd',
				r: { target: 'products' }
			}
		]
	}
};
//------------------------------------------------------------------------------
export default function loader() {
	categoriesLoader.call(this,
		result => {
			const rows = [];
		
			for (const v of result.rows)
				rows.push(v, undefined);
		
			rows.pop();
			result.rows = rows;
			store.set('home.cat', result, 1, deepEqual);
		}
	);

	for (let i = 0; i < opts.r.r.length; i++) {
		const r = Object.assign({}, opts.r.r[i]);
		const { alias } = r;
		delete r.alias;

		if (this.state.kiosk) {
			if (alias === 'rnd')
				r.r.n = 24;
			else if (alias === 'top')
				r.r.n = 12;
		}

		bfetch(
			{ r },
			successor(result => store.set(`home.${alias}`, result, 1, deepEqual)),
			failer(),
			starter()
		);
	}
}
//------------------------------------------------------------------------------
