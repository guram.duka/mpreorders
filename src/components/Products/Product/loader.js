//------------------------------------------------------------------------------
import { burl, bfetch } from '../../../backend';
import { successor, failer, starter } from '../../load';
import { deepEqual } from '../../../lib/util';
import store from '../../../lib/rstore';
import { headerTitleStorePath } from '../../../const';
import { refreshUrls as favoritesRefreshUrls } from '../../Favorites/loader';
import { refreshUrls as cartRefreshUrls } from '../../Cart/loader';
//------------------------------------------------------------------------------
export const productsStorePath = 'products';
//------------------------------------------------------------------------------
function pullOpts() {
	const { props, state } = this;
	const { f, link } = props;
	const { auth } = state;
	const ae = f && auth.authorized && auth.employee ? true : undefined;
	return {
		auth,
		r: {
			m: 'dict',
			f: f ? f : 'object',
			r: {
				target: 'products',
				link
			}
		},
		a: ae,
		e: ae
	};
}
//------------------------------------------------------------------------------
export function pull() {
	const opts = pullOpts.call(this);

	return bfetch(
		opts,
		successor(result => {
			if (this.props.f) {
				store.set(this.storePath, result.rows, 1, deepEqual);
			}
			else {
				result = result.rows[0];
				store.set(headerTitleStorePath, result.name);
				store.set(this.storePath, result, 1, deepEqual);
				store.set(this.msgsStorePath, result.messages.rows, 1, deepEqual);
				store.set(this.avgRatingStorePath, result.avgRating);
			}
		}),
		failer(),
		starter()
	);
}
//------------------------------------------------------------------------------
function pullVabOpts() {
	const { props, state } = this;

	return {
		auth: state.auth,
		r: {
			m: 'cart',
			f: 'pull',
			r: { link: props.link }
		},
		a: state.auth.link
	};
}
//------------------------------------------------------------------------------
export function pullVab() {
	if (!this.state.auth.authorized)
		return;

	const opts = pullVabOpts.call(this);

	return bfetch(
		opts,
		successor(result => {
			this.refreshUrls = {
				...this.refreshUrls,
				[opts.url]: result.maxAge
			};
			store.set(this.inFavoritesStorePath, result.favorite);
			store.set(this.inCartStorePath, result.quantity);
		}),
		failer(),
		starter()
	);
}
//------------------------------------------------------------------------------
export function push(r) {
	if (this.pushId)
		return;

	const { props, state } = this;
	const opts = {
		auth: state.auth,
		refreshUrls: this.refreshUrls,
		method: 'PUT',
		r: {
			m: 'cart',
			f: 'push',
			r: {
				link: props.link,
				...r
			}
		}
	};

	if (r.favorite !== undefined)
		opts.refreshUrls = { ...opts.refreshUrls, ...favoritesRefreshUrls.call(this) };
	if (r.quantity !== undefined)
		opts.refreshUrls = { ...opts.refreshUrls, ...cartRefreshUrls.call(this) };

	this.pushId = bfetch(
		opts,
		successor(result => {
			store.set(this.inFavoritesStorePath, result.favorite);
			store.set(this.inCartStorePath, result.quantity);
			delete this.pushId;
		}),
		failer(error => {
			delete this.pushId;
			this.showError(error.message);
		}),
		starter()
	);
}
//------------------------------------------------------------------------------
function pullMsgsOpts() {
	const { props, state } = this;
	const opts = {
		auth: state.auth,
		r: {
			m: 'dict',
			f: 'pull_msgs',
			r: {
				target: 'products',
				link: props.link
			}
		},
		a: state.auth.link
	};

	const { msgs } = state;

	for (let i = msgs.length - 1; i >= 0; i-- )
		if (msgs[i] && msgs[i].time !== undefined) {
			opts.r.r.time = msgs[i].time;
			break;
		}

	return opts;
}
//------------------------------------------------------------------------------
export function pullMsgs() {
	const opts = pullMsgsOpts.call(this);

	return bfetch(
		opts,
		result => {
			this.refreshUrls = {
				...this.refreshUrls,
				[opts.url]: result.maxAge
			};

			if (result.rows.length !== 0) {
				const { msgs } = store.get(this.msgsStorePath, []);

				for (const row of result.rows)
					msgs.push(row);

				store.replace(this.msgsStorePath, msgs);
			}
		}
	);
}
//------------------------------------------------------------------------------
export function pushMsgOpts(f, r) {
	const { props, state } = this;
	const opts = {
		auth: state.auth,
		method: 'PUT',
		r: {
			m: 'dict',
			f,
			r: {
				target: 'products',
				link: props.link,
				...r
			}
		}
	};

	opts.refreshUrls = {
		[burl(pullOpts.call(this))]: 180,
		[burl(pullMsgsOpts.call(this))]: 60,
		...this.refreshUrls
	};

	return opts;
}
//------------------------------------------------------------------------------
export function pushMsg(message) {
	if (this.pushMsgId)
		return;

	const opts = pushMsgOpts.call(this, 'push_msg', { msg: message });

	this.pushMsgId = bfetch(
		opts,
		successor(result => {
			const msgs = store.get(this.msgsStorePath, []);

			msgs.push({ ...result.msg, msg: message });
			store.replace(this.msgsStorePath, msgs);
			delete this.pushMsgId;

			this.setState({ msg: undefined });
		}),
		failer(error => {
			delete this.pushMsgId;
			this.showError(error.message);
		}),
		starter()
	);
}
//------------------------------------------------------------------------------
export function deleteMsg(key) {
	if (this.pushMsgId)
		return;

	const opts = pushMsgOpts.call(this, 'delete_msg', { time: key });

	this.pushMsgId = bfetch(
		opts,
		successor(result => {
			const msgs = store.get(this.msgsStorePath, []);

			for (const [i, msg] of msgs.entries())
				if (msg && msg.time === key) {
					msgs[i] = undefined;
					break;
				}

			store.replace(this.msgsStorePath, msgs);
			delete this.pushMsgId;
		}),
		failer(error => {
			delete this.pushMsgId;
			this.showError(error.message);
		}),
		starter()
	);
}
//------------------------------------------------------------------------------
function pullRatingOpts() {
	const { props, state } = this;
	return {
		auth: state.auth,
		r: {
			m: 'dict',
			f: 'rating',
			r: {
				target: 'products',
				link: props.link
			}
		},
		a: state.auth.link
	};
}
//------------------------------------------------------------------------------
export function pullRating() {
	if (!this.state.auth.authorized)
		return;

	const opts = pullRatingOpts.call(this);

	return bfetch(
		opts,
		successor(result => {
			store.set(this.ratingStorePath, result.rating);
			store.set(this.avgRatingStorePath, result.avgRating);
		}),
		failer(),
		starter()
	);
}
//------------------------------------------------------------------------------
export function pushRatingOpts(r) {
	const { props, state } = this;
	const opts = {
		auth: state.auth,
		method: 'PUT',
		r: {
			m: 'dict',
			f: 'push_rating',
			r: {
				target: 'products',
				link: props.link,
				rating: r
			}
		}
	};

	opts.refreshUrls = {
		[burl(pullOpts.call(this))]: 180,
		[burl(pullRatingOpts.call(this))]: 60,
		...this.refreshUrls
	};

	return opts;
}
//------------------------------------------------------------------------------
export function pushRating(r) {
	const opts = pushRatingOpts.call(this, r);

	return bfetch(
		opts,
		successor(result => {
			store.set(this.ratingStorePath, result.rating);
			store.set(this.avgRatingStorePath, result.avgRating);
			this.setState({ isRatingExpanded: undefined });
		}),
		failer(error => this.showError(error.message)),
		starter()
	);
}
//------------------------------------------------------------------------------
export function refreshUrls() {
	return {
		[burl(pullVabOpts.call(this))]: 60
	};
}
//------------------------------------------------------------------------------
