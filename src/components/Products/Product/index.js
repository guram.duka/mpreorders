//------------------------------------------------------------------------------
import DOMPurify from 'dompurify';
import LayoutGrid from 'preact-material-components/LayoutGrid';
import 'preact-material-components/LayoutGrid/style.css';
import Icon from 'preact-material-components/Icon';
import 'preact-material-components/Icon/style.css';
import TextField from 'preact-material-components/TextField';
import 'preact-material-components/TextField/style.css';
import List from 'preact-material-components/List';
import 'preact-material-components/List/style.css';
import Elevation from '../../Material/Elevation';
import Chips from 'preact-material-components/Chips';
import 'preact-material-components/Chips/style.css';
import Typography from 'preact-material-components/Typography';
import 'preact-material-components/Typography/style.css';
import Snackbar from 'preact-material-components/Snackbar';
import 'preact-material-components/Snackbar/style.css';
import Component from '../../Component';
import Image from '../../Material/Image';
import Vab from '../../Material/VerticalActionBar';
import Divider from '../../Material/Divider';
import {
	headerTitleStorePath,
	headerSearchStorePath,
	iconsLosslessStorePath,
	iconsQualityStorePath,
	kioskStorePath
} from '../../../const';
import { prevent, plinkRoute, dateFormatter } from '../../../lib/util';
import {
	productsStorePath,
	pull,
	pullRating,
	pullVab,
	pullMsgs,
	push,
	pushMsg,
	deleteMsg,
	pushRating
} from './loader';
import store from '../../../lib/rstore';
import style from './style.scss';
//------------------------------------------------------------------------------
function propRender(data, props) {
	const { rows } = data ? data : props.properties;
	const a = [], l = rows.length;

	for (let i = 0; i < l; i++) {
		const row = rows[i];

		if (row.index !== 0)
			continue;

		let s = row.display;

		for (const r of rows)
			if (r.propertyLink === row.propertyLink && r.index !== 0)
				s += ', ' + r.display;

		a.push(<span><strong>{row.propertyDisplay}</strong>:&nbsp;{s}</span>);

		if (i + 1 < l)
			a.push(<strong>; </strong>);
	}

	return a.length !== 0
		? a
		: <span>Свойства не заданы</span>;
}
//------------------------------------------------------------------------------
function descRender(data, props) {
	let { name, fullName, description, descriptionInHtml } = data ? data : props;
	const a = [];

	if (fullName && name !== fullName)
		a.push(<strong>{fullName}</strong>);

	if (description)
		if (descriptionInHtml)
			a.push(
				<Typography body1
					dangerouslySetInnerHTML={{
						__html: DOMPurify.sanitize(description)
					}}
				/>);
		else
			a.push(<div className={style.taj}>{description}</div>);

	return a.length !== 0
		? a
		: <span>Описание не задано</span>;
}
//------------------------------------------------------------------------------
function remsRender(data) {
	if (!data || data.length === 0)
		return <span>Нет остатков, временно отсутствует</span>;

	return (
		<table className={style.lightBorderTable}>
			<tr>
				<th>Склад</th>
				<th>Остаток</th>
			</tr>
			<tbody>
				{data.map(r => (
					<tr>
						<td>{r.storeDisplay}</td>
						<td align="right">{r.remainder}</td>
					</tr>))}
			</tbody>
		</table>);
}
//------------------------------------------------------------------------------
function prrmRender(data, props) {
	let { remainder, reserve, price } = props.data;
	remainder -= reserve;
	remainder = Math.max(0, remainder);

	return (
		<div>
			<Typography headline4>
				Доступно для заказа: {remainder} шт.
			</Typography>
			<br />
			<Typography headline4>
				Цена: {price}₽
			</Typography>
		</div>);
}
//------------------------------------------------------------------------------
function sysrRender(data) {
	if (!data || data.length === 0)
		return undefined;//<span>Нет остатков, временно отсутствует</span>;

	return (
		<table className={style.lightBorderTable} >
			<tr>
				<th>Магазин</th>
				<th>Остаток</th>
				<th>Резерв</th>
			</tr>
			<tbody>
				{data.map(r => (
					<tr>
						<td>{r.me ? <b>{r.storeDisplay}</b> : r.storeDisplay}</td>
						<td align="right">{r.remainder}</td>
						<td align="right">{r.reserve}</td>
					</tr>))}
			</tbody>
		</table>);
}
//------------------------------------------------------------------------------
function bprsRender(data) {
	if (!data || data.length === 0)
		return <span>Цены поставщиков не определены</span>;

	return (
		<table className={style.lightBorderTable}>
			<thead />
			{/*<caption>Таблица размеров обуви</caption>*/}
			<tr>
				<th>Дата</th>
				<th>Документ</th>
				<th>Цена</th>
			</tr>
			<tbody>
				{data.map(r => (
					<tr>
						<td>{dateFormatter(r.period)}</td>
						<td>{r.display}</td>
						<td align="right">{r.price}</td>
					</tr>))}
			</tbody>
			<tfoot />
		</table>);
}
//------------------------------------------------------------------------------
function sprsRender(data) {
	if (!data || data.length === 0)
		return <span>Базовые цены не определены</span>;

	return (
		<table className={style.lightBorderTable}>
			<tr>
				<th>Поставщик</th>
				<th>Цена</th>
				<th>Дата</th>
			</tr>
			<tbody>
				{data.map(r => (
					<tr>
						<td>{r.display}</td>
						<td align="right">{r.price}</td>
						<td>{dateFormatter(r.period)}</td>
					</tr>))}
			</tbody>
		</table>);
}
//------------------------------------------------------------------------------
function lprsRender(data) {
	if (!data || data.length === 0)
		return <span>Цены продажи не определены</span>;

	return (
		<table className={style.lightBorderTable}>
			<tr>
				<th>Дата</th>
				<th>Склад</th>
				<th>Цена</th>
			</tr>
			<tbody>
				{data.map(r => (
					<tr>
						<td>{dateFormatter(r.period)}</td>
						<td>{r.display}</td>
						<td align="right">{r.price}</td>
					</tr>))}
			</tbody>
		</table>);
}
//------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------------------
class ExpandableItem extends Component {
	storeConfig() {
		const { f, nopull, link } = this.props;

		this.storePath = productsStorePath + '.' + link + '.' + f;
		this.expandedStorePath = this.storePath + '.expanded';

		const config = [
			{ path: 'auth', alias: 'auth', defaultValue: {} },
			{ path: this.expandedStorePath, alias: 'expanded' }
		];

		if (!nopull)
			config.push(
				{ path: this.storePath, alias: 'data', defaultValue: [] }
			);

		return config;
	}

	willMount() {
		this.pull(this.props);
	}

	pull(props) {
		if (!props.nopull)
			pull.call(this);
	}

	onClick = e => {
		store.toggle(this.expandedStorePath);
		return prevent(e);
	}

	render(props, state) {
		const { title, meta, render, simple } = props;
		const { data, expanded } = state;

		if (simple)
			return (
				<Elevation z={3} tag="div" className={props.className}>
					<Typography headline5>
						<Icon>{meta}</Icon>
						{title.toUpperCase()}
					</Typography>
					<br /><br />
					{render(data, props)}
				</Elevation>);

		return (
			<Elevation z={1} tag="div">
				<List className={style.p0} onClick={this.onClick}>
					<List.Item>
						<List.ItemGraphic>
							{`expand_${expanded ? 'less' : 'more'}`}
						</List.ItemGraphic>
						{title}
						<List.ItemMeta>
							{meta}
						</List.ItemMeta>
					</List.Item>
					{expanded ? <List.Divider /> : undefined}
					{expanded && render ? render(data, props) : undefined}
				</List>
			</Elevation>);
	}
}
//------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------------------
export default class Product extends Component {
	storeConfig() {
		const { link } = this.props;

		this.storePath = productsStorePath + '.' + link;
		this.msgsStorePath = this.storePath + '.msgs';
		this.ratingStorePath = this.storePath + '.rating';
		this.avgRatingStorePath = this.storePath + '.avgrating';
		this.inFavoritesStorePath = this.storePath + '.favorite';
		this.inCartStorePath = this.storePath + '.quantity';

		const data = store.get(this.storePath);

		return [
			{ path: 'auth', alias: 'auth', defaultValue: {} },
			{ path: kioskStorePath, alias: 'kiosk' },
			{ path: headerTitleStorePath, mount: data && data.name },
			{ path: headerSearchStorePath, mount: undefined },
			{ path: iconsLosslessStorePath, alias: 'iconsLossless' },
			{ path: iconsQualityStorePath, alias: 'iconsQuality' },
			{ path: this.storePath, alias: 'data', defaultValue: { products: { rows: [] } } },
			{ path: this.msgsStorePath, alias: 'msgs', defaultValue: [] },
			{ path: this.ratingStorePath, alias: 'rating' },
			{ path: this.avgRatingStorePath, alias: 'avgRating' },
			{ path: this.inFavoritesStorePath, alias: 'inFavorites' },
			{ path: this.inCartStorePath, alias: 'inCart' }
		];
	}

	willMount() {
		pull.call(this);
		pullRating.call(this);
		pullVab.call(this);
	}

	didMount() {
		clearInterval(this.pullMsgsIntervalId);
		this.pullMsgsIntervalId = setInterval(this.pullMsgs, 60 * 1000);
	}

	willUnmount() {
		clearInterval(this.pullMsgsIntervalId);
	}

	linkTo = path => ({ href: path, onClick: plinkRoute(path) })

	goHome = this.linkTo('/')
	goCart = this.linkTo('/cart')

	imageMagnifierRef = e => this.imageMagnifier = e
	showImageMagnifier = link => e => {
		this.imageMagnifier.show(link);
		return prevent(e);
	}

	transformImages(images) {
		const images2 = [];

		for (const v of images)
			images2.push(v, undefined);

		images2.pop();

		return images2;
	}

	inFavoritesToggle = e => {
		push.call(this, { favorite: !this.state.inFavorites });
		return prevent(e);
	}

	inCartToggle = e => {
		push.call(this, { quantity: this.state.inCart ? 0 : 1 });
		return prevent(e);
	}

	pullMsgs = () => this.__pullMsgs()
	__pullMsgs() {
		pullMsgs.call(this);
	}

	snackbarRef = e => this.__snackbarRef(e)
	__snackbarRef(e) {
		this.snackbar = e;
	}

	showError = e => this.__showError(e)
	__showError(e) {
		if (this.snackbar)
			this.snackbar.MDComponent.show({ message: e });
	}

	// regex replace comma without space after
	static cr = /(,(?=\S)|:)/g;
	static sr = /(\s{2})/g;

	static crsr(s) {
		const { cr, sr } = Product;
		return s.replace(cr, ', ').replace(sr, ' ').trim();
	}

	inputMsg = e => {
		this.setState({ msg: e.target.value });
		return prevent(e);
	}

	sendMsg = e => {
		if (this.state.msg)
			pushMsg.call(this, this.state.msg);
		return prevent(e);
	}

	deleteMsg = time => e => {
		deleteMsg.call(this, time);
		return prevent(e);
	}

	toggleRating = e => {
		this.setState({ isRatingExpanded: !this.state.isRatingExpanded });
		return prevent(e);
	}

	pushRating = rating => e => {
		pushRating.call(this, rating);
		return prevent(e);
	}

	render(props, state) {
		const { auth, data, kiosk } = state;

		if (!data)
			return undefined;

		let {
			link,
			code,
			name,
			fullName,
			article,
			manufacturer,
			remainder,
			reserve,
			price,
			images,
			description,
			descriptionInHtml,
			properties
		} = data;

		if (!link)
			return undefined;

		const { authorized } = auth;
		const { crsr } = Product;

		name = crsr(name);
		article = crsr(article);
		manufacturer = crsr(manufacturer);
		remainder = remainder !== 0 || reserve !== 0 ? remainder + (reserve ? ' (' + reserve + ')' : '') : '';

		let display = `[${code}] ${name}`;

		if (article)
			display += `, арт.: ${article}`;

		if (manufacturer)
			display += `, производитель: ${manufacturer}`;

		if (remainder || reserve)
			display += `, ${remainder}#`;

		if (price)
			display += `, ${price}₽`;

		const { imagesLossless, imagesQuality, iconsLossless, iconsQuality } = state;

		images = this.transformImages(images);

		const itemsEx = [];

		if (kiosk) {
			itemsEx.push(
				<LayoutGrid.Cell cols="12">
					<ExpandableItem className={style.sysrContainer}
						nopull simple
						link={link}
						pull={false}
						data={data}
						f="prrm" render={prrmRender}
						title="Заказ"
						meta="attach_money"
					/>
					<ExpandableItem className={style.sysrContainer}
						nopull simple
						link={link}
						pull={false}
						properties={properties}
						f="prop" render={propRender}
						title="Свойства"
						meta="list"
					/>
					<ExpandableItem className={style.sysrContainer}
						nopull simple
						link={link}
						pull={false}
						name={name}
						fullName={fullName}
						description={description}
						descriptionInHtml={descriptionInHtml}
						f="desc" render={descRender}
						title="Описание"
						meta="info"
					/>
					<ExpandableItem className={style.sysrContainer}
						simple
						link={link}
						f="sysr" render={sysrRender}
						title="Остатки"
						meta="reorder"
					/>
				</LayoutGrid.Cell>);
		}
		else {
			itemsEx.push(
				<LayoutGrid.Cell>
					<ExpandableItem
						nopull
						link={link}
						pull={false}
						properties={properties}
						f="prop" render={propRender}
						title="Свойства"
						meta="list"
					/>
				</LayoutGrid.Cell>,
				<LayoutGrid.Cell>
					<ExpandableItem
						nopull
						link={link}
						pull={false}
						name={name}
						fullName={fullName}
						description={description}
						descriptionInHtml={descriptionInHtml}
						f="desc" render={descRender}
						title="Описание"
						meta="info"
					/>
				</LayoutGrid.Cell>);

			if (authorized && auth.employee)
				itemsEx.push(
					<LayoutGrid.Cell>
						<ExpandableItem
							link={link}
							f="rems" render={remsRender}
							title="Остатки"
							meta="reorder"
						/>
					</LayoutGrid.Cell>,
					<LayoutGrid.Cell>
						<ExpandableItem
							link={link}
							f="bprs" render={bprsRender}
							title="Базовые цены"
							meta="monetization_on"
						/>
					</LayoutGrid.Cell>,
					<LayoutGrid.Cell>
						<ExpandableItem
							link={link}
							f="sprs" render={sprsRender}
							title="Цены поставщиков"
							meta="multiline_chart"
						/>
					</LayoutGrid.Cell>,
					<LayoutGrid.Cell>
						<ExpandableItem
							link={link}
							f="lprs" render={lprsRender}
							title="Цены продажи"
							meta="score"
						/>
					</LayoutGrid.Cell>
				);
		}

		const imagesPanelItems = [
			<Image.Magnifier ref={this.imageMagnifierRef}
				lossless={imagesLossless} quality={imagesQuality}
			/>
		];

		for (const v of images)
			imagesPanelItems.push(v ?
				<Image
					inline
					className={style.media}
					link={v}
					onClick={this.showImageMagnifier(v)}
					lossless={iconsLossless}
					quality={iconsQuality}
				/>
				: <Divider inline vertical />);

		let imagesPanel = <div className={style.container}>{imagesPanelItems}</div>;

		if (kiosk)
			imagesPanel = (
				<div className={style.containerKiosk}>
					{imagesPanel}
				</div>);

		const items = [
			<div>{display}</div>,
			<Divider horizontal />,
			imagesPanel,
			<Divider horizontal />,
			<LayoutGrid className={style.p0}>
				<LayoutGrid.Inner className={style.gg0}>
					{itemsEx}
				</LayoutGrid.Inner>
			</LayoutGrid>
		];

		const { inFavorites, inCart, rating, avgRating, isRatingExpanded } = state;
		const bratings = authorized && isRatingExpanded ? [
			<Vab.Fab
				className={rating === 1 ? style.bratingm : style.brating}
				onClick={this.pushRating(1)}
			>
				1
			</Vab.Fab>,
			<Vab.Fab
				className={rating === 2 ? style.bratingm : style.brating}
				onClick={this.pushRating(2)}
			>
				2
			</Vab.Fab>,
			<Vab.Fab
				className={rating === 3 ? style.bratingm : style.brating}
				onClick={this.pushRating(3)}
			>
				3
			</Vab.Fab>,
			<Vab.Fab
				className={rating === 4 ? style.bratingm : style.brating}
				onClick={this.pushRating(4)}
			>
				4
			</Vab.Fab>,
			<Vab.Fab
				className={rating === 5 ? style.bratingm : style.brating}
				onClick={this.pushRating(5)}
			>
				5
			</Vab.Fab>
		] : undefined;

		const vab = (
			<Vab fixed>
				{kiosk ?
					<Vab.Fab {...this.goHome}>
						<Vab.Fab.Icon>
							home
						</Vab.Fab.Icon>
					</Vab.Fab> : undefined}
				{kiosk ?
					<Vab.Fab className={style.b} {...this.goCart}>
						<Vab.Fab.Icon>
							shopping_cart
						</Vab.Fab.Icon>
					</Vab.Fab> : undefined}
				{authorized || avgRating !== undefined ?
					<div className={style.br}>
						{bratings}
						<Vab.Fab className={style.b}
							onClick={authorized ? this.toggleRating : undefined}
						>
							<label className={style.blabel}>
								{avgRating}
							</label>
							<Vab.Fab.Icon>
								{avgRating >= 4.5 ? 'star' : avgRating >= 2.5 ? 'star_half' : 'star_border'}
							</Vab.Fab.Icon>
						</Vab.Fab>
					</div> : undefined}
				{!kiosk && authorized && inFavorites !== undefined ?
					<Vab.Fab className={style.b}
						onClick={this.inFavoritesToggle}
					>
						<Vab.Fab.Icon style={{ justifyContent: 'left' }}>
							{inFavorites ? 'favorite' : 'favorite_bordered'}
						</Vab.Fab.Icon>
					</Vab.Fab> : undefined}
				{authorized ?
					<Vab.Fab className={style.b}
						onClick={this.inCartToggle}
					>
						<Vab.Fab.Icon>
							{inCart ? 'remove_shopping_cart' : 'add_shopping_cart'}
						</Vab.Fab.Icon>
					</Vab.Fab> : undefined}
			</Vab>);

		const { msgs } = state;
		const msgsNodes = [];
		let msgSendPanel;

		if (!kiosk) {
			if (authorized)
				msgSendPanel = (
					<Elevation z={2} className={style.el}>
						<TextField
							outerStyle={{ width: 'calc(100% - 48px - 1px)' }}
							className={style.msgInput1}
							autocomplete="off"
							textarea
							label="Введите текст сообщения ..."
							type="text"
							value={state.msg}
							onInput={this.inputMsg}
						/>
						<Icon className={style.msgIco} onClick={this.sendMsg}>
							send
						</Icon>
					</Elevation>);

			for (let i = msgs.length - 1; i >= 0; i--) {
				if (msgs[i] === undefined)
					continue;

				const { author, isAuthorEmployee, msg, time } = msgs[i];
				let m = msg.replace(/\r\n/g, '\n');
				m = m.replace(/\r/g, '\n');
				m = m.replace(/\n/g, '<br>');

				msgsNodes.push(
					<Elevation key={time} z={2} className={style.el}>
						<Chips>
							<Chips.Chip>
								<Chips.Icon className="material-icons" leading>
									{isAuthorEmployee ? 'supervisor_account' : 'person'}
								</Chips.Icon>
								<Chips.Text>
									{isAuthorEmployee ? 'Сотрудник' : 'Покупатель'}
									&nbsp;
									{dateFormatter(new Date(time))}
								</Chips.Text>
								{auth.link === author ?
									<Chips.Icon
										className="material-icons"
										trailing tabindex="1" role="button"
										title="Delete"
										onClick={this.deleteMsg(time)}
									>
										delete
									</Chips.Icon> : undefined}
							</Chips.Chip>
						</Chips>
						<Typography body1
							dangerouslySetInnerHTML={{ __html: DOMPurify.sanitize(m) }}
						/>
					</Elevation>
				);
			}
		}

		return (
			<div className={style.product}>
				<Snackbar ref={this.snackbarRef} className={style.snackbar} />
				{items}
				{msgSendPanel}
				{msgsNodes}
				{vab}
			</div>
		);
	}
}
//------------------------------------------------------------------------------
