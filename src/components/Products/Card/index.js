//------------------------------------------------------------------------------
import { Component } from 'preact';
import MCard from 'preact-material-components/Card';
import 'preact-material-components/Card/style.css';
import 'preact-material-components/Button/style.css';
import Image from '../../Material/Image';
import { plinkRoute } from '../../../lib/util';
//import nopic from '../../../assets/nopic.svg';
//import hourglassImage from '../../../assets/hourglass.svg';
//import loadingImage from '../../../assets/loading-process.svg';
import style from './style.scss';
//------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------------------
export default class Card extends Component {
	componentWillMount() {
		this.mount(this.props);
	}

	componentWillReceiveProps(props) {
		this.mount(props);
	}

	// regex replace comma without space after
	static cr = /(,(?=\S)|:)/g
	static sr = /(\s{2})/g

	static crsr(s) {
		const { cr, sr } = Card;
		return s.replace(cr, ', ').replace(sr, ' ').trim();
	}

	mount(props) {
		let {
			link,
			name,
			article,
			manufacturer,
			remainder,
			reserve,
			price
		} = props.data;

		this.goProduct = this.linkTo('/product/' + link);

		const { crsr } = Card;

		name = name ? crsr(name) : '';
		article = article ? crsr(article) : '';
		manufacturer = manufacturer ? crsr(manufacturer) : '';
		remainder = remainder && Number.isFinite(remainder) && remainder !== 0
			? remainder.toString() : '';
		remainder += Number.isFinite(reserve) && reserve !== 0
			? ' (' + reserve + ')' : '';
		this.price = price ? price + '₽' : '';

		this.title = name;
		let subTitle = '';

		for (const v of [
			article,
			manufacturer,
			remainder
		])
			if (v.length !== 0)
				subTitle += `, ${v}`;

		this.subTitle = subTitle.substr(2);

		this.titleStyle = [
			'mdc-typography--title',
			style.title,
			props.mini ? style.mini : ''
		].join(' ');
		
		this.subTitleStyle = [
			'mdc-typography--caption',
			style.subTitle,
			props.mini ? style.mini : ''
		].join(' ');
	}

	linkTo = path => ({ href: path, onClick: plinkRoute(path) })

	showImageMagnifier = e => {
		const { showImageMagnifier, data } = this.props;

		if (showImageMagnifier)
			showImageMagnifier(e, data.primaryImageLink);
	}

	render(props, state) {
		return (
			<MCard>
				<div class={this.titleStyle}>
					{this.title}
				</div>
				<div class={this.subTitleStyle}>
					{this.subTitle}<strong>{this.subTitle ? ', ' : ''}{this.price}</strong>
				</div>
				<MCard.Media onClick={this.showImageMagnifier}>
					<Image
						lossless={props.iconLossless}
						quality={props.iconQuality}
						link={props.data.primaryImageLink}
					/>
				</MCard.Media>
				<MCard.ActionButton className={'mdc-button--dense'} {...this.goProduct}>
					ПЕРЕЙТИ
				</MCard.ActionButton>
			</MCard>);
	}
}
//------------------------------------------------------------------------------
