//------------------------------------------------------------------------------
import Button from 'preact-material-components/Button';
import 'preact-material-components/Button/style.css';
import Chips from 'preact-material-components/Chips';
import 'preact-material-components/Chips/style.css';
import Component from '../Component';
import Checkmark from '../Material/Checkmark';
import { imgUrl } from '../../backend';
import { prevent } from '../../lib/util';
import {
	getStoreListPath,
	getStoreSelectedPath,
	applySelectedValues,
	setAPCPaths
} from './props';
import { storePrefix, quickFilterLoader } from './loader';
import store from '../../lib/rstore';
import style from './style.scss';
//------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------------------
export default class QuickFilter extends Component {
	storeConfig() {
		const { props } = this;

		this.storePath = storePrefix + '.' + props.category + '.quickFilter';
		this.storeVisibilityPath = this.storePath + '.visibility';

		return [
			{ path: this.storePath, alias: 'filter' },
			{ path: this.storeVisibilityPath, alias: 'visibility' }
		];
	}

	willUnmount() {
		let controller = this.__loaderController;

		if (controller) {
			controller.abort();
			delete this.__loaderController;
		}
	}

	willMount() {
		quickFilterLoader.call(this);
	}

	willUpdate(props, state) {
		if (this.props.category !== props.category)
			quickFilterLoader.call(this);
	}

	propValueChangeOp(selected, value, op) {
		if (Array.isArray(value))
			for (const v of value)
				selected = this.propValueChangeOp(selected, v.link ? v.link : v.value, op);
		else
			switch (op) {
				case 'toggle':
					if (selected.has(value))
						selected.delete(value);
					else
						selected.add(value);
					break;
				case 'set':
					selected.add(value);
					break;
				case 'reset':
					selected.delete(value);
					break;
			}

		return selected;
	}

	propValueChange(link, value, op) {
		const { category } = this.props;
		const storeListPath = getStoreListPath(category);
		const propsList = store.get(storeListPath, []);
		const storeSelectedPath = getStoreSelectedPath(category, link);
		const selected = this.propValueChangeOp(store.get(storeSelectedPath, new Set()), value, op);
		const apcPaths = setAPCPaths({ storePath: storePrefix + '.' + category });

		if (propsList.findIndex(v => v.link === link) === -1) {
			propsList.push({ link });
			store.replace(storeListPath, propsList);
		}

		store.replace(storeSelectedPath, selected);
		store.replace(apcPaths.storeChangedPath, true);
		applySelectedValues.call(apcPaths, category);
	}

	quickSubFilterValueToggle = (link, value) => e => {
		this.propValueChange(link, value, 'toggle');
		return prevent(e);
	}

	renderQuickSubFilterValues(link, values) {
		const { category } = this.props;
		const storeSelectedPath = getStoreSelectedPath(category, link);
		const selected = store.get(storeSelectedPath, new Set());
		const items = [];

		for (const v of values) {
			const key = v.link ? v.link : v.value;

			items.push(
				<Chips.Chip
					key={key}
					selected={selected.has(key)}
					onClick={this.quickSubFilterValueToggle(link, key)}
				>
					<Chips.Checkmark />
					<Chips.Text>
						{v.value}
					</Chips.Text>
				</Chips.Chip>);
		}

		return items;
	}

	quickSubFilterToggle = link => e => {
		let v = store.get(this.storeVisibilityPath);

		if (!(v instanceof Object))
			v = {};

		if (v.subkey === link) {
			v.subvisible = !v.subvisible;
		}
		else {
			v.subkey = link;
			v.subvisible = true;
		}

		store.replace(this.storeVisibilityPath, v);

		return prevent(e);
	}

	renderQuickSubFilter(subfilter) {
		let { visibility } = this.state;

		if (!(visibility instanceof Object))
			visibility = {};

		const { category } = this.props;
		let sub, head = [];

		for (const { link, name, ico, value } of subfilter) {
			const visible = visibility.subkey === link && visibility.subvisible;
			const storeSelectedPath = getStoreSelectedPath(category, link);
			const selected = store.get(storeSelectedPath, new Set());

			head.push(
				<Button
					className={visible ? style.kioskButtonSel : style.kioskButton}
					onClick={this.quickSubFilterToggle(link)}
				>
					{ico ? <img src={imgUrl({ u: ico, h: 24, lossless: true, type: 'png' })} /> : undefined}
					{name.toUpperCase()}
					{selected.size !== 0 ? <Button.Icon>check</Button.Icon> : undefined}
				</Button>);

			if (visible)
				sub = (
					<Chips choice>
						{this.renderQuickSubFilterValues(link, value.rows)}
					</Chips>);

		}

		if (head.length !== 0)
			head = [<div>{head}</div>, sub];

		return head;
	}

	quickFilterValueToggle = (link, value) => e => {
		this.propValueChange(link, value, 'toggle');
		return prevent(e);
	}

	renderQuickFilterValues(qfLink, values) {
		const { category } = this.props;
		const storeSelectedPath = getStoreSelectedPath(category, qfLink);
		const selected = store.get(storeSelectedPath, new Set());
		let items = [];

		for (const { link, value, ico } of values) {
			const sel = selected.has(link);
			const cn = sel
				? style.kioskManufacturerButtonSel
				: style.kioskManufacturerButton;
			const clk = this.quickFilterValueToggle(qfLink, link);

			items.push(ico ?
				<div className={cn}>
					<img src={imgUrl({ u: ico, h: 30, lossless: true, type: 'png' })}
						key={link}
						onClick={clk}
						alt={value}
					/>
					{sel ? <Checkmark /> : undefined}
				</div> :
				<Button
					key={link}
					className={cn}
					onClick={clk}
				>
					{value}
					{sel ? <Button.Icon>check</Button.Icon> : undefined}
				</Button>);
		}

		if (items.length !== 0)
			items = <div>{items}</div>;

		return items;
	}

	getQuickFilterValueKey(link, value) {
		let key;

		if (value.rows)
			key = link;
		else if (Array.isArray(value)) {
			key = '';

			for (const v of value)
				key += ',' + (v.link ? v.link : v.value);

			key = key.substr(1);
		}
		else
			key = value;

		return key;
	}

	quickFilterToggle = (filter, link, value) => e => {
		const key = this.getQuickFilterValueKey(link, value);
		let v = store.get(this.storeVisibilityPath);

		if (!(v instanceof Object))
			v = {};

		if (v.key === key) {
			v.visible = !v.visible;
		}
		else {
			v.key = key;
			v.visible = true;
		}

		store.replace(this.storeVisibilityPath, v);

		for (const qf of filter) {
			if (qf.value.rows)
				continue;

			const op = link === qf.link && key === this.getQuickFilterValueKey(link, qf.value)
				? 'set' : 'reset';
			this.propValueChange(qf.link, qf.value, op);
		}

		return prevent(e);
	}

	render(props, state) {
		const { filter } = state;

		if (!filter)
			return undefined;

		let { visibility } = state;

		if (!(visibility instanceof Object))
			visibility = {};

		let quickFilterView = [], quickFilterValuesView = [], subFilterView = [];
		let rows, rowsLink;

		for (const { link, name, value, ico, subfilter } of filter) {
			if (value.rows) {
				rows = value.rows;
				rowsLink = link;
			}

			const selected = visibility.key === this.getQuickFilterValueKey(link, value);
			const visible = selected && visibility.visible;

			if (visible && rows)
				quickFilterValuesView = this.renderQuickFilterValues(rowsLink, rows);

			quickFilterView.push(
				<Button
					key={value}
					className={selected && (!value.rows || visible) ? style.kioskButtonSel : style.kioskButton}
					onClick={this.quickFilterToggle(filter, link, value)}
				>
					{ico ? <img src={imgUrl({ u: ico, h: 24, lossless: true, type: 'png' })} /> : undefined}
					{name}
				</Button>);

			if (visible && subfilter)
				subFilterView = this.renderQuickSubFilter(subfilter.rows);
		}

		if (quickFilterView.length !== 0) {
			quickFilterView = <div>{quickFilterView}</div>;
			quickFilterView = (
				<div className={style.quickFilterView}>
					{quickFilterView}
					{quickFilterValuesView}
					{subFilterView}
				</div>);
		}

		return quickFilterView;
	}
}
//------------------------------------------------------------------------------
