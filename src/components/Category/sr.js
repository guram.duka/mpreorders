//------------------------------------------------------------------------------
import Switch from 'preact-material-components/Switch';
import 'preact-material-components/Switch/style.css';
import FormField from 'preact-material-components/FormField';
import 'preact-material-components/FormField/style.css';
import Radio from 'preact-material-components/Radio';
import 'preact-material-components/Radio/style.css';
import 'preact-material-components/List/style.css';
import Component from '../Component';
import { prevent } from '../../lib/util';
import {
	orderFields,
	orderDirections,
	getStoreSearchPath,
	applySelectedValues,
	setAPCPaths
} from './props';
import { storePrefix } from './loader';
import store from '../../lib/rstore';
import style from './style.scss';
//------------------------------------------------------------------------------
const orderFieldsPseudonyms = [
	'коду',
	'наименованию',
	'цене',
	'остатку',
	'артикулу'
];
//------------------------------------------------------------------------------
// const orderDirectionsPseudonyms = [
// 	'возрастанию',
// 	'убыванию'
// ];
//------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------------------
export default class Sorting extends Component {
	storeConfig() {
		const { category } = this.props;
		const apcPaths = setAPCPaths({ storePath: storePrefix + '.' + category });

		this.storePath = apcPaths.storeAppliedPath;

		return [
			{ path: this.storePath, alias: 'applied' }
		];
	}

	toggleAlias = alias => e => {
		const { category } = this.props;
		const apcPaths = setAPCPaths({ storePath: storePrefix + '.' + category });

		store.toggle(getStoreSearchPath(category) + '.' + alias);
		store.replace(apcPaths.storeChangedPath, true);
		applySelectedValues.call(apcPaths, category);

		//return prevent(e);
	}

	selectOrderField = field => e => {
		const { category } = this.props;
		const apcPaths = setAPCPaths({ storePath: storePrefix + '.' + category });

		store.set(getStoreSearchPath(category) + '.orderField', field);
		store.replace(apcPaths.storeChangedPath, true);
		applySelectedValues.call(apcPaths, category);

		return prevent(e);
	}

	selectOrderDirection = direction => e => {
		const { category } = this.props;
		const apcPaths = setAPCPaths({ storePath: storePrefix + '.' + category });

		store.set(getStoreSearchPath(category) + '.orderDirection', direction);
		store.replace(apcPaths.storeChangedPath, true);
		applySelectedValues.call(apcPaths, category);

		return prevent(e);
	}

	render(props, state) {
		const { applied } = state;
		const orderItems = [
			<FormField>
				<label className={style.f120}>
					Упорядочить по
				</label>
			</FormField>
		];

		for (const [i, field] of orderFields.entries()) {
			const name = 'order-field';
			const id = name + '-' + field;
			const sfield = applied && applied.orderField
				? applied.orderField
				: orderFields[1];

			orderItems.push(
				<FormField>
					<Radio
						id={id}
						autocomplete="off"
						checked={sfield === field}
						value={field}
						name={name}
						onChange={this.selectOrderField(field)}
					/>
					<label for={id}>
						{orderFieldsPseudonyms[i]}
					</label>
				</FormField>);
		}

		orderItems.push(
			<FormField>
				<label className={style.f150}>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</label>
			</FormField>);

		for (const [i, direction] of orderDirections.entries()) {
			const name = 'order-direction';
			const id = name + '-' + direction;
			const sdir = applied && applied.orderDirection
				? applied.orderDirection
				: orderDirections[0];

			orderItems.push(
				<FormField>
					<Radio
						id={id}
						autocomplete="off"
						checked={sdir === direction}
						value={direction}
						name={name}
						onChange={this.selectOrderDirection(direction)}
					/>
					<label className={style.f150} for={id}>
						{[
							<r>&#8593;</r>,
							<r>&#8595;</r>
						][i]}
					</label>
				</FormField>);
		}

		const stockSwitch = (
			<Switch
				checked={applied && applied.stock}
				onChange={this.toggleAlias('stock')}
			/>);

		const imageSwitch = (
			<Switch
				checked={applied && applied.image}
				onChange={this.toggleAlias('image')}
			/>);
	
		return (
			<div className={style.sortingView}>
				{orderItems}
				<span className={style.prel2}>
					<span>Наличие</span>
					<span>{stockSwitch}</span>
				</span>
				<span className={style.prel2}>
					<span>Изображение</span>
					<span>{imageSwitch}</span>
				</span>
			</div>);
	}
}
//------------------------------------------------------------------------------
