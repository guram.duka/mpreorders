//------------------------------------------------------------------------------
import DOMPurify from 'dompurify';
import Button from 'preact-material-components/Button';
import 'preact-material-components/Button/style.css';
import Component from '../Component';
import { imgUrl } from '../../backend';
import { prevent, deepEqual } from '../../lib/util';
import {
	getStoreSearchPath,
	applySelectedValues,
	setAPCPaths
} from './props';
import { storePrefix, carFilterLoader } from './loader';
import { newEnum } from '../../lib/enum';
import store from '../../lib/rstore';
import style from './style.scss';
//------------------------------------------------------------------------------
const Steps = newEnum({
	CONNECT: 0,
	MANUFACTURER: 1,
	MODEL: 2,
	HULL: 3,
	MODIFICATION: 4,
	YEAR: 5
});
//------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------------------
export default class CarFilter extends Component {
	storeConfig() {
		const { props } = this;

		this.storePath = storePrefix + '.' + props.category;
		this.storeFilterLinkPath = this.storePath + '.carFilter';
		this.storeFilterIsHullsPath = this.storeFilterLinkPath + '.hulls';
		this.storeFilterCategoriesPath = this.storeFilterLinkPath + '.categories';
		this.storeFilterVisibilityPath = this.storeFilterLinkPath + '.visible';

		const config = [
			{ path: this.storeFilterLinkPath, alias: 'link', owner: this, reaction: this.filterLinkConnect },
			{ path: this.storeFilterIsHullsPath, alias: 'hulls', owner: this },
			{ path: this.storeFilterCategoriesPath, alias: 'categories', owner: this },
			{ path: this.storeFilterVisibilityPath, alias: 'visible' }
		];

		if (this.link)
			config.push(
				{ path: this.storeFilterSelectedPath, alias: 'selected' },
				{ path: this.storeFilterManufacturersPath, alias: 'manufacturers' },
				{ path: this.storeFilterModelsPath, alias: 'models' },
				{ path: this.storeFilterHullsPath, alias: 'hulls' },
				{ path: this.storeFilterModificationsPath, alias: 'modifications' },
				{ path: this.storeFilterYearsPath, alias: 'years' },
				{ path: this.storeFilterCarLogoPath, alias: 'carLogo' },
				{ path: this.storeFilterCarRecommendationsPath, alias: 'carRecommendations' }
			);

		return config;
	}

	willUnmount() {
		delete this.link;
	}

	willMount() {
		carFilterLoader.call(this, this.filterLinkLoaded);
	}

	willUpdate(props, state) {
		if (this.props.category !== props.category)
			carFilterLoader.call(this, this.filterLinkLoaded);
	}

	oneRowQuickSelection() {
		const selected = store.get(this.storeFilterSelectedPath);
		const step = selected ? selected.step : Steps.CONNECT;
		const manufacturers = store.get(this.storeFilterManufacturersPath);
		const models = store.get(this.storeFilterModelsPath);
		const hulls = store.get(this.storeFilterHullsPath);
		const modifications = store.get(this.storeFilterModificationsPath);
		const years = store.get(this.storeFilterYearsPath);

		switch (step) {
			case Steps.CONNECT:
				if (manufacturers && manufacturers.length === 1)
					this.manufacturerClick(manufacturers[0])();
				break;
			case Steps.MANUFACTURER:
				if (models && models.length === 1)
					this.modelClick(models[0])();
				break;
			case Steps.MODEL:
				if (hulls && hulls.length === 1)
					this.hullClick(hulls[0])();
				break;
			case Steps.HULL:
				if (modifications && modifications.length === 1)
					this.modificationClick(modifications[0])();
				break;
			case Steps.MODIFICATION:
				if (years && years.length === 1)
					this.yearClick(years[0])();
		}
	}

	filterLinkLoaded = result => {
		store.replace(this.storeFilterLinkPath, result.link);
		store.set(this.storeFilterIsHullsPath, !result.noHulls);
		store.set(this.storeFilterCategoriesPath, result.categories, 1, deepEqual);
	}

	filterLinkConnect() {
		this.storeFilterDataPath = 'filters.' + this.link;
		this.storeFilterSelectedPath = this.storeFilterDataPath + '.selected';
		this.storeFilterManufacturersPath = this.storeFilterDataPath + '.manufacturers';
		this.storeFilterModelsPath = this.storeFilterDataPath + '.models';
		this.storeFilterHullsPath = this.storeFilterDataPath + '.hulls';
		this.storeFilterModificationsPath = this.storeFilterDataPath + '.modifications';
		this.storeFilterYearsPath = this.storeFilterDataPath + '.years';

		this.storeFilterCarLogoPath = this.storeFilterDataPath + '.carLogo';
		this.storeFilterCarRecommendationsPath = this.storeFilterDataPath + '.carRecommendations';

		const selected = store.get(this.storeFilterSelectedPath);

		this.stepLoader(selected);
	}

	stepLoader(selected, onLoad) {
		const step = selected ? selected.step : Steps.CONNECT;
		const success = storePath => result => {
			store.set(storePath, result.rows, 1, deepEqual);
			if (onLoad)
				onLoad.call(this);
		};
		let query;

		switch (step) {
			case Steps.CONNECT:
				carFilterLoader.call(this, success(this.storeFilterManufacturersPath), { manufacturers: true });
				break;
			case Steps.MANUFACTURER:
				carFilterLoader.call(this, success(this.storeFilterModelsPath), {
					manufacturer: selected.manufacturer.link,
					models: true
				});
				break;
			case Steps.MODEL:
				carFilterLoader.call(this, success(this.storeFilterHullsPath), {
					manufacturer: selected.manufacturer.link,
					model: selected.model.link,
					hulls: true
				});
				break;
			case Steps.HULL:
				query = {
					manufacturer: selected.manufacturer.link,
					model: selected.model.link,
					modifications: true
				};

				if (this.hulls)
					query.hull = selected.hull.link;

				carFilterLoader.call(this, success(this.storeFilterModificationsPath), query);
				break;
			case Steps.MODIFICATION:
				query = {
					manufacturer: selected.manufacturer.link,
					model: selected.model.link,
					modification: selected.modification.link,
					years: true
				};

				if (this.hulls)
					query.hull = selected.hull.link;
					
				carFilterLoader.call(this, success(this.storeFilterYearsPath), query);
				break;
			case Steps.YEAR:
				query = {
					manufacturer: selected.manufacturer.link,
					model: selected.model.link,
					modification: selected.modification.link,
					year: selected.year.link
				};

				if (this.hulls)
					query.hull = selected.hull.link;

				carFilterLoader.call(this, result => {
					store.set(this.storeFilterCarLogoPath, result.logo);
					store.set(this.storeFilterCarRecommendationsPath, result.recommendations);
				}, query);
				this.applyCarFilter(selected);
				break;
		}
	}

	toggleVisibility = e => {
		store.toggle(this.storeFilterVisibilityPath);
		return prevent(e);
	}

	removeCarFilter(category) {
		if (!category && this.categories) {
			for (const c of this.categories)
				this.removeCarFilter(c);
			return;
		}

		if (!category)
			category = this.props.category;

		// remove car filter
		const storeCarPath = getStoreSearchPath(category) + '.car';
		const apcPaths = setAPCPaths({ storePath: storePrefix + '.' + category });

		if (store.get(storeCarPath)) {
			store.delete(storeCarPath);
			store.replace(apcPaths.storeChangedPath, true);
			applySelectedValues.call(apcPaths, category);
		}
	}

	applyCarFilter(selected) {
		// apply car filter
		const { category } = this.props;
		const storeCarPath = getStoreSearchPath(category) + '.car';
		const apcPaths = setAPCPaths({ storePath: storePrefix + '.' + category });
		const values = {
			manufacturer: selected.manufacturer.link,
			model: selected.model.link,
			modification: selected.modification.link,
			year: selected.year.link
		};

		if (this.hulls)
			values.hull = selected.hull.link;

		store.replace(storeCarPath, values);
		store.replace(apcPaths.storeChangedPath, true);
		applySelectedValues.call(apcPaths, category);
	}

	stepBackSelection = e => {
		const selected = { ...store.get(this.storeFilterSelectedPath) };

		selected.step = ~~selected.step - 1;

		if (selected.step === Steps.HULL && !this.hulls)
			selected.step--;

		store.replace(this.storeFilterSelectedPath, selected);

		this.removeCarFilter();
		this.stepLoader();

		return prevent(e);
	}

	clearSelection = e => {
		this.removeCarFilter();
		store.delete(this.storeFilterSelectedPath);
		this.stepLoader();
		return prevent(e);
	}

	manufacturerClick = manufacturer => e => {
		const selected = {
			step: Steps.MANUFACTURER,
			manufacturer
		};

		store.replace(this.storeFilterSelectedPath, selected);
		this.stepLoader(selected, this.oneRowQuickSelection);

		return prevent(e);
	}

	modelClick = model => e => {
		const selected = {
			...store.get(this.storeFilterSelectedPath),
			step: this.hulls ? Steps.MODEL : Steps.HULL,
			model
		};

		store.replace(this.storeFilterSelectedPath, selected);
		this.stepLoader(selected, this.oneRowQuickSelection);

		return prevent(e);
	}

	hullClick = hull => e => {
		const selected = {
			...store.get(this.storeFilterSelectedPath),
			step: Steps.HULL,
			hull
		};

		store.replace(this.storeFilterSelectedPath, selected);
		this.stepLoader(selected, this.oneRowQuickSelection);

		return prevent(e);
	}

	modificationClick = modification => e => {
		const selected = {
			...store.get(this.storeFilterSelectedPath),
			step: Steps.MODIFICATION,
			modification
		};

		store.replace(this.storeFilterSelectedPath, selected);
		this.stepLoader(selected, this.oneRowQuickSelection);

		return prevent(e);
	}

	yearClick = year => e => {
		const selected = {
			...store.get(this.storeFilterSelectedPath),
			step: Steps.YEAR,
			year
		};

		store.replace(this.storeFilterSelectedPath, selected);
		this.stepLoader(selected);

		return prevent(e);
	}

	renderSelection(items, data, click) {
		if (data)
			for (const v of data) {
				items.push(
					v.ico ?
						<div className={style.kioskManufacturerButton}>
							<img src={imgUrl({ u: v.ico, h: 50, lossless: true, type: 'png' })}
								key={v.link}
								alt={v.name}
								onClick={click(v)}
							/>
						</div> :
						<Button
							key={v.link}
							className={style.kioskManufacturerButton}
							onClick={click(v)}
						>
							{v.name}
						</Button>
				);
			}

		return items;
	}

	renderCarLR() {
		let { carLogo, carRecommendations } = this.state;

		if (carLogo) {
			carLogo = imgUrl({
				u: carLogo,
				h: carRecommendations ? undefined : 350,
				lossless: true,
				w2t: true,
				type: 'png'
			});
			carLogo = (
				<td>
					<img src={carLogo} style={{ width: '100%' }} />
				</td>);
		}

		if (carRecommendations) {
			const html = { __html: DOMPurify.sanitize(carRecommendations) };
			// eslint-disable-next-line
			carRecommendations = <td dangerouslySetInnerHTML={html} />;
		}

		if (carLogo || carRecommendations)
			// return (
			// 	<div className={style.car}>
			// 		{carRecommendations}{carLogo}
			// 	</div>);
			/* eslint react/no-danger: 0 */
			return (
				<table className={style.carTable}>
					<tr>
						{carRecommendations}
						{carLogo}
					</tr>
				</table>);
			/* eslint react/no-danger: 1 */
	}

	render(props, state) {
		if (this.link) {
			const { selected, visible } = state;
			const step = selected ? selected.step : Steps.CONNECT;
			let items = [], selectionItems = [], extraText = 'Подбор по автомобилю';
			const selectedCarName = [];

			if (selected) {
				const { manufacturer, model, hull, modification, year } = selected;

				if (step >= Steps.MANUFACTURER)
					selectedCarName.push(manufacturer.name);

				if (step >= Steps.MODEL)
					selectedCarName.push(model.name);

				if (step >= Steps.HULL && this.hulls)
					selectedCarName.push(hull.name);

				if (step >= Steps.MODIFICATION)
					selectedCarName.push(modification.name);

				if (step >= Steps.YEAR)
					selectedCarName.push(year.name);
			}

			switch (step) {
				case Steps.CONNECT:
					if (visible) {
						this.renderSelection(selectionItems, state.manufacturers, this.manufacturerClick);
						extraText = 'Выберите производителя автомобиля';
					}
					break;
				case Steps.MANUFACTURER:
					if (visible)
						this.renderSelection(selectionItems, state.models, this.modelClick);
					extraText = selectedCarName.join(' ').trim() + ': Выберите модель';
					break;
				case Steps.MODEL:
					if (visible)
						this.renderSelection(selectionItems, state.hulls, this.hullClick);
					extraText = selectedCarName.join(' ').trim() + ': Выберите кузов';
					break;
				case Steps.HULL:
					if (visible)
						this.renderSelection(selectionItems, state.modifications, this.modificationClick);
					extraText = selectedCarName.join(' ').trim() + ': Выберите модификацию';
					break;
				case Steps.MODIFICATION:
					if (visible)
						this.renderSelection(selectionItems, state.years, this.yearClick);
					extraText = selectedCarName.join(' ').trim() + ': Выберите год выпуска';
					break;
				case Steps.YEAR:
					extraText = 'Выбран автомобиль: ' + selectedCarName.join(' ').trim();
					break;
			}

			if (visible && step > Steps.CONNECT)
				items.push(
					<Button onClick={this.stepBackSelection}>
						<Button.Icon>arrow_back</Button.Icon>
					</Button>,
					<Button onClick={this.clearSelection}>
						<Button.Icon>clear</Button.Icon>
					</Button>
				);

			if (selectionItems.length !== 0)
				selectionItems = (
					<div className={style.quickFilterView}>
						{selectionItems}
					</div>);

			items.unshift(
				<Button
					className={visible ? style.kioskButtonSel : style.kioskButton}
					onClick={this.toggleVisibility}
				>
					<Button.Icon>directions_car</Button.Icon>
					{extraText}
				</Button>);

			return (
				<div>
					<div className={style.quickFilterView}>
						{items}
					</div>
					{selectionItems}
					{visible && step === Steps.YEAR
						? this.renderCarLR() : undefined}
				</div>);
		}
	}
}
//------------------------------------------------------------------------------
