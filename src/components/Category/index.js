//------------------------------------------------------------------------------
import { route } from 'preact-router';
import LayoutGrid from 'preact-material-components/LayoutGrid';
import 'preact-material-components/LayoutGrid/style.css';
import Component from '../Component';
import { prevent, plinkRoute } from '../../lib/util';
import {
	headerTitleStorePath,
	headerSearchStorePath,
	imagesLosslessStorePath,
	imagesQualityStorePath,
	iconsLosslessStorePath,
	iconsQualityStorePath,
	kioskStorePath
} from '../../const';
import PropsView from './props';
import QuickFilter from './qf';
import CarFilter from './cf';
import Sorting from './sr.js';
import loader, { storePrefix } from './loader';
import store from '../../lib/rstore';
import style from './style.scss';
import ProductCard from '../Products/Card';
import Vab from '../Material/VerticalActionBar';
import Image from '../Material/Image';
//------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------------------
export default class Category extends Component {
	storeConfig() {
		const { props } = this;

		this.storePath = storePrefix + '.' + props.category;
		this.storeDataPath = this.storePath + '.' + this.page;
		this.storeAppliedPath = this.storePath + '.applied';
		this.storeVisiblePath = this.storePath + '.visible';

		const data = store.get(this.storeDataPath);

		return [
			{ path: headerTitleStorePath, mount: data && data.name },
			{ path: headerSearchStorePath, mount: this.storePath },
			{ path: kioskStorePath, alias: 'kiosk' },
			{ path: imagesLosslessStorePath, alias: 'imagesLossless' },
			{ path: imagesQualityStorePath, alias: 'imagesQuality' },
			{ path: iconsLosslessStorePath, alias: 'iconsLossless' },
			{ path: iconsQualityStorePath, alias: 'iconsQuality' },
			{ path: this.storeDataPath, alias: 'data', defaultValue: { rows: [] } },
			{ path: this.storeAppliedPath, alias: 'applied', owner: this, reaction: loader },
			{ path: this.storeVisiblePath, alias: 'visible' }
		];
	}

	decodeProps(props) {
		let [page, pageSize] = props.pageProps.split(',');

		pageSize = ~~pageSize;
		this.pageSize = pageSize = pageSize > 0 ? pageSize : 40;
		this.page = page = ~~page;

		this.goPrev = this.goPage(page - 1);
		this.goNext = this.goPage(page + 1);
		this.index = ((page > 0 ? page : 1) - 1) * pageSize;
	}

	willUnmount() {
		let controller = this.__loaderController;

		if (controller) {
			controller.abort();
			delete this.__loaderController;
		}
	}

	willMount() {
		const { kiosk, visible } = this.state;

		if (kiosk && visible)
			store.delete(this.storeVisiblePath);

		loader.call(this);
	}

	willUpdate(props, state) {
		if (this.props.category !== props.category
			|| this.props.pageProps !== props.pageProps)
			loader.call(this, props, state);
	}

	didSetState(keys) {
		const { state } = this;
		const { data } = state;

		if (data && this.page > data.pages)
			route(this.pageHref(data.pages !== 0 ? data.pages : 1), true);
	}

	linkTo = path => ({ href: path, onClick: plinkRoute(path) })
	pageHref = page => '/category/' + this.props.category + '/' + page + ',' + this.pageSize
	goPage = page => {
		const path = this.pageHref(page);
		return {
			href: path,
			onClick: e => {
				store.delete(this.storeVisiblePath);
				return plinkRoute(path);
			}
		};
	}

	//goPrev = e => root.history.back()

	goHome = this.linkTo('/')
	goCart = this.linkTo('/cart')

	imageMagnifierRef = e => this.__imageMagnifierRef(e)
	__imageMagnifierRef(e) {
		this.imageMagnifier = e;
	}

	showImageMagnifier = (...args) => this.__showImageMagnifier(...args)
	__showImageMagnifier(e, link) {
		this.imageMagnifier.show(link);
		return prevent(e);
	}

	render(props, state) {
		const { category } = props;
		const {
			data,
			visible,
			kiosk,
			imagesLossless,
			imagesQuality,
			iconsLossless,
			iconsQuality
		} = state;

		return (
			<div className={style.category}>
				<Image.Magnifier ref={this.imageMagnifierRef}
					lossless={imagesLossless} quality={imagesQuality}
				/>
				{visible ? <PropsView category={category} /> : undefined}
				{visible && kiosk ? undefined : <CarFilter category={category} />}
				{visible && kiosk ? undefined : <QuickFilter category={category} />}
				{visible && kiosk ? undefined : <Sorting category={category} />}
				{visible ? undefined :
					<LayoutGrid>
						<LayoutGrid.Inner>
							{data.rows.map(row => (
								<LayoutGrid.Cell>
									<ProductCard data={row}
										iconLossless={iconsLossless}
										iconQuality={iconsQuality}
										showImageMagnifier={this.showImageMagnifier}
									/>
								</LayoutGrid.Cell>))}
						</LayoutGrid.Inner>
					</LayoutGrid>}
				{visible ? undefined :
					<Vab fixed>
						{kiosk ?
							<Vab.Fab {...this.goHome}>
								<Vab.Fab.Icon>
									home
								</Vab.Fab.Icon>
							</Vab.Fab> : undefined}
						{kiosk ?
							<Vab.Fab {...this.goCart}>
								<Vab.Fab.Icon>
									shopping_cart
								</Vab.Fab.Icon>
							</Vab.Fab> : undefined}

						{/* {this.page > 1 && kiosk
							? <Vab.Fab {...this.goPrev}>
								<Vab.Fab.Icon>
									arrow_back
								</Vab.Fab.Icon>
							</Vab.Fab>
							: undefined} */}
						{this.page < data.pages
							? <Vab.Fab {...this.goNext}>
								<Vab.Fab.Icon>
									arrow_forward
								</Vab.Fab.Icon>
							</Vab.Fab>
							: undefined}
						<Vab.ScrollUp />
					</Vab>}
			</div>);
	}
}
//------------------------------------------------------------------------------
