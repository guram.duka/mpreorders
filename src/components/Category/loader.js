//------------------------------------------------------------------------------
import { bfetch } from '../../backend';
import store from '../../lib/rstore';
import { deepEqual } from '../../lib/util';
import { headerTitleStorePath } from '../../const';
import { successor, failer, starter } from '../load';
//------------------------------------------------------------------------------
export const storePrefix = 'category';
//------------------------------------------------------------------------------
export function pullPropertiesValues() {
	const object = this;
	const { props, storePath } = object;
	const opts = {
		r: {
			m: 'category',
			f: 'pull_filter_prop_values',
			r: {
				target: 'products',
				category: props.category,
				link: props.link
			}
		}
	};

	bfetch(
		opts,
		successor(result => store.set(storePath, result.rows, 1, deepEqual)),
		failer(),
		starter()
	);
}
//------------------------------------------------------------------------------
export function pullProperties() {
	const object = this;
	const { props, storePath } = object;
	const opts = {
		r: {
			m: 'category',
			f: 'pull_filter_props',
			r: {
				target: 'products',
				category: props.category
			}
		}
	};

	return bfetch(
		opts,
		successor(result => store.set(storePath, result.rows, 1, deepEqual)),
		failer(),
		starter()
	);
}
//------------------------------------------------------------------------------
export default function loader(props, state) {
	if (!props)
		props = this.props;
	if (!state)
		state = this.state;

	const object = this;
	const { page, pageSize, applied } = object;
	const { category } = props;
	const r = {
		target: 'products',
		piece: pageSize,
		index: (page - 1) * pageSize
	};

	if (category !== undefined)
		r.category = category;

	if (applied) {
		const { orderField, orderDirection, filter, stock, image } = applied;

		if (orderField && orderField !== 'name')
			r.order = { field: orderField };

		if (orderDirection && orderDirection !== 'asc')
			r.order = { ...r.order, direction: orderDirection };

		if (filter)
			r.filter = filter;

		if (stock !== undefined && stock)
			r.stock = true;

		if (image !== undefined && image)
			r.image = true;

		if (applied.props) {
			// const p = [];

			// // skip special value reserved for GUI
			// for (const prop of applied.props) {
			// 	const { link, values } = prop;
			// 	const i = values.indexOf(link);

			// 	if (i === -1) {
			// 		p.push(prop);
			// 	}
			// 	else {
			// 		const v = Array.from(values);

			// 		v.splice(i, 1);
					
			// 		if (v.length !== 0)
			// 			p.push({ link, values: v });
			// 	}
			// }

			r.props = applied.props;
		}

		if (applied.car)
			r.car = applied.car;
	}

	let controller = this.__loaderController;

	if (controller) {
		controller.abort();
		delete this.__loaderController;
	}

	const opts = { r: { m: 'dict', f: 'filter', r } };

	if (state.kiosk)
		opts.r.r.nogzip = true;

	bfetch(
		opts,
		successor(result => {
			if (opts.controller === this.__loaderController) {
				store.set(headerTitleStorePath, result.name);
				store.set(object.storeDataPath, result, 1, deepEqual);
			}
		}),
		failer(),
		starter()
	);

	this.__loaderController = opts.controller;

	return controller;
}
//------------------------------------------------------------------------------
export function quickFilterLoader() {
	const { category } = this.props;
	let controller = this.__loaderController;

	if (controller) {
		controller.abort();
		delete this.__loaderController;
	}

	const opts = { r: {
		m: 'category',
		f: 'pull_quick_filter',
		r: { target: 'products', category }
	} };
	const object = this;

	bfetch(
		opts,
		successor(result => {
			if (opts.controller === this.__loaderController)
				store.set(object.storePath, result.rows, 1, deepEqual);
		}),
		failer(),
		starter()
	);

	this.__loaderController = opts.controller;
}
//------------------------------------------------------------------------------
export function carFilterLoader_(storePath, query, onLoad) {
	const { category } = this.props;
	// let controller = this.__loaderController;

	// if (controller) {
	// 	controller.abort();
	// 	delete this.__loaderController;
	// }

	const opts = { r: {
		m: 'category',
		f: 'pull_car_filter',
		r: { target: 'products', category, ...query }
	} };
	//const object = this;

	bfetch(
		opts,
		successor(result => {
			//if (opts.controller === this.__loaderController)
			if (storePath === 'car')
				this.setState({ carLogo: result.logo, carRecommendations: result.recommendations });
			else if (result.rows)
				store.set(storePath, result.rows ? result.rows : result.link, 1, deepEqual);
			else
				store.replace(storePath, result.link);
			
			if (onLoad)
				onLoad.call(this);
		}),
		failer(),
		starter()
	);

	//this.__loaderController = opts.controller;
}
//------------------------------------------------------------------------------
export function carFilterLoader(success, query) {
	const { category } = this.props;
	// let controller = this.__loaderController;

	// if (controller) {
	// 	controller.abort();
	// 	delete this.__loaderController;
	// }

	const opts = { r: {
		m: 'category',
		f: 'pull_car_filter',
		r: { target: 'products', category, ...query }
	} };
	//const object = this;

	bfetch(
		opts,
		successor(success),
		failer(),
		starter()
	);

	//this.__loaderController = opts.controller;
}
//------------------------------------------------------------------------------
