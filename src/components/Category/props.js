//------------------------------------------------------------------------------
import LayoutGrid from 'preact-material-components/LayoutGrid';
import 'preact-material-components/LayoutGrid/style.css';
import List from 'preact-material-components/List';
import 'preact-material-components/List/style.css';
import Switch from 'preact-material-components/Switch';
import 'preact-material-components/Switch/style.css';
import TextField from 'preact-material-components/TextField';
import 'preact-material-components/TextField/style.css';
import Select from 'preact-material-components/Select';
import 'preact-material-components/Select/style.css';
import Component from '../Component';
import 'preact-material-components/Button/style.css';
import Chips from 'preact-material-components/Chips';
import 'preact-material-components/Chips/style.css';
import Elevation from '../Material/Elevation';
import store from '../../lib/rstore';
import { prevent, deepEqual } from '../../lib/util';
import { storePrefix, pullProperties, pullPropertiesValues } from './loader';
import style from './style.scss';
import Button from 'preact-material-components/Button';
//------------------------------------------------------------------------------
function getPrefix(category) {
	return storePrefix + '.' + category;
}
//------------------------------------------------------------------------------
function storeChangedPath(category) {
	return getPrefix(category) + '.changed';
}
//------------------------------------------------------------------------------
export function getStoreSearchPath(category) {
	return getPrefix(category) + '.search';
}
//------------------------------------------------------------------------------
function getStorePrefix(category) {
	return getPrefix(category) + '.properties';
}
//------------------------------------------------------------------------------
export function getStoreListPath(category) {
	return getStorePrefix(category) + '.list';
}
//------------------------------------------------------------------------------
function getStorePropertyPath(category, link) {
	return getStorePrefix(category) + '.' + link;
}
//------------------------------------------------------------------------------
function getStoreExpandedPath(category, link) {
	return getStorePropertyPath(category, link) + '.expanded';
}
//------------------------------------------------------------------------------
export function getStoreSelectedPath(category, link) {
	return getStorePropertyPath(category, link) + '.selected';
}
//------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------------------
class PropValuesView extends Component {
	storeConfig() {
		return [
			{ path: this.expandedPath, alias: 'visible' },
			{ path: this.storePath, alias: 'values', defaultValue: [] },
			{ path: this.storeSelectedPath, alias: 'selected', defaultValue: new Set() },
			{ path: this.storeChangedPath, alias: 'changed' }
		];
	}

	decodeProps(props) {
		const { category, link } = props;
		this.expandedPath = getStoreExpandedPath(category, link);
		this.storePath = getStorePropertyPath(category, link) + '.values';
		this.storeSelectedPath = getStoreSelectedPath(category, link);
		this.storeChangedPath = storeChangedPath(category);
	}

	willMount() {
		pullPropertiesValues.call(this);
	}

	willUpdate(props, state) {
		if (this.props.category !== props.category
			|| this.props.link !== props.link)
			pullPropertiesValues.call(this);
	}

	chipClick = key => e => {
		const selected = store.get(this.storeSelectedPath, new Set());

		if (selected.has(key))
			selected.delete(key);
		else
			selected.add(key);

		store.replace(this.storeSelectedPath, selected);
		store.set(this.storeChangedPath, true);
	}

	render(props, state) {
		let { values, selected } = state;

		if (!selected)
			selected = new Set();

		const items = [];

		for (const { link, value } of values) {
			const key = link ? link : value;

			items.push(
				<Chips.Chip
					selected={selected.has(key)}
					onClick={this.chipClick(key)}
				>
					<Chips.Checkmark />
					<Chips.Text>
						{value}
					</Chips.Text>
				</Chips.Chip>);
		}

		return (
			<Chips filter
				className={state.visible ? '' : style.dn}
			>
				{items}
			</Chips>);
	}
}
//------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------------------
class PropsViewItem extends Component {
	storeConfig() {
		return [
			{ path: this.storeListPath, alias: 'list', defaultValue: [] },
			{ path: this.storePath, alias: 'expanded' }
		];
	}

	decodeProps(props) {
		const { category, link } = props;
		this.storeListPath = getStoreListPath(category);
		this.storePath = getStoreExpandedPath(category, link);
	}

	toggle = e => this.__toggle(e)
	__toggle(e) {
		const { props } = this;
		const { category, link } = props;
		const list = store.get(this.storeListPath, []);

		for (const v of list) {
			const p = getStoreExpandedPath(category, v.link);
			const expanded = store.get(p);

			if (v.link === link && !expanded)
				store.set(p, true);
			else
				store.delete(p);
		}

		return prevent(e);
	}

	render(props, state) {
		const { link, name, category } = props;
		const { expanded } = state;

		return (
			<Elevation z={1} tag="div">
				<List className={style.p0}>
					<List.Item onClick={this.toggle}>
						<List.ItemGraphic>
							{`expand_${expanded ? 'less' : 'more'}`}
						</List.ItemGraphic>
						{name}
					</List.Item>
					{expanded ? <List.Divider /> : undefined}
					{expanded ?
						<PropValuesView
							category={category}
							link={link}
						/> : undefined}
				</List>
			</Elevation>);
	}
}
//------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------------------
class PropsViewItems extends Component {
	storeConfig() {
		return [
			{ path: this.storePath, alias: 'list', defaultValue: [] }
		];
	}

	decodeProps(props) {
		this.storePath = getStoreListPath(props.category);
	}

	willMount() {
		pullProperties.call(this);
	}

	willUpdate(props, state) {
		if (this.props.category !== props.category)
			pullProperties.call(this);
	}

	render(props, state) {
		//console.log('render PropsViewItems', props, state);
		const items = [];

		for (const v of state.list)
			items.push((
				<LayoutGrid.Cell>
					<PropsViewItem
						key={v.link}
						category={props.category}
						{...v}
					/>
				</LayoutGrid.Cell>));

		return (
			<LayoutGrid.Inner className={style.gg0}>
				{items}
			</LayoutGrid.Inner>);
	}
}
//------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------------------
export const orderFields = [
	'code',
	'name',
	'price',
	'remainder',
	'article'
];
//------------------------------------------------------------------------------
const aliases = [
	'filter',
	'stock',
	'image',
	'orderField',
	'orderDirection',
	'car'
];
//------------------------------------------------------------------------------
export const orderDirections = [
	'asc',
	'desc'
];
//------------------------------------------------------------------------------
class OtherPropsView extends Component {
	storeConfig() {
		return aliases.map(n => ({
			path: this['store'
				+ n.charAt(0).toUpperCase()
				+ n.substr(1)
				+ 'Path'] = this.storePath + '.' + n,
			alias: n
		}));
	}

	decodeProps(props) {
		const { category } = props;
		this.storePath = getStoreSearchPath(category);
		this.storeChangedPath = storeChangedPath(category);
	}

	filterInput = e => this.__filterInput(e)
	__filterInput(e) {
		let v = e.target.value.trim();

		if (v.length === 0)
			v = undefined;

		store.undef(this.storeFilterPath, v);
		store.set(this.storeChangedPath, true);

		return prevent(e);
	}

	stockChange = e => this.__stockChange(e)
	__stockChange(e) {
		let v = e.target.checked;

		if (!v)
			v = undefined;

		store.undef(this.storeStockPath, v);
		store.set(this.storeChangedPath, true);

		return prevent(e);
	}

	imageChange = e => this.__imageChange(e)
	__imageChange(e) {
		let v = e.target.checked;

		if (!v)
			v = undefined;

		store.undef(this.storeImagePath, v);
		store.set(this.storeChangedPath, true);

		return prevent(e);
	}

	orderFieldChange = e => this.__orderFieldChange(e)
	__orderFieldChange = e => {
		let v = orderFields[e.target.selectedIndex - 1];

		if (v === 'name')
			v = undefined;

		store.undef(this.storeOrderFieldPath, v);
		store.set(this.storeChangedPath, true);

		return prevent(e);
	}

	orderDirectionChange = e => this.__orderDirectionChange(e)
	__orderDirectionChange = e => {
		let v = orderDirections[e.target.selectedIndex - 1];

		if (v === 'asc')
			v = undefined;

		store.undef(this.storeOrderDirectionPath, v);
		store.set(this.storeChangedPath, true);

		return prevent(e);
	}

	render(props, state) {
		const { orderField, orderDirection } = state;
		const orderFieldSelectedIndex = orderFields.indexOf(orderField) + 1;
		const orderDirectionSelectedIndex = orderDirections.indexOf(orderDirection) + 1;

		return (
			<LayoutGrid.Inner className={style.gg0}>
				<LayoutGrid.Cell>
					<Elevation z={1} tag="div">
						<TextField
							outerStyle={{ width: '100%', marginTop: 0, marginBottom: 0 }}
							label="Вводите текст ..."
							trailingIcon="search"
							value={state.filter}
							onInput={this.filterInput}
						/>
					</Elevation>
				</LayoutGrid.Cell>
				<LayoutGrid.Cell>
					<Elevation z={1} tag="div">
						<div>&#x200B;</div>
						<div className={style.prel}>
							<span>Имеющиеся в наличии</span>
							<span className={style.pabsr0}>
								<Switch
									checked={state.stock}
									onChange={this.stockChange}
								/>
							</span>
						</div>
						<div>&#x200B;</div>
						<div>&#x200B;</div>
						<div className={style.prel}>
							<span>Имеющие изображение</span>
							<span className={style.pabsr0}>
								<Switch
									checked={state.image}
									onChange={this.imageChange}
								/>
							</span>
						</div>
						<div>&#x200B;</div>
					</Elevation>
				</LayoutGrid.Cell>
				<LayoutGrid.Cell>
					<Elevation z={1} tag="div">
						<Select hintText="Поле сортировки"
							className={style.w100}
							selectedIndex={orderFieldSelectedIndex}
							onChange={this.orderFieldChange}
						>
							<Select.Item>Код</Select.Item>
							<Select.Item>Наименование</Select.Item>
							<Select.Item>Цена</Select.Item>
							<Select.Item>Остаток</Select.Item>
							<Select.Item>Артикул</Select.Item>
						</Select>
						<Select hintText="Направление сортировки"
							className={style.w100}
							selectedIndex={orderDirectionSelectedIndex}
							onChange={this.orderDirectionChange}
						>
							<Select.Item>123АБВ ... 789ЭЮЯ</Select.Item>
							<Select.Item>987ЯЮЭ ... 321ВБА</Select.Item>
						</Select>
					</Elevation>
				</LayoutGrid.Cell>
			</LayoutGrid.Inner>);
	}
}
//------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------------------
export function applySelectedValues(category) {
	const applied = {};

	for (const alias of aliases) {
		const p = getStoreSearchPath(category) + '.' + alias;
		const v = store.get(p);

		if (v !== undefined)
			applied[alias] = v;
	}

	const properties = store.get(getStoreListPath(category), []);

	for (const { link } of properties) {
		const p = getStoreSelectedPath(category, link);
		const v = store.get(p);

		if (v !== undefined) {
			if (applied.props === undefined)
				applied.props = [];

			const values = Array.from(v);

			if (values.length !== 0)
				applied.props.push({ link, values });
		}
	}

	if (applied.props && applied.props.length === 0)
		delete applied.props;

	if (Object.keys(applied).length !== 0)
		store.set(this.storeAppliedPath, applied, 1, deepEqual);
	else
		store.delete(this.storeAppliedPath);

	store.delete(this.storeChangedPath);
	store.delete(this.storeVisiblePath);
}
//------------------------------------------------------------------------------
export function setAPCPaths(object) {
	if (this === undefined) {
		setAPCPaths.call(object);
		return object;
	}

	return ['applied', 'changed', 'visible'].map(n => ({
		path: this['store'
			+ n.charAt(0).toUpperCase()
			+ n.substr(1)
			+ 'Path'] = this.storePath + '.' + n,
		alias: n
	}));
}
//------------------------------------------------------------------------------
class ControlInner extends Component {
	storeConfig() {
		return setAPCPaths.call(this);
	}

	decodeProps(props) {
		this.storePath = getPrefix(props.category);
		//this.storeChangedPath = storeChangedPath(category);
		//this.storeVisiblePath = getPrefix(category) + '.visible';
	}

	apply = e => this.__apply(e)
	__apply(e) {
		applySelectedValues.call(this, this.props.category);
		return prevent(e);
	}

	reset = e => this.__reset(e)
	__reset(e) {
		const { category } = this.props;

		for (const alias of aliases)
			store.delete(getStoreSearchPath(category) + '.' + alias);

		const properties = store.get(getStoreListPath(category), []);

		for (const { link } of properties)
			store.delete(getStoreSelectedPath(category, link));

		store.delete(this.storeChangedPath);
		store.delete(this.storeAppliedPath);
		store.delete(this.storeVisiblePath);

		return prevent(e);
	}

	render(props, state) {
		return (
			<LayoutGrid.Inner className={style.gg0}>
				<LayoutGrid.Cell cols="12">
					<Elevation z={2} tag="div">
						<Button
							className={style.w100}
							disabled={!state.changed}
							onClick={this.apply}
						>
							ПРИМЕНИТЬ
						</Button>
					</Elevation>
				</LayoutGrid.Cell>
				<LayoutGrid.Cell cols="12">
					<Elevation z={2} tag="div">
						<Button
							className={style.w100}
							disabled={!state.changed && !state.applied}
							onClick={this.reset}
						>
							СБРОСИТЬ
						</Button>
					</Elevation>
				</LayoutGrid.Cell>
			</LayoutGrid.Inner>);
	}
}
//------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------------------
export default class PropsView extends Component {
	storeConfig() {
		const { category } = this.props;

		this.storeVisiblePath = getPrefix(category) + '.visible';

		return [
			{ path: this.storeVisiblePath, alias: 'visible' }
		];
	}

	render(props, state) {
		if (!state.visible)
			return undefined;

		return (
			<div>
				<LayoutGrid className={style.gp0}>
					<OtherPropsView category={props.category} />
				</LayoutGrid>
				<LayoutGrid className={style.gp0}>
					<PropsViewItems category={props.category} />
				</LayoutGrid>
				<LayoutGrid className={style.gpb0}>
					<ControlInner category={props.category} />
				</LayoutGrid>
			</div>);
	}
}
//------------------------------------------------------------------------------
