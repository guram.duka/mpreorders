//------------------------------------------------------------------------------
import Component from '../../Component';
import Elevation from '../../Material/Elevation';
import Typography from 'preact-material-components/Typography';
import 'preact-material-components/Typography/style.css';
import Checkbox from 'preact-material-components/Checkbox';
import 'preact-material-components/Checkbox/style.css';
import Image from '../../Material/Image';
import { plinkRoute } from '../../../lib/util';
import style from './style.scss';
//------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------------------
export default class Card extends Component {
	storeConfig() {
		const { link, checkMarksStorePath } = this.props;
		const config = [];

		if (checkMarksStorePath)
			config.push(
				{ path: checkMarksStorePath + '.' + link, alias: 'checkMark' }
			);
		
		return config;
	}

	// regex replace comma without space after
	static cr = /(,(?=\S)|:)/g
	static sr = /(\s{2})/g

	static crsr(s) {
		const { cr, sr } = Card;
		return s.replace(cr, ', ').replace(sr, ' ').trim();
	}

	decodeProps(props) {
		const {
			link,
			code,
			name,
			quantity,
			price
		} = props;

		this.goProduct = this.linkTo('/product/' + link);
		this.name = Card.crsr(`[${code}] ${name}`);
		this.price = `${price}₽`;
		this.quantity = `${quantity} шт.`;
	}

	linkTo = path => ({ href: path, onClick: plinkRoute(path) })

	render(props, state) {
		return (
			<Elevation z={1} tag="div" className={style.pr}>
				<Image
					lossless={props.iconLossless}
					quality={props.iconQuality}
					link={props.primaryImageLink}
					className={style.media}
					{...this.goProduct}
				>
					<Checkbox className={props.checkMarkClick ? style.cb : style.cbInvis}
						onChange={props.checkMarkClick}
						onClick={props.checkMarkClick}
						checked={state.checkMark}
					/>
				</Image>
				<div className={style.titleBlock}>
					<Typography body1 className={style.n}>
						<strong>#{props.n}</strong>
					</Typography>
					&nbsp;
					<a {...this.goProduct}>
						{this.name}
					</a>
					<div className={style.brc}>
						<div className={style.tar}>
							<Typography body1 className={style.sm}>
								Цена: <strong>{this.price}</strong>
							</Typography>
						</div>
					</div>
				</div>
			</Elevation>);
	}
}
//------------------------------------------------------------------------------
