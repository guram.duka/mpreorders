//------------------------------------------------------------------------------
import { burl, bfetch } from '../../backend';
import { successor, failer, starter } from '../load';
import { deepEqual } from '../../lib/util';
import store from '../../lib/rstore';
import { refreshUrls as productRefreshUrls } from '../Products/Product/loader';
//------------------------------------------------------------------------------
export const favoritesPath = 'favorites';
export const favoritesDataPath = `${favoritesPath}.data`;
export const favoritesCheckMarksPath = `${favoritesPath}.checkmarks`;
//------------------------------------------------------------------------------
function pullOpts() {
	const { state } = this;
	return {
		auth: state.auth,
		r: {
			m: 'favorites',
			f: 'pull'
		},
		a: state.auth.link
	};
}
//------------------------------------------------------------------------------
function generatePKey(result) {
	const { rows } = result;
	const pkey = {};

	for (const [i, { link }] of rows.entries())
		pkey[link] = i;

	result.pkey = pkey;
	result.pkl = rows.length;

	return result;
}
//------------------------------------------------------------------------------
export function pull() {
	const opts = pullOpts.call(this);

	return bfetch(
		opts,
		successor(result => {
			this.refreshUrls = {
				...this.refreshUrls,
				[opts.url]: result.maxAge
			};

			store.set(favoritesDataPath, generatePKey(result), 1, deepEqual);
		}),
		failer(error => this.showError(error.message)),
		starter()
	);
}
//------------------------------------------------------------------------------
function clearCommitedCards(links) {
	const data = store.get(favoritesDataPath);
	const { rows, pkey } = data;

	for (const link of links) {
		const i = pkey[link];
		const row = rows[i];

		if (!row || row.link !== link)
			continue;

		rows[i] = undefined;
		delete pkey[link];
		data.pkl--;
	}

	return data;
}
//------------------------------------------------------------------------------
function gatherCheckMarkedCardsInfo() {
	const { state } = this;
	const { data, auth } = state;
	const { pkey } = data;
	const links = [], refreshUrls = { ...this.refreshUrls };
	let checkedCnt = 0, nzCnt = 0;

	for (const link of Object.keys(pkey)) {
		if (store.get(favoritesCheckMarksPath + '.' + link)) {
			const urls = productRefreshUrls.call({
				// emulate Product object
				props: { link },
				state: { auth }
			});

			for (const [k, v] of Object.entries(urls))
				refreshUrls[k] = v;

			links.push(link);
			checkedCnt++;
		}
		nzCnt++;
	}

	return [refreshUrls, links, checkedCnt, nzCnt];
}
//------------------------------------------------------------------------------
export function clear() {
	if (this.clearCallId)
		return;

	const { state } = this;
	const opts = {
		auth: state.auth,
		method: 'PUT',
		r: {
			m: 'cart',
			f: 'clear',
			r: {
				favorite: false
			}
		}
	};

	const [refreshUrls, links, checkedCnt, nzCnt] =
		gatherCheckMarkedCardsInfo.call(this);

	if (checkedCnt === 0)
		return;

	if (nzCnt !== checkedCnt)
		opts.r.r.links = links;

	opts.refreshUrls = refreshUrls;

	this.clearCallId = bfetch(
		opts,
		successor(result => {
			const data = clearCommitedCards.call(this, links);

			store.replace(favoritesDataPath, data);
			store.delete(favoritesCheckMarksPath);

			delete this.clearCallId;
		}),
		failer(error => {
			delete this.clearCallId;
			this.showError(error.message);
		}),
		starter()
	);
}
//------------------------------------------------------------------------------
export function refreshUrls() {
	return { [burl(pullOpts.call(this))]: 60 };
}
//------------------------------------------------------------------------------
