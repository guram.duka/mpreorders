//------------------------------------------------------------------------------
import { route } from 'preact-router';
import Snackbar from 'preact-material-components/Snackbar';
import 'preact-material-components/Snackbar/style.css';
import LayoutGrid from 'preact-material-components/LayoutGrid';
import 'preact-material-components/LayoutGrid/style.css';
import Component from '../Component';
import {
	headerTitleStorePath,
	headerSearchStorePath,
	iconsLosslessStorePath,
	iconsQualityStorePath
} from '../../const';
import { prevent, plinkRoute } from '../../lib/util';
import {
	favoritesDataPath,
	favoritesCheckMarksPath,
	pull,
	clear
} from './loader';
import store from '../../lib/rstore';
import style from './style.scss';
import Card from './Card';
import Vab from '../Material/VerticalActionBar';
//------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------------------
export default class Favorite extends Component {
	storeConfig = [
		{ path: headerTitleStorePath, mount: 'Избранное' },
		{ path: headerSearchStorePath, mount: undefined },
		{ path: iconsLosslessStorePath, alias: 'iconsLossless' },
		{ path: iconsQualityStorePath, alias: 'iconsQuality' },
		{ path: 'auth', alias: 'auth', defaultValue: {} },
		{ path: favoritesDataPath, alias: 'data', defaultValue: { rows: [] } },
		{ path: favoritesCheckMarksPath, alias: 'checkMarks' }
	]

	willMount() {
		this.didSetState();
		pull.call(this);
	}

	didSetState() {
		const { auth } = this.state;

		if (!auth.authorized)
			route('/', true);
	}

	linkTo = path => this.__linkTo(path)
	__linkTo(path) {
		return { href: path, onClick: plinkRoute(path) };
	}

	snackbarRef = e => this.__snackbarRef(e)
	__snackbarRef(e) {
		this.snackbar = e;
	}

	showError = e => this.__showError(e)
	__showError(e) {
		if (this.snackbar)
			this.snackbar.MDComponent.show({ message: e });
	}

	getCard(link) {
		let { cards } = this;

		if (!cards)
			this.cards = cards = {};

		let card = cards[link];

		if (!card)
			this.cards[link] = card = {};

		return card;
	}

	handleCheckMarkClick = link => {
		const card = this.getCard(link);
		let { cm } = card;

		if (!cm)
			card.cm = cm = e => {
				const p0 = favoritesCheckMarksPath + '.' + link;
				const p1 = favoritesCheckMarksPath + '.length';

				store.toggle(p0);

				if (store.get(p0))
					store.inc(p1);
				else {
					store.dec(p1);
					store.delete(p0);
				}

				let l = store.get(p1);

				if (!l)
					store.delete(p1);

				store.undef(favoritesCheckMarksPath, l ? true : undefined);

				return prevent(e);
			};

		return cm;
	}

	setCheckMarks = e => this.__setCheckMarks(e)
	__setCheckMarks(e) {
		const { data } = this.state;

		store.delete(favoritesCheckMarksPath);

		for (const row of data.rows)
			store.set(favoritesCheckMarksPath + '.' + row.link, true);

		store.set(favoritesCheckMarksPath + '.length', data.rows.length);
		store.set(favoritesCheckMarksPath, true);

		return prevent(e);
	}

	resetCheckMarks = e => this.__resetCheckMarks(e)
	__resetCheckMarks(e) {
		store.delete(favoritesCheckMarksPath);
		return prevent(e);
	}

	clearSelectedItems = e => {
		clear.call(this);
		return prevent(e);
	}

	render(props, state) {
		const { data, checkMarks } = state;

		if (data === undefined)
			return undefined;

		const vab = (
			<Vab fixed display={data.pkl}>
				{checkMarks ?
					<Vab.Fab className={style.b}
						onClick={this.clearSelectedItems}
					>
						<Vab.Fab.Icon>
							delete
						</Vab.Fab.Icon>
					</Vab.Fab> : undefined}
				<Vab.Fab className={style.b}
					onClick={this.setCheckMarks}
				>
					<Vab.Fab.Icon>
						&#xE834;
					</Vab.Fab.Icon>
				</Vab.Fab>
				<Vab.Fab className={style.b}
					onClick={this.resetCheckMarks}
				>
					<Vab.Fab.Icon>
						&#xE835;
					</Vab.Fab.Icon>
				</Vab.Fab>
			</Vab>);

		const { iconsLossless, iconsQuality } = state;
		let n = 0;

		return (
			<div class={style.cart}>
				{/*<i className="icomoon">checkbox-checked</i>
				<i className="icomoon">checkbox-unchecked</i>
				<i className="lnr">&#xe811;</i>*/}
				<Snackbar ref={this.snackbarRef} class={style.snackbar} />
				<LayoutGrid>
					<LayoutGrid.Inner>
						{data.rows.map(row => row ? (
							<LayoutGrid.Cell {...this.cellEventsHandlers}>
								<Card {...row}
									n={++n}
									key={row.link}
									iconLossless={iconsLossless}
									iconQuality={iconsQuality}
									checkMarkClick={this.handleCheckMarkClick(row.link)}
									checkMarksStorePath={favoritesCheckMarksPath}
								/>
							</LayoutGrid.Cell>) : undefined)}
					</LayoutGrid.Inner>
				</LayoutGrid>
				{vab}
			</div>
		);
	}
}
//------------------------------------------------------------------------------
