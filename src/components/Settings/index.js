//------------------------------------------------------------------------------
import Component from '../Component';
import LayoutGrid from 'preact-material-components/LayoutGrid';
import 'preact-material-components/LayoutGrid/style.css';
import Switch from 'preact-material-components/Switch';
import 'preact-material-components/Switch/style.css';
import TextField from 'preact-material-components/TextField';
import 'preact-material-components/TextField/style.css';
import {
	headerTitleStorePath,
	headerSearchStorePath,
	darkThemeStorePath,
	imagesLosslessStorePath,
	imagesQualityStorePath,
	iconsLosslessStorePath,
	iconsQualityStorePath,
	kioskStorePath,
	kioskSlipCopiesStorePath,
	kioskNameStorePath,
	backendStorePath
} from '../../const';
import store from '../../lib/rstore';
import { prevent } from '../../lib/util';
import style from './style.scss';
//------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------------------
export default class Settings extends Component {
	storeConfig() {
		return [
			{ path: 'auth', alias: 'auth', defaultValue: {} },
			{ path: headerTitleStorePath, mount: 'Настройки' },
			{ path: headerSearchStorePath, mount: undefined },
			{ path: darkThemeStorePath, alias: 'darkThemeEnabled' },
			{ path: imagesLosslessStorePath, alias: 'imagesLossless' },
			{ path: imagesQualityStorePath, alias: 'imagesQuality' },
			{ path: iconsLosslessStorePath, alias: 'iconsLossless' },
			{ path: iconsQualityStorePath, alias: 'iconsQuality' },
			{ path: kioskStorePath, alias: 'kiosk' },
			{ path: kioskSlipCopiesStorePath, alias: 'slipCopies' },
			{ path: kioskNameStorePath, alias: 'kioskName' },
			{ path: backendStorePath, alias: 'backend' }
		];
	}

	darkThemeToggle = e => {
		store.toggle(darkThemeStorePath);
		return prevent(e);
	}

	imagesLosslessToggle = e => {
		store.toggle(imagesLosslessStorePath);
		return prevent(e);
	}

	imagesQualityInput = e => {
		const v = e.target.value;

		if (v.length !== 0 && ~~v >= 1 && ~~v <= 100)
			store.set(imagesQualityStorePath, ~~v);
		else
			store.delete(imagesQualityStorePath);

		return prevent(e);
	}

	iconsLosslessToggle = e => {
		store.toggle(iconsLosslessStorePath);
		return prevent(e);
	}

	iconsQualityInput = e => {
		const v = e.target.value;

		if (v.length !== 0 && ~~v >= 1 && ~~v <= 100)
			store.set(iconsQualityStorePath, ~~v);
		else
			store.deleteIn(iconsQualityStorePath);

		return prevent(e);
	}

	kioskToggle = e => {
		store.toggle(kioskStorePath);
		return prevent(e);
	}

	backendInput = e => {
		const v = e.target.value;

		if (v.length !== 0)
			store.set(backendStorePath, v);
		else
			store.delete(backendStorePath);

		return prevent(e);
	}

	slipCopiesInput = e => {
		const v = e.target.value;

		if (v.length !== 0 && ~~v >= 0 && ~~v <= 10)
			store.set(kioskSlipCopiesStorePath, ~~v);
		else
			store.delete(kioskSlipCopiesStorePath);

		return prevent(e);
	}

	kioskNameInput = e => {
		const v = e.target.value;

		if (v.length !== 0)
			store.set(kioskNameStorePath, v);
		else
			store.delete(kioskNameStorePath);

		return prevent(e);
	}

	render(props, state) {
		const {
			darkThemeEnabled,
			imagesLossless,
			imagesQuality,
			iconsLossless,
			iconsQuality,
			kiosk,
			backend,
			slipCopies,
			kioskName
		} = state;

		const outerStyle = { width: '100%', marginTop: 0, marginBottom: 0 };

		return (
			<LayoutGrid className={style.settings}>
				<LayoutGrid.Inner>
					<LayoutGrid.Cell>
						<span className={style.fl}>Тёмная тема</span>
						<Switch className={style.fr} onClick={this.darkThemeToggle} checked={darkThemeEnabled} />
					</LayoutGrid.Cell>
					<LayoutGrid.Cell>
						<span className={style.fl}>Изображения без потерь</span>
						<Switch className={style.fr} onClick={this.imagesLosslessToggle} checked={imagesLossless} />
					</LayoutGrid.Cell>
					<LayoutGrid.Cell>
						<TextField
							outerStyle={outerStyle}
							className={imagesLossless ? style.dn : undefined}
							label="Качество изображений [1...100] - 100"
							trailingIcon="image"
							value={imagesQuality ? imagesQuality : ''}
							onInput={this.imagesQualityInput}
							type="number" step="1" min="1" max="100"
							invalid={!Number.isFinite(imagesQuality) || imagesQuality < 1 || imagesQuality > 100}
						/>
					</LayoutGrid.Cell>
					<LayoutGrid.Cell>
						<span className={style.fl}>Иконки без потерь</span>
						<Switch className={style.fr} onClick={this.iconsLosslessToggle} checked={iconsLossless} />
					</LayoutGrid.Cell>
					<LayoutGrid.Cell>
						<TextField
							outerStyle={outerStyle}
							className={iconsLossless ? style.dn : undefined}
							label="Качество иконок [1...100] - 15"
							trailingIcon="image"
							value={iconsQuality ? iconsQuality : ''}
							onInput={this.iconsQualityInput}
							type="number" step="1" min="1" max="100"
							invalid={!Number.isFinite(iconsQuality) || imagesQuality < 1 || imagesQuality > 100}
						/>
					</LayoutGrid.Cell>
					<LayoutGrid.Cell>
						<span className={style.fl}>Режим киоска</span>
						<Switch className={style.fr} onClick={this.kioskToggle} checked={kiosk} />
					</LayoutGrid.Cell>
					{kiosk ? <LayoutGrid.Cell>
						<TextField
							outerStyle={outerStyle}
							label="Backend URL"
							trailingIcon="http"
							value={backend}
							onInput={this.backendInput}
						/>
					</LayoutGrid.Cell> : undefined}
					{kiosk ? <LayoutGrid.Cell>
						<TextField
							outerStyle={outerStyle}
							label="Копий слипа"
							trailingIcon="print"
							value={slipCopies ? slipCopies : 1}
							onInput={this.slipCopiesInput}
							type="number" step="1" min="0" max="10"
							invalid={!Number.isFinite(slipCopies) || slipCopies < 0 || slipCopies > 10}
						/>
					</LayoutGrid.Cell> : undefined}
					{kiosk ? <LayoutGrid.Cell>
						<TextField
							outerStyle={outerStyle}
							label="Имя киоска"
							trailingIcon="print"
							value={kioskName}
							onInput={this.kioskNameInput}
						/>
					</LayoutGrid.Cell> : undefined}
				</LayoutGrid.Inner>
			</LayoutGrid>);
	}
}
//------------------------------------------------------------------------------
