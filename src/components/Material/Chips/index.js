//------------------------------------------------------------------------------
import { MDCChipSet } from '@material/chips';
import MaterialComponent from 'preact-material-components/Base/MaterialComponent';
import Icon from 'preact-material-components/Icon';
import 'preact-material-components/Chips/style.css';
import './style.scss';
//------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------------------
// make controlled component
//----------------------------------- -------------------------------------------
class Chips extends MaterialComponent {
	constructor() {
		super();
		this.componentName = 'chip-set';
		this.mdcProps = ['choice', 'filter', 'input'];
	}

	componentDidMount() {
		this.__mount(this.props);
	}

	componentWillUpdate(props) {
		this.MDComponent.destroy && this.MDComponent.destroy();
		this.__mount(props);
	}

	componentWillUnmount() {
		this.MDComponent.destroy && this.MDComponent.destroy();
	}

	__mount(props) {
		this.MDComponent = new MDCChipSet(this.control);
		const { choice, filter, onSelect } = props;

		if ((choice || filter) && onSelect) {
			const obj = this;
			const { MDComponent } = obj;
			const interaction = MDComponent.foundation_.handleChipInteraction_;

			MDComponent.foundation_.handleChipInteraction_ = function (evt) {
				const { foundation_ } = evt.detail.chip;

				for (const [i, v] of MDComponent.chips.entries())
					if (v.foundation_ === foundation_) {
						interaction.call(this, evt);
						onSelect(i, foundation_.isSelected());
						break;
					}
			};
		}
	}

	materialDom(allprops) {
		const { children, ...props } = allprops;

		return (
			<div {...props} ref={this.setControlRef}>
				{children}
			</div>
		);
	}
}
//------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------------------
class Chip extends MaterialComponent {
	constructor() {
		super();
		this.componentName = 'chip';
		this.mdcProps = ['selected'];
	}

	materialDom(allprops, state) {
		const { children, ...props } = allprops;

		return (
			<div {...props} ref={this.setControlRef}>
				{children}
			</div>
		);
	}
}
//------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------------------
class ChipIcon extends Icon {
	constructor() {
		super();
		this.componentName = 'chip__icon';
		this.mdcProps = ['leading', 'trailing'];
	}

	materialDom(allprops) {
		const { children, ...props } = allprops;
		const otherprops = {};
		if (props.trailing) {
			otherprops.tabindex = 0;
			otherprops.role = 'button';
		}
		return (
			<Icon {...props} {...otherprops}>
				{children}
			</Icon>
		);
	}
}
//------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------------------
class ChipText extends MaterialComponent {
	constructor() {
		super();
		this.componentName = 'chip__text';
		this.mdcProps = [];
	}

	materialDom(allprops) {
		const { children, tag, href, ...props } = allprops;
		const JSXTag = tag ? tag : href ? 'a' : 'div';

		if (JSXTag === 'a') {
			const className = `mdc-${this.componentName}--link`;
			return <a {...props} className={className} href={href}>{children}</a>;
		}

		return <JSXTag {...props}>{children}</JSXTag>;
	}
}
//------------------------------------------------------------------------------
class ChipCheckmark extends Icon {
	constructor() {
		super();
		this.componentName = 'chip__checkmark';
		this.mdcProps = [];
	}

	materialDom(props) {
		return (
			<div {...props}>
				<svg class="mdc-chip__checkmark-svg" viewBox="-2 -3 30 30">
					<path
						class="mdc-chip__checkmark-path"
						fill="none"
						stroke="black"
						d="M1.73,12.91 8.1,19.28 22.79,4.59"
					/>
				</svg>
			</div>
		);
	}
}
//------------------------------------------------------------------------------
Chips.Chip = Chip;
Chips.Icon = ChipIcon;
Chips.Text = ChipText;
Chips.Checkmark = ChipCheckmark;
export default Chips;
//------------------------------------------------------------------------------
