//------------------------------------------------------------------------------
import MElevation from 'preact-material-components/Elevation';
import 'preact-material-components/Elevation/style.css';
//------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------------------
export default class Elevation extends MElevation {
	materialDom(allprops) {
		const { children, tag, z, ...props } = allprops;
		const className = z ? 'mdc-elevation--z' + z : '';
		const JSXTag = tag ? tag : 'p';

		return (
			<JSXTag {...props} className={className} ref={this.setControlRef}>
				{children}
			</JSXTag>
		);
	}
}
//------------------------------------------------------------------------------
