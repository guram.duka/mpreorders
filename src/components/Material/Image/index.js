//------------------------------------------------------------------------------
import { LRUMap } from 'lru_map';
import { Component } from 'preact';
import { imgReq, imgUrl, imgKey, bfetch } from '../../../backend';
import { nullLink } from '../../../const';
import { randomInteger, prevent, isEdge } from '../../../lib/util';
import root from '../../../lib/root';
import { webpRuntimeInitialize, webpNativeSupport, webp2png } from '../../../lib/webp';
import style from './style.scss';
//------------------------------------------------------------------------------
function createElement(tag) {
	return root.document
		? root.document.createElement(tag)
		: {};
}
//------------------------------------------------------------------------------
function headAppendChild(element) {
	return root.document
		? root.document.head.appendChild(element)
		: element;
}
//------------------------------------------------------------------------------
function getElementById(key) {
	return root.document
		? root.document.getElementById(key)
		: undefined;
}
//------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------------------
class Image extends Component {
	static __id = 0
	static __cacheId = '$__image_cache__#'
	static __cacheLRU = (() => {
		const c = new LRUMap(400);
		c.shift = function () {
			const entry = LRUMap.prototype.shift.call(this);
			const [key] = entry;
			const e = getElementById(key);

			if (e)
				e.remove();

			return entry;
		};
		return c;
	})()

	constructor() {
		super();

		this.idn = ++Image.__id;
		this.ids = `$__image__#${this.idn}`;
	}

	componentWillMount() {
		let cache = getElementById(Image.__cacheId);

		if (cache) {
			cache.refCount++;
		}
		else {
			cache = createElement('style');
			cache.id = Image.__cacheId;
			cache.refCount = 1;
			headAppendChild(cache);

			// if (cache.sheet) {
			// 	cache.sheet.insertRule(`.nopic { background-image: url(/assets/nopic.svg) }`);
			// 	cache.sheet.insertRule(`.picld { background-image: url(/assets/loading-process.svg) }`);
			// }
		}
	}

	componentDidMount() {
		this.mounted = true;
		this.waitStyleComputed();
		root.addEventListener('resize', this.windowOnResize);
	}

	componentWillReceiveProps(props) {
		this.waitStyleComputed(props);
	}

	componentWillUnmount() {
		clearTimeout(this.timerId);
		delete this.mounted;
		root.removeEventListener('resize', this.windowOnResize);

		const cache = getElementById(Image.__cacheId);

		if (--cache.refCount === 0)
			cache.remove();
	}

	mount(props) {
		this.setState({ imageClass: undefined }, this.waitStyleComputed);
	}

	windowOnResize = e => this.__windowOnResize(e)
	__windowOnResize(e) {
		this.waitStyleComputed();
	}

	// findCacheEntry(key) {
	// 	return getElementById(key);
	// 	// return root.document.evaluate(
	// 	// 	`style[@key='${key}']`,
	// 	// 	root.document.head,
	// 	// 	null,
	// 	// 	XPathResult.FIRST_ORDERED_NODE_TYPE,
	// 	// 	null).singleNodeValue;
	// }

	insertCacheEntry(key, url) {
		const { __cacheLRU } = Image;
		let entry = __cacheLRU.get(key);

		if (!entry) {
			entry = createElement('style');
			entry.id = key;
			headAppendChild(entry);

			if (entry.sheet) {
				//const index = cache.sheet.cssRules.length;
				entry.sheet.insertRule(`.${key} { background-image: url('${url}'); }`);
			}

			__cacheLRU.set(key, true);
		}

		if (this.state.imageClass !== key)
			this.setState({ imageClass: key });
	}

	static __rpx = /px$/i;
	static __rpxi = i => ~~i.replace(Image.__rpx, '')

	waitStyleComputed = props => this.__waitStyleComputed(props)
	__waitStyleComputed(props) {
		props = props ? props : this.props;

		if (!this.mounted || (props.display !== undefined && !!props.display === false))
			return;

		if (!isEdge() && !webpRuntimeInitialize(this.waitStyleComputed))
			return;

		const image = getElementById(this.ids);
		let loop = true;

		if (image) {
			let { width, height } = root.getComputedStyle(image);
			//const wpx = width.search(Image.rpx) !== -1;
			//const hpx = height.search(Image.rpx) !== -1;

			width = Image.__rpxi(width);
			height = Image.__rpxi(height);

			if (width > 0 && height > 0) {
				const { link, lossless, quality, noresize } = props;

				if (link && link !== nullLink) {
					const r = { u: link, w: width, h: height };

					if (noresize)
						r.w = r.h = undefined;

					if (quality)
						r.quality = quality;

					if (lossless) {
						r.quality = undefined;
						r.lossless = true;
					}

					if (isEdge())
						r.type = 'png';

					const imageKey = imgKey(r);

					let entry = Image.__cacheLRU.get(imageKey);

					if (entry) {
						if (this.state.imageClass !== imageKey)
							this.setState({ imageClass: imageKey });
					}
					else if (webpNativeSupport || isEdge()) {
						this.insertCacheEntry(imageKey, imgUrl(r));
						this.setState({ imageClass: imageKey });
					}
					else {
						bfetch(
							imgReq(r),
							result => this.insertCacheEntry(imageKey, webp2png(result)),
							error => this.setState({ imageClass: 'nopic' })
						);
						this.setState({ imageClass: 'picld' });
					}
					loop = false;
				}
				else {
					this.setState({ imageClass: 'nopic' });
					loop = false;
				}
			}
			//else if (wpx && hpx)
			//	loop = false;
		}

		if (loop)
			this.timerId = root.setTimeout(this.waitStyleComputed, 10);
	}

	render(allprops, { imageClass }) {
		const { className, inline, display, tag, children, ...props } = allprops;
		const classNameString = [
			style.image,
			inline ? 'mdc-display-inline' : '',
			className ? className : '',
			imageClass === 'picld'
				? style.picld + ' ' + style['spin' + randomInteger(0, 7)]
				: imageClass === 'nopic'
					? style.nopic
					: imageClass ? imageClass : '',
			display !== undefined && !!display === false
				? style.dn : ''
		].join(' ');

		const JSXTag = tag ? tag : 'div';

		return (
			<JSXTag {...props} id={this.ids} className={classNameString}>
				{children}
			</JSXTag>);
	}
}
//------------------------------------------------------------------------------
class Magnifier extends Component {
	show = link => this.setState({ link })

	close = e => {
		this.setState({ link: undefined });
		return prevent(e);
	}

	componentWillMount() {
		this.componentWillReceiveProps(this.props);
	}

	componentWillReceiveProps(props) {
		this.iProps = {
			lossless: props.lossless,
			quality: props.quality ? props.quality : 100,
			noresize: true,
			onClick: this.close
		};
	}

	render(props, state) {
		const { link } = state;
		const cn = [
			style.magnifier,
			props.className ? props.className : '',
			link === undefined ? style.dn : ''
		].join(' ');

		return (
			<div className={cn}>
				<Image {...this.iProps}
					display={link !== undefined}
					link={link}
				/>
			</div>);
	}
}
//------------------------------------------------------------------------------
Image.Magnifier = Magnifier;
export default Image;
//------------------------------------------------------------------------------
