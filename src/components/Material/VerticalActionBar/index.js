//------------------------------------------------------------------------------
import { Component } from 'preact';
import generateThemeClass from 'preact-material-components/themeUtils/generateThemeClass';
import MIcon from 'preact-material-components/Icon';
import 'preact-material-components/Icon/style.css';
import MFab from 'preact-material-components/Fab';
import 'preact-material-components/Fab/style.css';
import { prevent, isEdge } from '../../../lib/util';
import root from '../../../lib/root';
import style from './style.scss';
//------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------------------
class Icon extends MIcon {
	constructor() {
		super();
		this.componentName = 'fab__icon';
	}

	materialDom(props) {
		const classes = ['material-icons'];
		// CardActionIcon sends className
		if (props.className)
			classes.push(props.className);

		return (
			<i {...props} className={classes.join(' ')}>
				{props.children}
			</i>
		);
	}
}
//------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------------------
class Fab extends MFab {
	constructor() {
		super();
		this.mdcProps.push('disabled', 'disabled-color', 'disabled-background');
	}

	materialDom(props) {
		const { className, href, children } = props;
		const classNames = [];

		for (const themeProp of this.themeProps)
			if (themeProp in props && props[themeProp] !== false)
				classNames.push(generateThemeClass(themeProp));

		if (className)
			classNames.push(className);

		const JSXTag = href ? 'a' : 'button';

		return (
			<JSXTag
				ref={this.setControlRef}
				{...props}
				className={classNames.join(' ')}
			>
				{children}
			</JSXTag>
		);
	}
}
//------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------------------
class Vab extends Component {
	static __id = 0

	constructor() {
		super();

		this.__id = '$__vab__#' + (++Vab.__id);
	}

	componentWillMount() {
		this.componentWillReceiveProps(this.props);
	}

	componentWillReceiveProps(props) {
		if (this.state.isPopup)
			this.handlePopupStateChange(false);

		this.stateChangeCallback();
	}

	componentWillUnmount() {
		this.handlePopupStateChange(false);
	}

	stateChangeCallback = () => {
		if (!this.props.fixed)
			this.handlePopupStateChange(this.state.isPopup);
	}

	handleOutsideClick = e => this.__handleOutsideClick(e)
	__handleOutsideClick(e) {
		const { __id } = this;
		// ignore clicks on the component itself
		for (let t = e.target; t && t !== e.currentTarget; t = t.parentNode)
			if (t.id === __id)
				return;

		this.handleClickPopup(e);
	}

	handlePopupStateChange = isPopup => {
		const p = isPopup ? 'add' : 'remove';
		const f = root.document[`${p}EventListener`];

		for (const e of ['mousedown', 'keydown', 'touchstart'])
			f(e, this.handleOutsideClick, false);
	}

	handleClickPopup = e => {
		this.setState(
			{ isPopup: !this.state.isPopup },
			this.stateChangeCallback
		);
		//return prevent(e);
	}

	render(props, state) {
		const { fixed, disabled, display, ...allprops } = props;
		const popuper = fixed ? undefined : (
			<Fab
				className={style.vis}
				secondary disabled={disabled}
				onClick={this.handleClickPopup}
			>
				<Fab.Icon>
					{state.isPopup ? 'remove' : 'add'}
				</Fab.Icon>
			</Fab>);

		const className = [
			style.bar,
			fixed || state.isPopup ? '' : style.invis,
			display || Object.keys(props).indexOf('display') === -1 ? '' : style.dn,
			className
		].join(' ');

		return (
			<div {...allprops} id={this.id} className={className}>
				{props.children}
				{popuper}
			</div>);
	}
}
//------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------------------
class ScrollUp extends Component {
	static goUp = e => {
		let child = root.document.getElementById('app-kiosk-scroll');

		if (!child)
			child = root;

		if (isEdge())
			child.scrollTop = 0;
		else if (child.scrollTo)
			child.scrollTo({
				top: 0,
				left: 0,
				behavior: 'instant'
			});

		return prevent(e);
	}

	render(props) {
		return (
			<Fab {...props} onClick={ScrollUp.goUp}>
				<Icon>arrow_upward</Icon>
			</Fab>
		);
	}
}
//------------------------------------------------------------------------------
Vab.Fab = Fab;
Vab.Fab.Icon = Icon;
Vab.ScrollUp = ScrollUp;
//------------------------------------------------------------------------------
export default Vab;
//------------------------------------------------------------------------------
