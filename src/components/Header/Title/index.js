//------------------------------------------------------------------------------
import Component from '../../Component';
import TopAppBar from 'preact-material-components/TopAppBar';
import 'preact-material-components/TopAppBar/style.css';
import { headerTitleStorePath } from '../../../const';
import style from './style.scss';
//------------------------------------------------------------------------------
export default class Title extends Component {
	storeConfig = [
		{ path: headerTitleStorePath, alias: 'title' }
	]

	render(props, { title }) {
		// Zero Width Space https://unicode-table.com/ru/200B/
		// Need for right positioning when title.length === 0
		return (
			<TopAppBar.Title class={style.title}>
				&#x200B;
				{title}
			</TopAppBar.Title>);
	}
}
//------------------------------------------------------------------------------
