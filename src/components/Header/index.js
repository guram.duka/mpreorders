//------------------------------------------------------------------------------
import Component from '../Component';
import { route } from 'preact-router';
import TopAppBar from 'preact-material-components/TopAppBar';
import 'preact-material-components/TopAppBar/style.css';
import Drawer from 'preact-material-components/Drawer';
import 'preact-material-components/Drawer/style.css';
import List from 'preact-material-components/List';
import 'preact-material-components/List/style.css';
import Spinner from './Spinner';
import Title from './Title';
import WatchClickOutside from '../WatchClickOutside';
import {
	headerSearchStorePath,
	darkThemeStorePath,
	mdcThemeDarkClass,
	kioskStorePath
} from '../../const';
import { prevent } from '../../lib/util';
import root from '../../lib/root';
import store from '../../lib/rstore';
//import style from './style.scss';
//------------------------------------------------------------------------------
// disable animation, due to slow on mobile
// import { cssClasses as dialogCssClasses } from '@material/dialog/constants';
// import { cssClasses as drawerCssClasses } from '@material/drawer/temporary/constants';
// dialogCssClasses.ANIMATING = 'mdc-dialog--noanimating';
// drawerCssClasses.ANIMATING = 'mdc-drawer--noanimating';
//------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------------------
export default class Header extends Component {
	// static decapitalizeR = /([a-z\xE0-\xFF])([A-Z\xC0\xDF])/g
	// static decapitalizeP = '$1.$2'

	// static decapitalize(s) {
	// 	// replace for example searchOrderDirection to search.order.direction
	// 	return s.replace(Header.decapitalizeR, Header.decapitalizeP).toLowerCase();
	// }

	storeConfig() {
		const config = [
			{ path: headerSearchStorePath, alias: 'searchStorePath' },
			{ path: darkThemeStorePath, alias: 'darkThemeEnabled' },
			{ path: kioskStorePath, alias: 'kiosk' },
			{ path: 'auth', alias: 'auth' }
		];

		const p = store.get(headerSearchStorePath);

		if (p)
			config.push(
				{ path: p + '.applied', alias: 'searchApplied' }
			);

		return config;
	}

	willMount() {
		this.setTheme(this.state);
	}

	willUpdate(props, state) {
		this.setTheme(state);
	}

	setTheme(state) {
		if (root.document) {
			const { classList } = root.document.body;

			if (state.darkThemeEnabled)
				classList.contains(mdcThemeDarkClass) || classList.add(mdcThemeDarkClass);
			else
				classList.contains(mdcThemeDarkClass) && classList.remove(mdcThemeDarkClass);
		}
	}

	linkTo = path => ({
		href: path,
		onClick: e => {
			this.closeDrawer(e);
			route(path);
			return prevent(e);
		}
	})

	goHome = this.linkTo('/')
	goProfile = this.linkTo('/profile')
	goLogin = this.linkTo('/login')
	goCategories = this.linkTo('/categories')
	//goProducts = this.linkTo('/products')
	goFavorites = this.linkTo('/favorites')
	goOrders = this.linkTo('/orders')
	goCart = this.linkTo('/cart')
	goSettings = this.linkTo('/settings')
	goBack = e => root.history.back()

	openDrawer = e => {
		this.setState({ drawerOpened: true });
		return prevent(e);
	}

	closeDrawer = e => {
		this.setState({ drawerOpened: false });
		return prevent(e);
	}

	drawerClickOutside = e => this.__drawerClickOutside(e)
	__drawerClickOutside = e => {
		if (this.state.drawerOpened)
			this.closeDrawer(e);
	}

	toggleSearch = e => this.__toggleSearch(e)
	__toggleSearch(e) {
		store.toggle(this.state.searchStorePath + '.visible');
		return prevent(e);
	}

	searchIcon() {
		const { searchStorePath, searchApplied, kiosk } = this.state;

		if (searchStorePath && !kiosk) {
			const iconStyle = ['material-icons',
				searchApplied ? 'blink' : ''
			].join(' ').trim();

			return (
				<TopAppBar.Icon navigation
					onClick={this.toggleSearch}
					className={iconStyle}
				>
					search
				</TopAppBar.Icon>);
		}
	}

	render(props, state) {
		const { auth, kiosk, drawerOpened } = state;
		const authorized = auth && auth.authorized;
		const drawer = (
			<Drawer
				dismissible
				open={drawerOpened}
				onClose={this.closeDrawer}
			>
				<WatchClickOutside onClickOutside={this.drawerClickOutside} />
				{/*<Drawer.DrawerHeader>
					&#x200B;
				</Drawer.DrawerHeader>*/}
				<Drawer.DrawerContent>
					<Drawer.DrawerItem {...this.goHome}>
						<List.ItemGraphic>home</List.ItemGraphic>
						Начало
					</Drawer.DrawerItem>
					{kiosk ? undefined : <Drawer.DrawerItem {...this.goCategories}>
						<List.ItemGraphic>view_stream</List.ItemGraphic>
						Категории
					</Drawer.DrawerItem>}
					{/*<Drawer.DrawerItem {...this.goProducts}>
						<List.ItemGraphic>view_list</List.ItemGraphic>
						Каталог
					</Drawer.DrawerItem>*/}
					{authorized ? <Drawer.DrawerItem {...this.goFavorites}>
						<List.ItemGraphic>favorite</List.ItemGraphic>
						Избранное
					</Drawer.DrawerItem> : undefined}
					{authorized ? <Drawer.DrawerItem {...this.goCart}>
						<List.ItemGraphic>shopping_cart</List.ItemGraphic>
						Корзина
					</Drawer.DrawerItem> : undefined}
					{authorized ? <Drawer.DrawerItem {...this.goOrders}>
						<List.ItemGraphic>reorder</List.ItemGraphic>
						Заказы
					</Drawer.DrawerItem> : undefined}
					{kiosk ? undefined : <Drawer.DrawerItem {...this.goSettings}>
						<List.ItemGraphic>settings</List.ItemGraphic>
						Настройки
					</Drawer.DrawerItem>}
					{kiosk ? undefined : <Drawer.DrawerItem {...(authorized ? this.goProfile : this.goLogin)}>
						<List.ItemGraphic>{authorized ? 'verified_user' : 'account_circle'}</List.ItemGraphic>
						{authorized ? 'Профиль' : 'Вход/Регистрация'}
					</Drawer.DrawerItem>}
				</Drawer.DrawerContent>
			</Drawer>);

		return (
			<div>
				{drawer}
				<TopAppBar fixed>
					<TopAppBar.Row>
						<TopAppBar.Section align-start>
							{state.kiosk ?
								<TopAppBar.Icon navigation onClick={this.goBack}>
									arrow_back
								</TopAppBar.Icon> : undefined}
							<TopAppBar.Icon navigation onClick={this.openDrawer}>
								menu
							</TopAppBar.Icon>
							<Title />
						</TopAppBar.Section>
						{/*<TopAppBar.Section shrink-to-fit>
							<Title />
						</TopAppBar.Section>*/}
						<TopAppBar.Section align-end>
							<Spinner />
							{this.searchIcon()}
							{/*<TopAppBar.Icon navigation>
								more_vert
							</TopAppBar.Icon>*/}
						</TopAppBar.Section>
					</TopAppBar.Row>
				</TopAppBar>
			</div>);
	}
}
//------------------------------------------------------------------------------
