//------------------------------------------------------------------------------
import Component from '../../Component';
import TopAppBar from 'preact-material-components/TopAppBar';
import 'preact-material-components/TopAppBar/style.css';
import { loaderSpinnerStorePath } from '../../../const';
import style from './style.scss';
//------------------------------------------------------------------------------
export default class Spinner extends Component {
	storeConfig = [
		{ path: loaderSpinnerStorePath, alias: 'active' }
	]

	render(props, { active }) {
		const cn = `material-icons ${active ? style.spin : style.dn}`;

		return (
			<TopAppBar.Icon className={cn}>
				refresh
			</TopAppBar.Icon>
		);
	}
}
//------------------------------------------------------------------------------
