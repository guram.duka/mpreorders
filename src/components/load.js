//------------------------------------------------------------------------------
import { loaderSpinnerStorePath } from '../const';
import { transform } from '../lib/util';
import store from '../lib/rstore';
//------------------------------------------------------------------------------
function incSpin() {
	store.inc(loaderSpinnerStorePath);
	store.publish();
}
//------------------------------------------------------------------------------
function decSpin() {
	store.dec(loaderSpinnerStorePath);
	const counter = ~~store.get(loaderSpinnerStorePath);

	if (counter <= 0)
		store.delete(loaderSpinnerStorePath);
}
//------------------------------------------------------------------------------
export function successor(...successors) {
	return result => {
		decSpin();

		result = transform(result);

		for (const success of successors)
			success && success(result);
	};
}
//------------------------------------------------------------------------------
export function failer(...failers) {
	return error => {
		decSpin();

		for (const fail of failers)
			fail && fail(error);
	};
}
//------------------------------------------------------------------------------
export function starter(...starters) {
	return opts => {
		incSpin();

		for (const start of starters)
			start && start(opts);
	};
}
//------------------------------------------------------------------------------
