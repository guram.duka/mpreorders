//------------------------------------------------------------------------------
import { Component } from 'preact';
import root from '../../lib/root';
//------------------------------------------------------------------------------
export default class WatchClickOutside extends Component {
	static __id = 0

	constructor() {
		super();

		this.__id = '$__wco__#' + (++WatchClickOutside.__id);
	}

	componentWillMount() {
		if (root.document)
			root.document.body.addEventListener('click', this.handleClick);
	}

	componentWillUnmount() {
		// remember to remove all events to avoid memory leaks
		if (root.document)
			root.document.body.removeEventListener('click', this.handleClick);
	}

	handleClick = e => this.__handleClick(e)
	__handleClick(e) {
		const { onClickOutside } = this.props; // get click outside callback

		// if there is no proper callback - no point of checking
		if (onClickOutside) {
			const { __id } = this;
			const child = root.document.getElementById(__id);
			const { parentNode } = child;

			for (let t = e.target; t && t !== e.currentTarget; t = t.parentNode)
				if (t === child || t === parentNode) {
					// clicked inside - do nothing
					return;
				}

			// clicked outside - fire callback
			onClickOutside(e);
		}
	}

	render(props, state) {
		return (
			<div id={this.__id}>
				{props.children}
			</div>
		);
	}
}
//------------------------------------------------------------------------------
