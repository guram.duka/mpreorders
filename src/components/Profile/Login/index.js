//------------------------------------------------------------------------------
import { route } from 'preact-router';
import Component from '../../Component';
import { bfetch } from '../../../backend';
import { SHA256 } from 'jshashes';
import Button from 'preact-material-components/Button';
import 'preact-material-components/Button/style.css';
import TextField from 'preact-material-components/TextField';
import 'preact-material-components/TextField/style.css';
import Snackbar from 'preact-material-components/Snackbar';
import 'preact-material-components/Snackbar/style.css';
import style from './style.scss';
import { headerTitleStorePath, headerSearchStorePath } from '../../../const';
import { successor, failer, starter } from '../../load';
import { plinkRoute } from '../../../lib/util';
import store from '../../../lib/rstore';
//------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------------------
export default class Login extends Component {
	storeConfig = [
		{ path: headerTitleStorePath, mount: 'Авторизация' },
		{ path: headerSearchStorePath, mount: undefined },
		{ path: 'auth', alias: 'auth', defaultValue: {} }
	]

	didSetState() {
		const { auth } = this.state;

		if (auth && auth.authorized)
			route('/', true);
	}

	fieldInput = field => e => this[field] = e.target.value

	userFieldInput = this.fieldInput('user')
	passFieldInput = this.fieldInput('pass')

	login = e => {
		if (this.loginCallId)
			return;

		const { state } = this;
		const { auth } = state;
		const user = this.user === undefined ? auth.user : this.user;
		const pass = this.pass === undefined ? auth.pass : this.pass;

		this.loginCallId = bfetch(
			{
				auth,
				method: 'PUT',
				r: {
					m: 'auth',
					f: 'login',
					r: {
						user,
						hash: (new SHA256()).hex(pass).toUpperCase()
					}
				}
			},
			successor(result => {
				delete this.loginCallId;

				if (result.authorized) {
					if (result.profile && result.profile.birthday)
						result.profile.birthday = new Date(result.profile.birthday);

					store.set('auth', { ...result, user, pass });
					route('/', true);
				}
				else
					this.showError('Ошибка авторизации');
			}),
			failer(error => {
				delete this.loginCallId;
				this.showError('Ошибка авторизации');
			}),
			starter()
		);
	}

	linkTo = path => plinkRoute(path)

	goRegistration = this.linkTo('/registration')

	snackbarRef = e => this.snackbar = e;

	showError(e) {
		if (e && this.snackbar)
			this.snackbar.MDComponent.show({ message: e });
	}

	render(props, state) {
		const { auth } = state;

		const user = this.user !== undefined
			? this.user
			: auth.user;
		const pass = this.pass !== undefined
			? this.pass
			: auth.pass;

		const textFieldOuterStyle = { width: '100%' };

		const userField = (
			<TextField
				outerStyle={textFieldOuterStyle}
				autocomplete="off"
				required
				label="Имя пользователя или E-mail"
				trailingIcon="perm_identity"
				type="text"
				value={user}
				onInput={this.userFieldInput}
			/>);

		const passField = (
			<TextField
				outerStyle={textFieldOuterStyle}
				autocomplete="off"
				required
				label="Пароль"
				trailingIcon="security"
				type="password"
				value={pass}
				onInput={this.passFieldInput}
			/>);

		const loginButton = (
			<Button unelevated
				className={style.button}
				onClick={this.login}
			>
				<Button.Icon>arrow_forward</Button.Icon>
				Войти
			</Button>);

		const regButton = (
			<Button unelevated
				className={style.button}
				onClick={this.goRegistration}
			>
				<Button.Icon>person_add</Button.Icon>
				Регистрация
			</Button>);

		return (
			<div class={style.login}>
				<Snackbar ref={this.snackbarRef} style={style.snackbar} />

				<form onSubmit={false}>
					{userField}
					{passField}
				</form>

				{loginButton}
				{regButton}

			</div>);
	}
}
//------------------------------------------------------------------------------
