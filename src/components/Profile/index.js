//------------------------------------------------------------------------------
import DOMPurify from 'dompurify';
import { route } from 'preact-router';
import Component from '../Component';
import { bfetch } from '../../backend';
import { successor, failer, starter } from '../load';
import { SHA256 } from 'jshashes';
import Button from 'preact-material-components/Button';
import 'preact-material-components/Button/style.css';
import TextField from 'preact-material-components/TextField';
import 'preact-material-components/TextField/style.css';
import Radio from 'preact-material-components/Radio';
import 'preact-material-components/Radio/style.css';
import Checkbox from 'preact-material-components/Checkbox';
import 'preact-material-components/Checkbox/style.css';
import FormField from 'preact-material-components/FormField';
import 'preact-material-components/FormField/style.css';
import 'preact-material-components/List/style.css';
import Snackbar from 'preact-material-components/Snackbar';
import 'preact-material-components/Snackbar/style.css';
import LayoutGrid from 'preact-material-components/LayoutGrid';
import 'preact-material-components/LayoutGrid/style.css';
import Typography from 'preact-material-components/Typography';
import 'preact-material-components/Typography/style.css';
import style from './style.scss';
import {
	headerTitleStorePath,
	headerSearchStorePath,
	textFieldHelperTextClasses,
	termsOfUseAndPrivacyPolicy
} from '../../const';
import store from '../../lib/rstore';
import strftime from '../../lib/strftime';
import { plinkRoute, prevent, deepEqual } from '../../lib/util';
//------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------------------
export default class Profile extends Component {
	state = {
		pushButtonName: 'Сохранить',
		pushFunction: 'profile_push',
		pushError: 'Ошибка записи',
		notAuthorizedRouteUrl: '/login',
		notUseTerms: true,
		header: 'Профиль'
	}

	storeConfig() {
		return [
			{ path: headerTitleStorePath, mount: this.state.header },
			{ path: headerSearchStorePath, mount: undefined },
			{ path: 'auth', alias: 'auth', defaultValue: {} }
		];
	}

	willMount() {
		this.pull();
	}

	didSetState() {
		const { auth, authorizedRouteUrl, notAuthorizedRouteUrl } = this.state;
		const url = auth.authorized
			? authorizedRouteUrl : notAuthorizedRouteUrl;

		if (url)
			route(url, true);
	}

	allFields = {}
	requiredFields = []
	profileFields = ['birthday', 'gender', 'family', 'fname', 'sname', 'phone', 'daddr1', 'daddr2']

	clearTypedFields = () => {
		for (const field of Object.keys(this.allFields))
			delete this[field];
	}

	fieldInputValidator = (field, display) => {
		this.allFields[field] = display;

		return e => {
			this.field = field;
			this[field] = e.target.value;
			this['validate' + field.substr(0, 1).toUpperCase() + field.substr(1) + 'Field']();
		};
	}

	// change event occurs when the value of an element has been changed
	//userFieldChange = e => this.user = e.target.value;
	// input event occurs immediately after the value of an element has changed
	userFieldInput = this.fieldInputValidator('user', 'Имя пользователя')
	emailFieldInput = this.fieldInputValidator('email', 'Электронная почта')
	passFieldInput = this.fieldInputValidator('pass', 'Пароль')
	pass2FieldInput = this.fieldInputValidator('pass2', 'Подтверждение пароля')
	birthdayFieldInput = this.fieldInputValidator('birthday', 'День рождения')
	genderFieldInput = this.fieldInputValidator('gender', 'Пол')
	phoneFieldInput = this.fieldInputValidator('phone', 'Контактный телефон')
	familyFieldInput = this.fieldInputValidator('family', 'Фамилия')
	fnameFieldInput = this.fieldInputValidator('fname', 'Имя')
	snameFieldInput = this.fieldInputValidator('sname', 'Отчество')
	daddr1FieldInput = this.fieldInputValidator('daddr1', 'Первичный адрес доставки')
	daddr2FieldInput = this.fieldInputValidator('daddr2', 'Вторичный адрес доставки')

	validateAllFields(state, callback) {
		const combo = { ...this.state, ...state };
		const notFilled = {};
		let v = true;

		for (const field of Object.keys(this.allFields)) {
			const fe = field + 'Error';
			const fieldError = combo[fe];

			if (fieldError)
				v = false;
			else if (this.requiredFields.includes(field)) {
				if (this[field] === undefined || this[field].trim().length === 0) {
					notFilled[fe] = 'необходимо заполнить';//this.allFields[field] + ' - необходимо заполнить';
					v = false;
				}
			}
		}

		this.setState({ valid: v, ...state, ...notFilled }, callback);
	}

	// check non printable symbols, allow only Basic Latin and Cyrillic, exclude space
	// http://kourge.net/projects/regexp-unicode-block
	userPattern = /[\u0021-\u007e,\u00a0-\u00ff,\u0400-\u04FF,\u0500-\u052F]/g

	validateUserField(ret) {
		//const { state } = this;
		const r = { userError: undefined };

		if (!this.user)
			r.userError = 'Не заполнено имя пользователя';
		else if (!this.userPattern.test(this.user))
			r.userError = 'Недопустимые символы в имени пользователя';

		if (ret)
			return r;

		// (state.isReg || (state.auth && state.auth.authorized)) && bfetch(
		// 	{ r: { m: 'auth', f: 'check', r: { user: this.user } } },
		// 	result => {
		// 		const userUC = this.user.toUpperCase();
		// 		const rserUC = result.user.toUpperCase();

		// 		if (userUC === rserUC && result.exists)
		// 			this.validateAllFields({
		// 				userError: 'Пользователь с таким именем уже существует'
		// 			});
		// 	}
		// );

		this.validateAllFields(r);
	}

	reValidEmail = new RegExp((() => {
		const sQtext = '[^\\x0d\\x22\\x5c\\x80-\\xff]';
		const sDtext = '[^\\x0d\\x5b-\\x5d\\x80-\\xff]';
		const sAtom = '[^\\x00-\\x20\\x22\\x28\\x29\\x2c\\x2e\\x3a-\\x3c\\x3e\\x40\\x5b-\\x5d\\x7f-\\xff]+';
		const sQuotedPair = '\\x5c[\\x00-\\x7f]';
		const sDomainLiteral = `\\x5b(${sDtext}|${sQuotedPair})*\\x5d`;
		const sQuotedString = `\\x22(${sQtext}|${sQuotedPair})*\\x22`;
		const sDomainRef = sAtom;
		const sSubDomain = `(${sDomainRef}|${sDomainLiteral})`;
		const sWord = `(${sAtom}|${sQuotedString})`;
		const sDomain = `${sSubDomain}(\\x2e${sSubDomain})*`;
		const sLocalPart = `${sWord}(\\x2e${sWord})*`;
		const sAddrSpec = `${sLocalPart}\\x40${sDomain}`; // complete RFC822 email address spec
		const sValidEmail = `^${sAddrSpec}$`; // as whole string
		return sValidEmail;
	})())

	validateEmail(email) {
		return email && email.length <= 127 && this.reValidEmail.test(email);
	}

	validateEmailField(ret) {
		const r = {
			emailError: this.validateEmail(this.email) ? undefined : 'Не заполнен E-mail'
		};

		if (ret)
			return r;

		this.validateAllFields(r);
	}

	//strongPassPattern = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\\$%\\^&\\*])(?=.{8,})/g
	//strongPassPattern = /^(?=.*\d)(?=.*[!@#$%^&*()-_+=.,:;?|~\\/`\'\"\[\]{}])(?=.*[a-z])(?=.*[A-Z]).{8,}/g
	//mediumPassPattern = /^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{6,})/g

	validatePassField(ret) {
		const { pass } = this;
		const r = { passError: undefined };
		//const valid = this.strongPassPattern.test(pass);

		if (pass.length < 8)
			r.passError = 'Пароль должен содержать минимум 8 символов';//'Your password must be at least 8 characters';
		else if (pass.search(/[a-z]/g) < 0)
			r.passError = 'Пароль должен содержать минимум одну строчную [a-z] букву';//'Your password must contain at least one lower case letter';
		else if (pass.search(/[A-Z]/g) < 0)
			r.passError = 'Пароль должен содержать минимум одну заглавную [A-Z] букву';//'Your password must contain at least one upper case letter';
		else if (pass.search(/[0-9]/g) < 0)
			r.passError = 'Пароль должен содержать минимум одну цифру [0-9]';//'Your password must contain at least one digit';
		else if (pass.search(/[!@#$%^&*()-_+=.,:;?|~\\/`\'"\[\]{}]/g) < 0)
			r.passError = 'Пароль должен содержать минимум один специальный символ';//'Your password must contain at least one special symbol';

		if (ret)
			return r;

		this.validateAllFields({ ...r, ...this.validatePass2Field(true) });
	}

	validatePass2Field(ret) {
		const { pass, pass2 } = this;
		const valid = pass === pass2;
		const r = { pass2Error: valid ? undefined : 'Пароли не совпадают' };

		if (ret)
			return r;

		this.validateAllFields(r);
	}

	validateBirthdayField(ret) {
		const { birthday } = this;
		// http://html5pattern.com/Dates
		//const pattern = /(0[1-9]|1[0-9]|2[0-9]|3[01]).(0[1-9]|1[012]).[0-9]{4}/g;
		let valid = false;

		if (birthday && birthday.constructor === String || birthday instanceof String) {
			//const p = birthday.split('.');
			//parsed = Date.parse(p[2] + '-' + p[1] + '-' + p[0]);
			const parsed = Date.parse(birthday);
			valid = !Number.isNaN(parsed);
		}

		const r = {
			birthdayError: valid ? undefined : 'Неверный формат дня рождения'
		};

		if (ret)
			return r;

		this.validateAllFields(r);
	}

	validateGenderField(ret) {
		const r = {
			genderError: undefined
		};

		if (ret)
			return r;

		this.validateAllFields(r);
	}

	validateFamilyField(ret) {
		const r = {
			familyError: undefined
		};

		if (ret)
			return r;

		this.validateAllFields(r);
	}

	validateFnameField(ret) {
		const r = {
			fnameError: undefined
		};

		if (ret)
			return r;

		this.validateAllFields(r);
	}

	validateSnameField(ret) {
		const r = {
			snameError: undefined
		};

		if (ret)
			return r;

		this.validateAllFields(r);
	}

	//phonePattern = /(\+?\d[- .()]*){7,13}/g
	phonePattern = /^((8|\+7)[- ]?)?(\(?\d{3}\)?[- ]?)?[\d\- ]{7,10}$/
	phoneMask = '+d (ddd) dd-dd-ddd'

	maskPhoneField = e => {
		// const value = e.target.value;

		// if (e.keyCode === 8) {
		// 	// ignore backspace
		// }
		// else if (/^\d{4}$/.test(value)) {
		// 	e.target.value = value + '-';
		// }
		// else if (/^\d{4}-\d{2}$/.test(value)) {
		// 	e.target.value = value + '-';
		// }
	}

	validatePhoneField(ret) {
		const { phone } = this;
		const valid = this.phonePattern.test(phone);
		const r = {
			phoneError: valid ? undefined : 'Неверный телефонный номер'
		};

		if (ret)
			return {};

		this.validateAllFields(r);
	}

	validateDaddr1Field(ret) {
		const r = {
			daddr1Error: undefined
		};

		if (ret)
			return r;

		this.validateAllFields(r);
	}

	validateDaddr2Field(ret) {
		const r = {
			daddr2Error: undefined
		};

		if (ret)
			return r;

		this.validateAllFields(r);
	}

	push = e => {
		if (this.pushCallId)
			return;

		const { state, pushSuccessorHook } = this;
		const { pushFunction, pushError } = state;
		const auth = state.auth2 ? state.auth2 : state.auth;
		const user = this.user === undefined ? auth.user : this.user;
		const email = this.email === undefined ? auth.email : this.email;
		const pass = this.pass === undefined ? auth.pass : this.pass;
		const r = {
			user,
			hash: (new SHA256()).hex(pass).toUpperCase()
		};

		if (email)
			r.email = email;

		const profile = {};

		for (const n of this.profileFields)
			if (this[n] !== undefined) {
				profile[n] = this[n];
				r.profile = profile;
			}

		if (profile.birthday) {
			const parsed = Date.parse(profile.birthday);

			if (Number.isNaN(parsed))
				delete profile.birthday;
			else
				profile.birthday = new Date(parsed);
		}

		this.pushCallId = bfetch(
			{
				auth,
				method: 'PUT',
				r: {
					m: 'auth',
					f: pushFunction,
					r
				}
			},
			successor(result => {
				delete this.pushCallId;

				if (result.profile && result.profile.birthday)
					result.profile.birthday = new Date(result.profile.birthday);

				pushSuccessorHook && this.pushSuccessorHook(result);
				this.clearTypedFields();
				store.set('auth', { ...r, ...result, pass });
				this.setState({ valid: false });
			}),
			failer(error => {
				delete this.pushCallId;
				this.showError(error.message + '\r\n' + pushError);
			}),
			starter()
		);
	}

	pull = e => {
		const { state } = this;
		const auth = state.auth2 ? state.auth2 : state.auth;
		const user = this.user === undefined ? auth.user : this.user;
		const pass = this.pass === undefined ? auth.pass : this.pass;
		const r = {
			user,
			hash: (new SHA256()).hex(pass).toUpperCase()
		};

		bfetch(
			{
				auth,
				method: 'PUT',
				r: { m: 'auth', f: 'profile_pull', r }
			},
			successor(result => {
				if (result.authorized) {
					if (result.profile && result.profile.birthday)
						result.profile.birthday = new Date(result.profile.birthday);

					store.set('auth', { ...result, user, pass }, 1, deepEqual);
				}
				else
					this.showError('Ошибка чтения');
			}),
			failer(error => this.showError('Ошибка чтения')),
			starter()
		);
	}

	logout = e => {
		if (this.pushCallId)
			return;

		const { state } = this;
		const auth = state.auth2 ? state.auth2 : state.auth;

		this.pushCallId = bfetch(
			{
				auth,
				method: 'PUT',
				r: { m: 'auth', f: 'logout' }
			},
			successor(result => {
				delete this.pushCallId;
				this.clearTypedFields();
				
				store.transform('auth', auth => {
					delete auth.authorized;
					return auth;
				}, 2);
			}),
			failer(error => {
				delete this.pushCallId;
				this.showError('Ошибка при отмене авторизации');
			}),
			starter()
		);
	}

	acceptTerms = e => {
		this.setState({ isTermsAccepted: e.target.checked });
		return prevent(e);
	}

	snackbarRef = e => this.snackbar = e;

	showError(e) {
		e && this.snackbar.MDComponent.show({ message: e });
	}

	linkTo = path => plinkRoute(path)

	render(props, state) {
		const auth = state.auth2 ? state.auth2 : state.auth;
		const profile = auth.profile ? auth.profile : {};
		const user = this.user !== undefined ? this.user : auth.user;
		const pass = this.pass !== undefined ? this.pass : auth.pass;
		const email = this.email !== undefined ? this.email : auth.email;
		const birthday = this.birthday !== undefined ? this.birthday : profile.birthday !== undefined ? strftime('%Y-%m-%d', profile.birthday) : undefined;
		const gender = this.gender !== undefined ? this.gender : profile.gender;
		const family = this.family !== undefined ? this.family : profile.family;
		const fname = this.fname !== undefined ? this.fname : profile.fname;
		const sname = this.sname !== undefined ? this.sname : profile.sname;
		const phone = this.phone !== undefined ? this.phone : profile.phone;
		const daddr1 = this.daddr1 !== undefined ? this.daddr1 : profile.daddr1;
		const daddr2 = this.daddr2 !== undefined ? this.daddr2 : profile.daddr2;

		const {
			valid,
			userError,
			emailError,
			passError,
			pass2Error,
			birthdayError,
			genderError,
			familyError, fnameError, snameError,
			phoneError,
			daddr1Error,
			daddr2Error,
			isTermsAccepted
		} = state;

		const textFieldOuterStyle = { width: '100%', marginTop: 0 };

		const userField = (
			<div>
				<TextField
					outerStyle={textFieldOuterStyle}
					autocomplete="off"
					invalid={!!userError}
					required={this.requiredFields.includes('user')}
					label={this.allFields.user + ' или E-mail'}
					trailingIcon="perm_identity"
					type="text"
					value={user}
					onInput={this.userFieldInput}
				/>
				<p aria-hidden="true" class={textFieldHelperTextClasses}>
					{(userError ? userError + ', ' : '') + 'например: VikDik или vik.dik@gmail.com'}
				</p>
			</div>);

		const emailField = (
			<div>
				<TextField
					outerStyle={textFieldOuterStyle}
					autocomplete="off"
					invalid={!!emailError}
					required={this.requiredFields.includes('email')}
					label={this.allFields.email + ' (E-mail)'}
					type="email"
					value={email}
					trailingIcon="email"
					onInput={this.emailFieldInput}
				/>
				<p aria-hidden="true" class={textFieldHelperTextClasses}>
					{(emailError ? emailError + ', ' : '') + 'например: vik.dik@gmail.com'}
				</p>
			</div>);

		const passField = (
			<div>
				<TextField
					outerStyle={textFieldOuterStyle}
					autocomplete="off"
					invalid={!!passError}
					required={this.requiredFields.includes('pass')}
					label={this.allFields.pass} minlength={8}
					trailingIcon="security"
					type="password"
					value={pass}
					onInput={this.passFieldInput}
				/>
				<p aria-hidden="true" class={textFieldHelperTextClasses}>
					{(passError ? passError + ', ' : '') + 'например: Uc_gliec6'}
				</p>
			</div>);

		const pass2Field = (
			<div>
				<TextField
					outerStyle={textFieldOuterStyle}
					autocomplete="off"
					invalid={!!pass2Error}
					required={this.requiredFields.includes('pass2')}
					label="Проверка пароля" minlength={8}
					trailingIcon="security"
					type="password"
					onInput={this.pass2FieldInput}
				/>
				<p aria-hidden="true" class={textFieldHelperTextClasses}>
					{pass2Error ? pass2Error : 'Повторите пароль'}
				</p>
			</div>);

		const birthdayField = (
			<div>
				<TextField
					outerStyle={textFieldOuterStyle}
					autocomplete="off"
					invalid={!!birthdayError}
					required={this.requiredFields.includes('birthday')}
					type="date"
					value={birthday}
					trailingIcon="cake"
					onInput={this.birthdayFieldInput}
				/>
				<p aria-hidden="true" class={textFieldHelperTextClasses}>
					{this.allFields.birthday + ', ' + (birthdayError ? birthdayError + ', ' : '') + 'например: 20.08.2000'}
				</p>
			</div>);

		const genderField = (
			<div>
				<FormField>
					<Radio
						id="radio-1-male"
						autocomplete="off"
						required={this.requiredFields.includes('gender')}
						fullwidth
						value="male"
						checked={gender === 'male'}
						name="gender"
						onChange={this.genderFieldInput}
					/>
					<label for="radio-1-male">
						Мужской
					</label>
				</FormField>
				<FormField>
					<Radio
						id="radio-2-female"
						autocomplete="off"
						required={this.requiredFields.includes('gender')}
						fullwidth
						value="female"
						checked={gender === 'female'}
						name="gender"
						onChange={this.genderFieldInput}
					/>
					<label for="radio-2-female">
						Женский
					</label>
				</FormField>
				<FormField>
					<Radio
						id="radio-3-other"
						autocomplete="off"
						required={this.requiredFields.includes('gender')}
						fullwidth
						value="other"
						checked={gender === 'other'}
						name="gender"
						onChange={this.genderFieldInput}
					/>
					<label for="radio-3-other">
						Другое
					</label>
				</FormField>
				<p aria-hidden="true" class={textFieldHelperTextClasses}>
					&nbsp;&nbsp;&nbsp;&nbsp;
					{genderError ? 'Пол (' + genderError + ')' : this.allFields.gender}
				</p>
			</div>);

		const phoneField = (
			<div>
				<TextField
					outerStyle={textFieldOuterStyle}
					autocomplete="off"
					invalid={!!phoneError}
					required={this.requiredFields.includes('phone')}
					label={this.allFields.phone}
					type="tel"
					value={phone}
					trailingIcon="phone"
					onInput={this.phoneFieldInput} onKeyUp={this.maskPhoneField}
				/>
				<p aria-hidden="true" class={textFieldHelperTextClasses}>
					{phoneError ? phoneError : this.allFields.phone + ', например: +7 (912) 88-85-554'}
				</p>
			</div>);

		const familyField = (
			<div>
				<TextField
					outerStyle={textFieldOuterStyle}
					autocomplete="off"
					invalid={!!familyError}
					required={this.requiredFields.includes('family')}
					label={this.allFields.family}
					trailingIcon="account_circle"
					type="text"
					value={family}
					onInput={this.familyFieldInput}
				/>
				<p aria-hidden="true" class={textFieldHelperTextClasses}>
					{familyError ? familyError : ' '}
				</p>
			</div>);

		const fnameField = (
			<div>
				<TextField
					outerStyle={textFieldOuterStyle}
					autocomplete="off"
					invalid={!!fnameError}
					required={this.requiredFields.includes('fname')}
					label={this.allFields.fname}
					trailingIcon="account_circle"
					type="text"
					value={fname}
					onInput={this.fnameFieldInput}
				/>
				<p aria-hidden="true" class={textFieldHelperTextClasses}>
					{fnameError ? fnameError : ' '}
				</p>
			</div>);

		const snameField = (
			<div>
				<TextField
					outerStyle={textFieldOuterStyle}
					autocomplete="off"
					invalid={!!snameError}
					required={this.requiredFields.includes('sname')}
					label={this.allFields.sname}
					trailingIcon="account_circle"
					type="text"
					value={sname}
					onInput={this.snameFieldInput}
				/>
				<p aria-hidden="true" class={textFieldHelperTextClasses}>
					{snameError ? snameError : ' '}
				</p>
			</div>);

		const daddr1Field = (
			<div>
				<TextField
					outerStyle={textFieldOuterStyle}
					autocomplete="off"
					invalid={!!daddr1Error}
					required={this.requiredFields.includes('daddr1')}
					label={this.allFields.daddr1}
					trailingIcon="local_shipping"
					type="text"
					value={daddr1}
					onInput={this.daddr1FieldInput}
				/>
				<p aria-hidden="true" class={textFieldHelperTextClasses}>
					{daddr1Error ? daddr1Error : ' '}
				</p>
			</div>);

		const daddr2Field = (
			<div>
				<TextField
					outerStyle={textFieldOuterStyle}
					autocomplete="off"
					invalid={!!daddr2Error}
					required={this.requiredFields.includes('daddr2')}
					label={this.allFields.daddr2}
					trailingIcon="local_shipping"
					type="text"
					value={daddr2}
					onInput={this.daddr2FieldInput}
				/>
				<p aria-hidden="true" class={textFieldHelperTextClasses}>
					{daddr2Error ? daddr2Error : ' '}
				</p>
			</div>);

		const logoutButton = state.notUseLogoutButton ? undefined : (
			<Button unelevated
				className={style.button}
				onClick={this.logout}
			>
				<Button.Icon>arrow_back</Button.Icon>Выйти
			</Button>);

		const pushButton = (
			<Button unelevated
				disabled={!valid || (!isTermsAccepted && !state.notUseTerms)}
				className={style.button}
				onClick={this.push}
			>
				<Button.Icon>backup</Button.Icon>{state.pushButtonName}
			</Button>);

		const terms = state.notUseTerms ? undefined : [
			<h3 style={{ textAlign: 'center' }}>
				Условия использования и политика конфиденциальности
			</h3>,
			<Typography body1 style={{ marginTop: 4, textAlign: 'justify', fontSize: '80%', lineHeight: '1.25em' }}
				dangerouslySetInnerHTML={{
					__html: DOMPurify.sanitize(termsOfUseAndPrivacyPolicy)
				}}
			/>,
			<FormField>
				<strong style={{ float: 'left', maxWidth: '85%', textAlign: 'right' }}>
					Я согласен с условиями использования и политикой конфиденциальности
				</strong>
				<Checkbox style={{ float: 'right' }}
					required
					checked={isTermsAccepted}
					onChange={this.acceptTerms}
				/>
			</FormField>];

		return (
			<div class={style.profile}>
				<Snackbar ref={this.snackbarRef} class={style.snackbar} />

				<form onSubmit={false}>
					<LayoutGrid>
						<LayoutGrid.Inner style={{ gridRowGap: 8 }}>
							<LayoutGrid.Cell>
								{userField}
								{emailField}
							</LayoutGrid.Cell>
							<LayoutGrid.Cell>
								{passField}
								{pass2Field}
							</LayoutGrid.Cell>
							<LayoutGrid.Cell>
								{phoneField}
								{birthdayField}
							</LayoutGrid.Cell>
							<LayoutGrid.Cell>
								{familyField}
								{fnameField}
								{snameField}
							</LayoutGrid.Cell>
							<LayoutGrid.Cell>
								{genderField}
							</LayoutGrid.Cell>
							<LayoutGrid.Cell>
								{daddr1Field}
								{daddr2Field}
							</LayoutGrid.Cell>
						</LayoutGrid.Inner>
					</LayoutGrid>
				</form>
				<LayoutGrid>
					<LayoutGrid.Inner>
						{terms ?
							<LayoutGrid.Cell cols="12">
								{terms}
							</LayoutGrid.Cell> : undefined}
						<LayoutGrid.Cell>
							{logoutButton}
							{pushButton}
						</LayoutGrid.Cell>
					</LayoutGrid.Inner>
				</LayoutGrid>
			</div>);
	}
}
//------------------------------------------------------------------------------
