//------------------------------------------------------------------------------
import { route } from 'preact-router';
import Snackbar from 'preact-material-components/Snackbar';
import 'preact-material-components/Snackbar/style.css';
import LayoutGrid from 'preact-material-components/LayoutGrid';
import 'preact-material-components/LayoutGrid/style.css';
import Typography from 'preact-material-components/Typography';
import 'preact-material-components/Typography/style.css';
import Radio from 'preact-material-components/Radio';
import 'preact-material-components/Radio/style.css';
import FormField from 'preact-material-components/FormField';
import 'preact-material-components/FormField/style.css';
import 'preact-material-components/List/style.css';
import Component from '../Component';
import {
	headerTitleStorePath,
	headerSearchStorePath,
	iconsLosslessStorePath,
	iconsQualityStorePath,
	textFieldHelperTextClasses,
	kioskStorePath,
	kioskSlipCopiesStorePath,
	kioskNameStorePath
} from '../../const';
import {
	prevent,
	plinkRoute
} from '../../lib/util';
import {
	cartDataPath,
	cartTotalPath,
	cartDAddrPath,
	cartCheckMarksPath,
	pull,
	push,
	clear,
	createOrder
} from './loader';
import store from '../../lib/rstore';
import style from './style.scss';
import Card from './Card';
import Vab from '../Material/VerticalActionBar';
import Printer from '../Orders/Order/Printer';
//------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------------------
export default class Cart extends Component {
	storeConfig = [
		{ path: headerTitleStorePath, mount: 'Корзина' },
		{ path: headerSearchStorePath, mount: undefined },
		{ path: iconsLosslessStorePath, alias: 'iconsLossless' },
		{ path: iconsQualityStorePath, alias: 'iconsQuality' },
		{ path: kioskStorePath, alias: 'kiosk' },
		{ path: kioskSlipCopiesStorePath, alias: 'slipCopies' },
		{ path: kioskNameStorePath, alias: 'kioskName' },
		{ path: 'auth', alias: 'auth', defaultValue: {} },
		{ path: cartDataPath, alias: 'data', defaultValue: { rows: [] } },
		{ path: cartTotalPath, alias: 'total' },
		{ path: cartDAddrPath, alias: 'daddr' },
		{ path: cartCheckMarksPath, alias: 'checkMarks' }
	]

	willMount() {
		this.didSetState();
		pull.call(this);
	}

	didSetState() {
		const { auth } = this.state;

		if (!auth.authorized)
			route('/', true);
	}

	linkTo = path => this.__linkTo(path)
	__linkTo(path) {
		return { href: path, onClick: plinkRoute(path) };
	}

	goHome = this.linkTo('/')

	snackbarRef = e => this.__snackbarRef(e)
	__snackbarRef(e) {
		this.snackbar = e;
	}

	showError = e => this.__showError(e)
	__showError(e) {
		if (this.snackbar)
			this.snackbar.MDComponent.show({ message: e });
	}

	getCard(link) {
		let { cards } = this;

		if (!cards)
			this.cards = cards = {};

		let card = cards[link];

		if (!card)
			this.cards[link] = card = {};

		return card;
	}

	handlePlusMinusClick = (link, q) => {
		const card = this.getCard(link);
		const n = q > 0 ? 'pc' : q < 0 ? 'mc' : undefined;
		let pmc = card[n];

		if (!pmc)
			card[n] = pmc = e => {
				push.call(this, { link, q }, pmc);
				return prevent(e);
			};

		return pmc;
	}

	handleCheckMarkClick = link => {
		const card = this.getCard(link);
		let { cm } = card;

		if (!cm)
			card.cm = cm = e => {
				const p0 = cartCheckMarksPath + '.' + link;
				const p1 = cartCheckMarksPath + '.length';

				store.toggle(p0);

				if (store.get(p0))
					store.inc(p1);
				else {
					store.dec(p1);
					store.delete(p0);
				}

				let l = store.get(p1);

				if (!l)
					store.delete(p1);

				store.undef(cartCheckMarksPath, l ? true : undefined);

				return prevent(e);
			};

		return cm;
	}

	setCheckMarks = e => this.__setCheckMarks(e)
	__setCheckMarks(e) {
		const { data } = this.state;

		store.delete(cartCheckMarksPath);

		for (const row of data.rows)
			if (row)
				store.set(cartCheckMarksPath + '.' + row.link, true);

		store.set(cartCheckMarksPath + '.length', data.rows.length);
		store.set(cartCheckMarksPath, true);

		return prevent(e);
	}

	resetCheckMarks = e => this.__resetCheckMarks(e)
	__resetCheckMarks(e) {
		store.delete(cartCheckMarksPath);
		return prevent(e);
	}

	clearSelectedItems = e => {
		clear.call(this);
		return prevent(e);
	}

	createOrder = e => {
		createOrder.call(this);
		return prevent(e);
	}

	daddrInput = e => this.__daddrInput(e)
	__daddrInput(e) {
		let daddr = ~~e.target.value;

		if (daddr === 0)
			daddr = undefined;

		store.undef(cartDAddrPath, daddr);

		return prevent(e);
	}

	printerRef = e => this.printer = e

	render(props, state) {
		const {
			data,
			total,
			auth,
			daddr,
			checkMarks,
			iconsLossless,
			iconsQuality,
			availability,
			kiosk,
			slipCopies,
			kioskName
		} = state;

		if (!auth.authorized || !data || data.rows.length === 0)
			return undefined;

		const vab = (
			<Vab fixed display={data.pkl}>
				{kiosk ?
					<Vab.Fab className={style.b} {...this.goHome}>
						<Vab.Fab.Icon>
							home
						</Vab.Fab.Icon>
					</Vab.Fab> : undefined}
				{checkMarks ?
					<Vab.Fab className={style.b}
						onClick={this.createOrder}
					>
						<Vab.Fab.Icon>
							thumb_up
						</Vab.Fab.Icon>
					</Vab.Fab> : undefined}
				{checkMarks ?
					<Vab.Fab className={style.b}
						onClick={this.clearSelectedItems}
					>
						<Vab.Fab.Icon>
							delete
						</Vab.Fab.Icon>
					</Vab.Fab> : undefined}
				<Vab.Fab className={style.b}
					onClick={this.setCheckMarks}
				>
					<Vab.Fab.Icon>
						&#xE834;
					</Vab.Fab.Icon>
				</Vab.Fab>
				<Vab.Fab className={style.b}
					onClick={this.resetCheckMarks}
				>
					<Vab.Fab.Icon>
						&#xE835;
					</Vab.Fab.Icon>
				</Vab.Fab>
				<Typography
					headline6={!kiosk}
					headline5={kiosk}
					className={style.sm}
				>
					{`Итого: ${total}₽`}
				</Typography>
			</Vab>);

		let n = 0;

		const daddrCell = state.kiosk ? undefined : (
			<LayoutGrid.Cell>
				{auth.profile.daddr1 ?
					<FormField>
						<Radio
							id="radio-1-daddr1"
							autocomplete="off"
							fullwidth
							value={1}
							checked={daddr === 1}
							name="daddr"
							onChange={this.daddrInput}
						/>
						<label for="radio-1-daddr1">
							Первичный
						</label>
					</FormField> : undefined}
				{auth.profile.daddr2 ?
					<FormField>
						<Radio
							id="radio-2-daddr2"
							autocomplete="off"
							fullwidth
							value={2}
							checked={daddr === 2}
							name="daddr"
							onChange={this.daddrInput}
						/>
						<label for="radio-2-daddr2">
							Вторичный
						</label>
					</FormField> : undefined}
				<FormField>
					<Radio
						id="radio-3-daddr-none"
						autocomplete="off"
						fullwidth
						value={0}
						checked={daddr === 0 || daddr === undefined}
						name="daddr"
						onChange={this.daddrInput}
					/>
					<label for="radio-3-daddr-none">
						Нет
					</label>
				</FormField>
				{daddr ?
					<p aria-hidden="true" className={textFieldHelperTextClasses}>
						{'Адрес доставки: ' + auth.profile[`daddr${~~daddr}`]}
					</p> : undefined}
			</LayoutGrid.Cell>);

		const printer = kiosk ? (
			<Printer ref={this.printerRef}
				slipCopies={slipCopies} kioskName={kioskName}
			/>)
			: undefined;

		return (
			<div class={style.cart}>
				{/*<i className="icomoon">checkbox-checked</i>
				<i className="icomoon">checkbox-unchecked</i>
				<i className="lnr">&#xe811;</i>*/}
				<Snackbar ref={this.snackbarRef} class={style.snackbar} />
				<LayoutGrid>
					<LayoutGrid.Inner>
						{data.rows.map(row => row ? (
							<LayoutGrid.Cell {...this.cellEventsHandlers}>
								<Card {...row}
									n={++n}
									key={row.link}
									iconLossless={iconsLossless}
									iconQuality={iconsQuality}
									minusClick={this.handlePlusMinusClick(row.link, -1)}
									plusClick={this.handlePlusMinusClick(row.link, +1)}
									checkMarkClick={this.handleCheckMarkClick(row.link)}
									checkMarksStorePath={cartCheckMarksPath}
									availability={availability ? availability[row.link] : undefined}
								/>
							</LayoutGrid.Cell>) : undefined)}
						{daddrCell}
					</LayoutGrid.Inner>
				</LayoutGrid>
				{vab}
				{printer}
			</div >
		);
	}
}
//------------------------------------------------------------------------------
