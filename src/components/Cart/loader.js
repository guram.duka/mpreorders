//------------------------------------------------------------------------------
import { route } from 'preact-router';
import { burl, bfetch } from '../../backend';
import { successor, failer, starter } from '../load';
import { deepEqual } from '../../lib/util';
import store from '../../lib/rstore';
import { refreshUrls as productRefreshUrls } from '../Products/Product/loader';
import { refreshUrls as ordersRefreshUrls } from '../Orders/Orders/loader';
//------------------------------------------------------------------------------
export const cartPath = 'cart';
export const cartDataPath = `${cartPath}.data`;
export const cartTotalPath = `${cartPath}.total`;
export const cartDAddrPath = `${cartPath}.daddr`;
export const cartCheckMarksPath = `${cartPath}.checkmarks`;
//------------------------------------------------------------------------------
function pullOpts() {
	const { state } = this;
	return {
		auth: state.auth,
		r: {
			m: 'cart',
			f: 'pull'
		},
		a: state.auth.link
	};
}
//------------------------------------------------------------------------------
function calcTotalAndPKey(result) {
	const { rows } = result;
	const pkey = {};
	let t = 0;

	for (const [i, { link, quantity, price }] of rows.entries()) {
		t += quantity * price;
		pkey[link] = i;
	}

	result.pkey = pkey;
	result.pkl = rows.length;

	return [t, result];
}
//------------------------------------------------------------------------------
export function pull() {
	const opts = pullOpts.call(this);

	return bfetch(
		opts,
		successor(result => {
			this.refreshUrls = {
				...this.refreshUrls,
				[opts.url]: result.maxAge
			};
			const [total, data] = calcTotalAndPKey(result);

			store.set(cartTotalPath, total);
			store.set(cartDataPath, data, 1, deepEqual);
		}),
		failer(error => this.showError(error.message)),
		starter()
	);
}
//------------------------------------------------------------------------------
export function push(r, pmc) {
	if (pmc.pushId)
		return;

	const { state } = this;
	const opts = {
		auth: state.auth,
		method: 'PUT',
		r: {
			m: 'cart',
			f: 'push',
			r: {
				link: r.link,
				q: r.q
			}
		}
	};

	// emulate Product object
	const product = {
		props: {
			link: r.link
		},
		state: {
			auth: state.auth
		}
	};

	opts.refreshUrls = {
		...this.refreshUrls,
		...productRefreshUrls.call(product)
	};

	pmc.pushId = bfetch(
		opts,
		successor(result => {
			delete pmc.pushId;
			const data = store.get(cartDataPath);
			const total = store.get(cartTotalPath);
			const { rows, pkey } = data;
			const i = pkey[r.link];
			const row = rows[i];

			if (!row || row.link !== r.link)
				return;

			const { quantity, price } = row;

			row.quantity = result.quantity;
			row.price = result.price;
			const t = total - quantity * price + row.quantity * row.price;

			if (row.quantity === 0) {
				data.pkl--;
				rows[i] = undefined;
				delete pkey[r.link];
			}

			store.replace(cartDataPath, data);
			store.set(cartTotalPath, t);
		}),
		failer(error => {
			delete pmc.pushId;
			this.showError(error.message);
		}),
		starter()
	);
}
//------------------------------------------------------------------------------
function clearCommitedCards(links) {
	const data = store.get(cartDataPath);
	let t = store.get(cartTotalPath);
	const { rows, pkey } = data;

	for (const link of links) {
		const i = pkey[link];
		const row = rows[i];

		if (!row || row.link !== link)
			continue;

		t = t - row.quantity * row.price;
		rows[i] = undefined;
		delete pkey[link];
		data.pkl--;
	}

	return [t, data];
}
//------------------------------------------------------------------------------
function gatherCheckMarkedCardsInfo() {
	const { state } = this;
	const { auth } = state;
	const data = store.get(cartDataPath);
	const { pkey } = data;
	const links = [], refreshUrls = { ...this.refreshUrls };
	let checkedCnt = 0, nzCnt = 0;

	for (const link of Object.keys(pkey)) {
		if (store.get(cartCheckMarksPath + '.' + link)) {
			const urls = productRefreshUrls.call({
				// emulate Product object
				props: { link },
				state: { auth }
			});

			for (const [k, v] of Object.entries(urls))
				refreshUrls[k] = v;

			links.push(link);
			checkedCnt++;
		}
		nzCnt++;
	}

	return [refreshUrls, links, checkedCnt, nzCnt];
}
//------------------------------------------------------------------------------
export function clear() {
	if (this.clearCallId)
		return;

	const { state } = this;
	const opts = {
		auth: state.auth,
		method: 'PUT',
		r: {
			m: 'cart',
			f: 'clear'
		}
	};

	const [refreshUrls, links, checkedCnt, nzCnt] =
		gatherCheckMarkedCardsInfo.call(this);

	if (checkedCnt === 0)
		return;

	if (nzCnt !== checkedCnt)
		opts.r.r = { links };

	opts.refreshUrls = refreshUrls;

	this.clearCallId = bfetch(
		opts,
		successor(result => {
			const [t, data] = clearCommitedCards.call(this, links);

			store.replace(cartDataPath, data);
			store.set(cartTotalPath, t);
			store.delete(cartCheckMarksPath);

			delete this.clearCallId;
		}),
		failer(error => {
			delete this.clearCallId;
			this.showError(error.message);
		}),
		starter()
	);
}
//------------------------------------------------------------------------------
export function createOrder() {
	if (this.createOrderCallId)
		return;

	const { state } = this;
	const opts = {
		auth: state.auth,
		method: 'PUT',
		r: {
			m: 'cart',
			f: 'order',
			r: {}
		}
	};

	const [refreshUrls, links, checkedCnt, nzCnt] =
		gatherCheckMarkedCardsInfo.call(this);

	if (checkedCnt === 0)
		return;

	if (nzCnt !== checkedCnt)
		opts.r.r.links = links;

	if (state.daddr)
		opts.r.r.daddr = state.daddr;
	if (state.kiosk)
		opts.r.r.post = true;

	opts.refreshUrls = refreshUrls;

	const urls = ordersRefreshUrls.call({
		// emulate Orders object
		state: { auth: state.auth }
	});

	for (const [k, v] of Object.entries(urls))
		refreshUrls[k] = v;

	this.createOrderCallId = bfetch(
		opts,
		successor(result => {
			delete this.createOrderCallId;

			if (result.availability) {
				const availability = {};

				for (const { product, ...r } of result.availability)
					availability[product] = r;

				this.setState({ availability });
			}
			else {
				const { kiosk } = this.state;
				const [t, data] = clearCommitedCards.call(this, links);

				store.replace(cartDataPath, data);
				store.set(cartTotalPath, t);
				store.delete(cartCheckMarksPath);

				if (kiosk) {
					this.printer.print({ order: result, data: result.products.rows });
					//setTimeout(() => route('/', true), 3000);
					route('/', true);
				}
				else
					route('/orders', true);
			}
		}),
		failer(error => {
			delete this.createOrderCallId;
			this.showError(error.message);
		}),
		starter()
	);
}
//------------------------------------------------------------------------------
export function refreshUrls() {
	return { [burl(pullOpts.call(this))]: 60 };
}
//------------------------------------------------------------------------------
