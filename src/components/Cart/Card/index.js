//------------------------------------------------------------------------------
import Component from '../../Component';
import Elevation from '../../Material/Elevation';
import Button from 'preact-material-components/Button';
import 'preact-material-components/Button/style.css';
import Typography from 'preact-material-components/Typography';
import 'preact-material-components/Typography/style.css';
import Checkbox from 'preact-material-components/Checkbox';
import 'preact-material-components/Checkbox/style.css';
import Image from '../../Material/Image';
import { plinkRoute } from '../../../lib/util';
import style from './style.scss';
//------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------------------
export default class Card extends Component {
	storeConfig() {
		const { props } = this;
		const config = [];

		if (props.checkMarksStorePath)
			config.push(
				{ path: props.checkMarksStorePath + '.' + props.link, alias: 'checkMark' }
			);

		return config;
	}

	// regex replace comma without space after
	static cr = /(,(?=\S)|:)/g
	static sr = /(\s{2})/g

	static crsr(s) {
		const { cr, sr } = Card;
		return s.replace(cr, ', ').replace(sr, ' ').trim();
	}

	linkTo = path => ({ href: path, onClick: plinkRoute(path) })

	render(props, state) {
		let {
			n,
			link,
			code,
			name,
			quantity,
			price,
			availability
		} = props;

		const goProduct = this.linkTo('/product/' + link);
		name = Card.crsr(`[${code}] ${name}`);
		price = `${price}₽`;
		quantity = `${quantity} шт.`;

		return (
			<Elevation z={1} tag="div" className={style.pr}>
				<div className={style.flh}>
					<Checkbox
						className={props.checkMarkClick ? style.cb : style.cbInvis}
						onClick={props.checkMarkClick}
						checked={state.checkMark}
					/>
					<Image
						lossless={props.iconLossless}
						quality={props.iconQuality}
						link={props.primaryImageLink}
						className={style.media}
						{...goProduct}
					/>
				</div>
				<div className={style.titleBlock}>
					<Typography body1 className={style.n}>
						<strong>#{n}</strong>
					</Typography>
					&nbsp;
					<a {...goProduct}>
						{name}
					</a>
					<div className={style.brc}>
						{availability ?
							<div>
								<strong>Доступно:</strong>
								&nbsp;
								<Typography className={style.red + ' blink'}>
									<strong>
										{`${availability.remainder} шт.`}
									</strong>
								</Typography>
							</div> : undefined}
						<div className={style.tar}>
							{props.minusClick ?
								<Button dense
									className={style.button}
									onClick={props.minusClick}
								>
									<Button.Icon>remove</Button.Icon>
								</Button> : undefined}
							<Typography button className={style.qt}>
								{quantity}
							</Typography>
							{props.plusClick ?
								<Button dense
									className={style.button}
									onClick={props.plusClick}
								>
									<Button.Icon>add</Button.Icon>
								</Button> : undefined}
						</div>
						<div className={style.tar}>
							<Typography body1 className={style.sm}>
								Всего: <strong>{props.quantity * props.price}₽</strong>
							</Typography>
						</div>
					</div>
					<div className={style.vt}>
						<Typography body1 className={style.sm}>
							Цена: <strong>{price}</strong>
						</Typography>
					</div>
				</div>
			</Elevation>);
	}
}
//------------------------------------------------------------------------------
