//------------------------------------------------------------------------------
//import debug from 'preact/devtools';
//------------------------------------------------------------------------------
import 'preact-material-components/Theme/style.css';
//------------------------------------------------------------------------------
import { Router } from 'preact-router';
import createHashHistory from 'history/createHashHistory';
import root from '../../lib/root';
import Component from '../Component';
import Header from '../Header';
import Home from '../../routes/Home';//import Home from 'async!../routes/Home';
import Settings from '../../routes/Settings';//import Settings from 'async!../routes/Settings';
import Cart from '../../routes/Cart';//import Cart from 'async!../routes/Cart';
import Orders from '../../routes/Orders';//import Orders from 'async!../routes/Orders';
import Order from '../../routes/Orders/Order';//import Order from 'async!../routes/Orders/Order';
import Favorites from '../../routes/Favorites';//import Favorites from 'async!../routes/Favorites';
import Profile from '../../routes/Profile';//import Profile from 'async!../routes/Profile';
import Login from '../../routes/Profile/Login';
import Registration from '../../routes/Profile/Registration';
import Categories from '../../routes/Categories';//import Categories from 'async!../routes/Categories';
import Category from '../../routes/Category';//import Category from 'async!../routes/Category';
import Product from '../../routes/Product';//import Product from 'async!../routes/Product';
import { kioskStorePath } from '../../const';
//------------------------------------------------------------------------------
import style from './style.scss';
import { doFullScreen, isFirefox } from '../../lib/util';
//------------------------------------------------------------------------------
/** fall-back route (handles unroutable URLs) */
const Error = ({ type, url }) => (
	<section class={style.error}>
		<h2>Error {type}</h2>
		<p>It looks like we hit a snag.</p>
		<pre>{url}</pre>
	</section>
);
//------------------------------------------------------------------------------
export default class App extends Component {
	/** Gets fired when the route changes.
	 *	@param {Object} event		"change" event from [preact-router](http://git.io/preact-router)
	 *	@param {string} event.url	The newly routed URL
	 */
	handleRoute = e => this.currentUrl = e.url

	storeConfig = [
		{ path: kioskStorePath, alias: 'kiosk' }
	]

	didMount() {
		if (this.state.kiosk) {
			let child = root.document.getElementById('app-kiosk-scroll');
			let addWidth = child.offsetWidth - child.clientWidth;

			if (addWidth !== 16)
				child.style.marginRight = -addWidth + 'px';

			const id = 'styleKiosk';
			child = root.document.getElementById(id);

			if (!child) {
				child = root.document.createElement('style');
				child.id = id;
				root.document.head.appendChild(child);
			}

			//child.sheet.insertRule(`* { cursor: none !important; }`);
			if (isFirefox())
				doFullScreen();
		}
	}

	didUpdate() {
		this.didMount();
	}

	getHashHistory() {
		// https://stackoverflow.com/questions/45742982/set-base-url-for-preact-cli
		const canUseDOM = !!(typeof window !== 'undefined' && window.document && window.document.createElement);

		if (canUseDOM)
			return createHashHistory();
	}

	render(props, state) {
		const content = [
			<Header />,
			<Router onChange={this.handleRoute} history={this.getHashHistory()}>
				<Home path="/" />
				<Settings path="/settings/" />
				<Cart path="/cart/" />
				<Orders path="/orders/" />
				<Order path="/orders/:link" />
				<Favorites path="/favorites/" />
				<Profile path="/profile/" />
				<Login path="/login/" />
				<Registration path="/registration/" />
				<Category path="/category/:category/:pageProps" />
				<Categories path="/categories/" />
				<Product path="/product/:link" />
				<Error type="404" default />
			</Router>
		];

		if (state.kiosk)
			return (
				<div id="app" className="app-kiosk">
					<div id="app-kiosk-scroll">
						{content}
					</div>
				</div>);

		return <div id="app">{content}</div>;
	}
}
