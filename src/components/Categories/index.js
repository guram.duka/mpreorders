//------------------------------------------------------------------------------
import Button from 'preact-material-components/Button';
import 'preact-material-components/Button/style.css';
import Chips from 'preact-material-components/Chips';
import 'preact-material-components/Chips/style.css';
import Component from '../Component';
import store from '../../lib/rstore';
import { headerTitleStorePath, headerSearchStorePath } from '../../const';
import loader, {
	storePath,
	storeSelectedPath
} from './loader';
import { goCategory } from './link';
import style from './style.scss';
import { prevent } from '../../lib/util';
//------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------------------
export default class Categories extends Component {
	storeConfig = [
		{ path: headerTitleStorePath, mount: 'Категории' },
		{ path: headerSearchStorePath, mount: undefined },
		{ path: storePath, alias: 'data', defaultValue: [] },
		{ path: storeSelectedPath, alias: 'selected', defaultValue: new Map() }
	]

	willMount() {
		loader.call(this);
	}

	chipClick = link => e => {
		const data = store.get(storePath);
		const selected = store.get(storeSelectedPath, new Set());
		const index = data.findIndex(v => v.link === link);

		if (index !== -1) {
			const { link } = data[index];

			if (selected.has(link))
				selected.delete(link);
			else
				selected.add(link);

			for (const link of Array.from(selected))
				if (data.findIndex(v => v.link === link) === -1)
					selected.delete(link);

			store.replace(storeSelectedPath, selected);
		}

		return prevent(e);
	}

	render(props, state) {
		const { data, selected } = state;
		const items = [];

		for (const { link, name } of data)
			items.push(
				<Chips.Chip
					selected={selected.has(link)}
					onClick={this.chipClick(link)}
				>
					<Chips.Checkmark />
					<Chips.Text>
						{name}
					</Chips.Text>
				</Chips.Chip>);

		return (
			<div class={style.categories}>
				<Chips filter>
					{items}
				</Chips>
				{selected.size ? (
					<Button ripple
						{...goCategory(Array.from(selected).join(','))}
					>
						Открыть выбранные
					</Button>) : undefined}
			</div>
		);
	}
}
//------------------------------------------------------------------------------
