//------------------------------------------------------------------------------
import { plinkRoute } from '../../lib/util';
import { kioskStorePath } from '../../const';
import store from '../../lib/rstore';
//------------------------------------------------------------------------------
export function linkTo(path) {
	return { href: path, onClick: plinkRoute(path) };
}
//------------------------------------------------------------------------------
export function goCategory(link, page = 1, pageSize) {
	if (store.get(kioskStorePath))
		pageSize = 42;
	else
		pageSize = 40;
	return linkTo('/category/' + link + '/' + page + ',' + pageSize);
}
//------------------------------------------------------------------------------
