//------------------------------------------------------------------------------
import { bfetch } from '../../backend';
import { zLink } from '../../const';
import { deepEqual } from '../../lib/util';
import store from '../../lib/rstore';
import { successor, failer, starter } from '../load';
//------------------------------------------------------------------------------
export const storePath = 'categories';
export const storeSelectedPath = storePath + '.selected';
//------------------------------------------------------------------------------
export default function loader(pusher) {
	return bfetch(
		{
			r: { m: 'category', f: 'list', r: { target: 'products' } }
		},
		successor(result => {
			result.rows.push({ link: zLink, name: 'Вне категорий' });

			if (pusher)
				pusher(result);
			else
				store.set(storePath, result.rows, 1, deepEqual);
		}),
		failer(),
		starter()
	);
}
//------------------------------------------------------------------------------
