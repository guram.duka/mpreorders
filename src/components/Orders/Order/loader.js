//------------------------------------------------------------------------------
import { burl, bfetch } from '../../../backend';
import { successor, failer, starter } from '../../load';
import { deepEqual } from '../../../lib/util';
import store from '../../../lib/rstore';
//------------------------------------------------------------------------------
function pullOpts(f) {
	const { props, state } = this;
	const auth = state.auth ? state.auth : props.auth;
	return {
		auth,
		r: {
			m: 'orders',
			f: f ? f : 'pull',
			r: {
				link: props.link
			}
		},
		a: auth.link
	};
}
//------------------------------------------------------------------------------
export function pull() {
	const opts = pullOpts.call(this);

	return bfetch(
		opts,
		successor(result => {
			result = result.rows[0];
			store.set(this.orderStorePath, result, 1, deepEqual);
			store.set(this.msgsStorePath, result.messages.rows, 1, deepEqual);
		}),
		failer(error => this.showError(error.message)),
		starter()
	);
}
//------------------------------------------------------------------------------
function pullMsgsOpts() {
	const opts = pullOpts.call(this, 'pull_msgs');
	const { msgs } = this.state;

	if (msgs)
		for (let i = msgs.length - 1; i >= 0; i--)
			if (msgs[i] && msgs[i].time !== undefined) {
				opts.r.r.time = msgs[i].time;
				break;
			}

	return opts;
}
//------------------------------------------------------------------------------
export function pullMsgs() {
	const opts = pullMsgsOpts.call(this);

	return bfetch(
		opts,
		result => {
			this.refreshUrls = {
				...this.refreshUrls,
				[opts.url]: result.maxAge
			};

			if (result.rows.length !== 0) {
				const { msgs } = store.get(this.msgsStorePath, []);

				for (const row of result.rows)
					msgs.push(row);

				store.replace(this.msgsStorePath, msgs);
			}
		}
	);
}
//------------------------------------------------------------------------------
export function pushMsgOpts(f, message) {
	const { props, state } = this;
	const opts = {
		auth: state.auth,
		method: 'PUT',
		r: {
			m: 'orders',
			f,
			r: {
				link: props.link,
				...message
			}
		}
	};

	opts.refreshUrls = {
		[burl(pullOpts.call(this))]: 180,
		[burl(pullMsgsOpts.call(this))]: 60,
		...this.refreshUrls
	};

	return opts;
}
//------------------------------------------------------------------------------
export function pushMsg(message) {
	if (this.pushMsgId)
		return;

	const opts = pushMsgOpts.call(this, 'push_msg', { msg: message });

	this.pushMsgId = bfetch(
		opts,
		successor(result => {
			const msgs = store.get(this.msgsStorePath, []);

			msgs.push({ ...result.msg, msg: message });
			store.replace(this.msgsStorePath, msgs);
			delete this.pushMsgId;

			this.setState({ msg: undefined });
		}),
		failer(error => {
			delete this.pushMsgId;
			this.showError(error.message);
		}),
		starter()
	);
}
//------------------------------------------------------------------------------
export function deleteMsg(key) {
	if (this.pushMsgId)
		return;

	const opts = pushMsgOpts.call(this, 'delete_msg', { time: key });

	this.pushMsgId = bfetch(
		opts,
		successor(result => {
			const msgs = store.get(this.msgsStorePath, []);

			for (const [i, msg] of msgs.entries())
				if (msg && msg.time === key) {
					msgs[i] = undefined;
					break;
				}

			store.replace(this.msgsStorePath, msgs);
			delete this.pushMsgId;
		}),
		failer(error => {
			delete this.pushMsgId;
			this.showError(error.message);
		}),
		starter()
	);
}
//------------------------------------------------------------------------------
export function refreshUrls() {
	return { [burl(pullOpts.call(this))]: 60 };
}
//------------------------------------------------------------------------------
