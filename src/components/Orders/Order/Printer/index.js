//------------------------------------------------------------------------------
import { Component } from 'preact';
import { xpathEvalSingle, dateFormatter } from '../../../../lib/util';
import BarcodeEAN13Render from '../../../../lib/barcode';
import style from './style.scss';
//------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------------------
export default class Printer extends Component {
	print(props) {
		const { slipCopies, kioskName } = this.props;
		const { order, data } = props ? props : this.props;

		try {
			const c = slipCopies ? slipCopies : 1;

			for (let i = 1; i <= c; i++) {
				const iframeElement = xpathEvalSingle('//iframe[@print and @copy=\'' + i + '\']');
				const iframeContent = iframeElement.contentWindow;
				const iframe = iframeContent.document;
				const head = xpathEvalSingle('html/body/div[@head]', iframe, iframe);

				xpathEvalSingle('p[@kiosk]', head, iframe).innerHTML = kioskName;
				xpathEvalSingle('p[@uuid]', head, iframe).innerHTML = order.link;
				xpathEvalSingle('p[@number]', head, iframe).innerHTML = 'Заказ&nbsp;:&nbsp;' + order.number;
				xpathEvalSingle('p[@date]', head, iframe).innerHTML = 'Время&nbsp;:&nbsp;' + dateFormatter(order.date);
				xpathEvalSingle('p[@barcode]', head, iframe).innerHTML = 'EAN13&nbsp;:&nbsp;' + order.barcode;

				const table = xpathEvalSingle('html/body/div[@table]', iframe, iframe);
				const rouble = '&#x20bd;';//'<i rouble>&nbsp;</i>';
				let html = '';

				for (const [i, { name, code, unitName, quantity, price }] of data.entries())
					html = html + `
						<p product>
						<span n>${i + 1}.</span> ${name} [${code}] ${price}${rouble}
						<span psum><b q>${quantity}&nbsp;${unitName}.</b>&nbsp;=${price * quantity}${rouble}</span>
						</p>`;

				table.innerHTML = html.replace(/(?:[\r\n\t])/g, '');

				const footer = xpathEvalSingle('html/body/div[@footer]', iframe, iframe);
				xpathEvalSingle('p[@totals]/span[@txt]', footer, iframe).innerHTML = 'Сумма:';
				xpathEvalSingle('p[@totals]/span[@sum]', footer, iframe).innerHTML = order.totals + rouble;

				const barcodeRender = new BarcodeEAN13Render({ width: 6.1 });
				const barcodeHTML = barcodeRender.render(order.barcode);
				const tail = xpathEvalSingle('html/body/div[@tail]', iframe, iframe);
				xpathEvalSingle('div[@barcode]', tail, iframe).innerHTML = barcodeHTML;

				// Note for Firefox
				// http://stackoverflow.com/a/11823629
				// Open about:config then change the pref dom.successive_dialog_time_limit to zero integer,
				// print.always_print_silent to true, print.show_print_progress to false

				//if (isEdge())
				// 	iframe.execCommand('print', false, null);
				// else
				iframeContent.print();
				//console.log(iframe.body.parentElement.innerHTML);
				// const printContent = iframe.body.parentElement.innerHTML;
			}
		}
		catch (e) {
		}
	}

	barcodeRender = new BarcodeEAN13Render({ width: 6.1 })

	render(props, state) {
		const { slipCopies } = props;
		const p = {
			src: '/assets/print/template.html',
			print: true,
			frameborder: 0,
			scrolling: 'no'
		};

		const iframes = [];

		for (let i = 1; i <= slipCopies; i++)
			iframes.push(<iframe {...p} copy={i} />);

		return (
			<div className={style.printer}>
				{iframes}
			</div>);
	}
}
//------------------------------------------------------------------------------
