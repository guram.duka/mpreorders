//------------------------------------------------------------------------------
import DOMPurify from 'dompurify';
import { route } from 'preact-router';
import Elevation from '../../Material/Elevation';
import Chips from 'preact-material-components/Chips';
import 'preact-material-components/Chips/style.css';
import Typography from 'preact-material-components/Typography';
import 'preact-material-components/Typography/style.css';
import Snackbar from 'preact-material-components/Snackbar';
import 'preact-material-components/Snackbar/style.css';
import LayoutGrid from 'preact-material-components/LayoutGrid';
import 'preact-material-components/LayoutGrid/style.css';
import TextField from 'preact-material-components/TextField';
import 'preact-material-components/TextField/style.css';
import Icon from 'preact-material-components/Icon';
import 'preact-material-components/Icon/style.css';
import Component from '../../Component';
import {
	headerTitleStorePath,
	headerSearchStorePath,
	iconsLosslessStorePath,
	iconsQualityStorePath,
	kioskStorePath,
	kioskSlipCopiesStorePath,
	kioskNameStorePath
} from '../../../const';
import {
	pull,
	pullMsgs,
	pushMsg,
	deleteMsg
} from './loader';
import { ordersStorePath } from '../Orders/loader';
import style from './style.scss';
import CartCard from '../../Cart/Card';
import OrderCard from '../Orders/Card';
import { prevent, plinkRoute, dateFormatter } from '../../../lib/util';
import Vab from '../../Material/VerticalActionBar';
import Printer from './Printer';
//------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------------------
export default class Order extends Component {
	storeConfig() {
		const { link } = this.props;

		this.orderStorePath = ordersStorePath + '.' + link;
		this.msgsStorePath = this.orderStorePath + '.msgs';

		return [
			{ path: 'auth', alias: 'auth', defaultValue: {} },
			{ path: kioskStorePath, alias: 'kiosk' },
			{ path: kioskSlipCopiesStorePath, alias: 'slipCopies' },
			{ path: kioskNameStorePath, alias: 'kioskName' },
			{ path: headerTitleStorePath, mount: 'Заказ' },
			{ path: headerSearchStorePath, mount: undefined },
			{ path: iconsLosslessStorePath, alias: 'iconsLossless' },
			{ path: iconsQualityStorePath, alias: 'iconsQuality' },
			{ path: this.orderStorePath, alias: 'data', defaultValue: { products: { rows: [] } } },
			{ path: this.msgsStorePath, alias: 'msgs', defaultValue: [] }
		];
	}

	willMount() {
		pull.call(this);
	}

	didMount() {
		clearInterval(this.pullMsgsIntervalId);
		if (!this.state.kiosk)
			this.pullMsgsIntervalId = setInterval(this.pullMsgs, 60 * 1000);
	}

	willUnmount() {
		clearInterval(this.pullMsgsIntervalId);
	}

	didSetState(keys) {
		const { auth } = this.state;

		if (!auth.authorized)
			route('/', true);
	}

	pullMsgs = () => this.__pullMsgs()
	__pullMsgs() {
		pullMsgs.call(this);
	}

	linkTo = path => this.__linkTo(path)
	__linkTo(path) {
		return { href: path, onClick: plinkRoute(path) };
	}

	goHome = this.linkTo('/')

	snackbarRef = e => this.__snackbarRef(e)
	__snackbarRef(e) {
		this.snackbar = e;
	}

	showError = e => this.__showError(e)
	__showError(e) {
		if (this.snackbar)
			this.snackbar.MDComponent.show({ message: e });
	}

	inputMsg = e => {
		this.setState({ msg: e.target.value });
		return prevent(e);
	}

	sendMsg = e => {
		if (this.state.msg)
			pushMsg.call(this, this.state.msg);
		return prevent(e);
	}

	deleteMsg = time => e => {
		deleteMsg.call(this, time);
		return prevent(e);
	}

	printerRef = e => this.printer = e

	print = e => {
		this.printer.print();
		return prevent(e);
	}

	render(props, state) {
		const { auth, data, msgs, kiosk, slipCopies, kioskName } = state;

		if (!data.link)
			return undefined;

		let n = 0;
		const msgsNodes = [];

		for (let i = msgs.length - 1; i >= 0; i--) {
			if (msgs[i] === undefined)
				continue;

			const { author, isAuthorEmployee, msg, time } = msgs[i];
			let m = msg.replace(/\r\n/g, '\n');
			m = m.replace(/\r/g, '\n');
			m = m.replace(/\n/g, '<br>');

			msgsNodes.push(
				<Elevation key={time} tag="div" z={2} className={style.el}>
					<Chips>
						<Chips.Chip>
							<Chips.Icon className="material-icons" leading>
								{isAuthorEmployee ? 'supervisor_account' : 'person'}
							</Chips.Icon>
							<Chips.Text>
								{isAuthorEmployee ? 'Сотрудник' : 'Покупатель'}
								&nbsp;
								{dateFormatter(new Date(time))}
							</Chips.Text>
							{auth.link === author ?
								<Chips.Icon
									className="material-icons"
									trailing tabindex="1" role="button"
									title="Delete"
									onClick={this.deleteMsg(time)}
								>
									delete
								</Chips.Icon> : undefined}
						</Chips.Chip>
					</Chips>
					<Typography body1
						dangerouslySetInnerHTML={{ __html: DOMPurify.sanitize(m) }}
					/>
				</Elevation>
			);
		}

		const printer = kiosk ? (
			<Printer ref={this.printerRef}
				order={data} data={data.products.rows}
				slipCopies={slipCopies} kioskName={kioskName}
			/>)
			: undefined;

		return (
			<div className={style.order}>
				<Snackbar ref={this.snackbarRef} class={style.snackbar} />
				<OrderCard {...data} className={style.orderCard} />
				<LayoutGrid>
					<LayoutGrid.Inner>
						{data.products.rows.map(row => row ? (
							<LayoutGrid.Cell>
								<CartCard {...row}
									n={++n}
									key={row.link}
									iconLossless={state.iconsLossless}
									iconQuality={state.iconsQuality}
								/>
							</LayoutGrid.Cell>) : undefined)}
					</LayoutGrid.Inner>
				</LayoutGrid>
				{kiosk ?
					<Vab fixed>
						<Vab.Fab {...this.goHome}>
							<Vab.Fab.Icon>
								home
							</Vab.Fab.Icon>
						</Vab.Fab>
						<Vab.Fab onClick={this.print}>
							<Vab.Fab.Icon>
								print
							</Vab.Fab.Icon>
						</Vab.Fab>
					</Vab>
					:
					<Elevation tag="div" z={2} className={style.el}>
						<TextField
							outerStyle={{ width: 'calc(100% - 48px - 1px)' }}
							className={style.msgInput1}
							autocomplete="off"
							textarea
							label="Введите текст сообщения ..."
							type="text"
							value={state.msg}
							onInput={this.inputMsg}
						/>
						<Icon className={style.msgIco}
							onClick={this.sendMsg}
						>
							send
						</Icon>
					</Elevation>}
				{msgsNodes}
				{printer}
			</div>);
	}
}
//------------------------------------------------------------------------------
