//------------------------------------------------------------------------------
import { burl, bfetch } from '../../../backend';
import { successor, failer, starter } from '../../load';
import { deepEqual } from '../../../lib/util';
import store from '../../../lib/rstore';
//------------------------------------------------------------------------------
export const ordersStorePath = 'orders';
export const ordersDataPath = `${ordersStorePath}.data`;
export const ordersCheckMarksPath = `${ordersStorePath}.checkmarks`;
//------------------------------------------------------------------------------
function pullOpts() {
	const { props, state } = this;
	const auth = state.auth ? state.auth : props.auth;
	return {
		auth,
		r: {
			m: 'orders',
			f: 'pull'
		},
		a: auth.link
	};
}
//------------------------------------------------------------------------------
function genPKey(result) {
	const { rows } = result;
	const pkey = {};

	for (const [i, { link }] of rows.entries())
		pkey[link] = i;

	result.pkey = pkey;
	result.pkl = rows.length;

	return result;
}
//------------------------------------------------------------------------------
export function pull() {
	const opts = pullOpts.call(this);

	return bfetch(
		opts,
		successor(result => {
			this.refreshUrls = {
				...this.refreshUrls,
				[opts.url]: result.maxAge
			};
			store.set(ordersDataPath, genPKey(result), 1, deepEqual);
		}),
		failer(error => this.showError(error.message)),
		starter()
	);
}
//------------------------------------------------------------------------------
function clearCommitedOrders(links) {
	const data = store.get(ordersDataPath);
	const { rows, pkey } = data;

	for (const link of links) {
		const i = pkey[link];
		const row = rows[i];

		if (!row || row.link !== link)
			continue;

		rows[i] = undefined;
		delete pkey[link];
		data.pkl--;
	}

	return data;
}
//------------------------------------------------------------------------------
function gatherCheckMarkedOrdersInfo() {
	const data = store.get(ordersDataPath);
	const { pkey } = data;
	const links = [], refreshUrls = { ...this.refreshUrls };
	let checkedCnt = 0, nzCnt = 0;

	for (const link of Object.keys(pkey)) {
		if (store.get(ordersCheckMarksPath + '.' + link)) {
			links.push(link);
			checkedCnt++;
		}
		nzCnt++;
	}

	return [refreshUrls, links, checkedCnt, nzCnt];
}
//------------------------------------------------------------------------------
export function clear() {
	if (this.clearCallId)
		return;

	const { state } = this;
	const opts = {
		auth: state.auth,
		method: 'PUT',
		r: {
			m: 'orders',
			f: 'clear'
		}
	};

	const [refreshUrls, links, checkedCnt, nzCnt] =
		gatherCheckMarkedOrdersInfo.call(this);

	if (checkedCnt === 0)
		return;

	if (checkedCnt <= nzCnt)
		opts.r.r = { links };

	opts.refreshUrls = refreshUrls;

	this.clearCallId = bfetch(
		opts,
		successor(result => {
			const data = clearCommitedOrders.call(this, links);

			for (const link of links)
				store.delete(ordersStorePath + '.' + link);

			store.replace(ordersDataPath, data);
			store.delete(ordersCheckMarksPath);

			delete this.clearCallId;
		}),
		failer(error => {
			delete this.clearCallId;
			this.showError(error.message);
		}),
		starter()
	);
}
//------------------------------------------------------------------------------
export function refreshUrls() {
	return { [burl(pullOpts.call(this))]: 60 };
}
//------------------------------------------------------------------------------
