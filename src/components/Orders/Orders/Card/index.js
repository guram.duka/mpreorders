//------------------------------------------------------------------------------
import Component from '../../../Component';
import Typography from 'preact-material-components/Typography';
import 'preact-material-components/Typography/style.css';
import Checkbox from 'preact-material-components/Checkbox';
import 'preact-material-components/Checkbox/style.css';
import TextField from 'preact-material-components/TextField';
import 'preact-material-components/TextField/style.css';
import FormField from 'preact-material-components/FormField';
import 'preact-material-components/FormField/style.css';
import Radio from 'preact-material-components/Radio';
import 'preact-material-components/Radio/style.css';
import 'preact-material-components/List/style.css';
import Icon from 'preact-material-components/Icon';
import 'preact-material-components/Icon/style.css';
import Elevation from '../../../Material/Elevation';
import { kioskStorePath } from '../../../../const';
import {
	prevent,
	plinkRoute,
	dateFormatter
} from '../../../../lib/util';
import root from '../../../../lib/root';
import { bfetch } from '../../../../backend';
import { successor, failer, starter } from '../../../load';
import { refreshUrls as ordersRefreshUrls } from '../loader';
import { refreshUrls as orderRefreshUrls } from '../../Order/loader';
import style from './style.scss';
//------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------------------
export default class Card extends Component {
	static __id = 0

	constructor() {
		super();

		this.idn = ++Card.__id;
		this.ids = `$__orders_cart__#${this.idn}`;
	}

	storeConfig() {
		const { link, checkMarksStorePath } = this.props;
		const config = [
			{ path: kioskStorePath, alias: 'kiosk' },
			{ path: 'auth', alias: 'auth', defaultValue: {} }
		];

		if (checkMarksStorePath)
			config.push(
				{ path: checkMarksStorePath + '.' + link, alias: 'checkMark' }
			);

		return config;
	}

	decodeProps(props) {
		const { link } = props;

		if (link)
			this.goOrder = this.linkTo('/orders/' + link);
	}

	linkTo = path => ({ href: path, onClick: plinkRoute(path) })

	customerEdit = e => {
		this.setState({ isCustomerEdit: true }, () => {
			const e = root.document.getElementById(this.ids);

			if (e)
				e.focus();
		});
		return prevent(e);
	}

	customerEditCancel = e => {
		this.setState({ isCustomerEdit: undefined });
		return prevent(e);
	}

	customerInput = e => {
		this.setState({ customerText: e.target.value }, this.customerSearch);
		return prevent(e);
	}

	customerSearch = () => {
		const { customerText } = this.state;

		if (!customerText || customerText.trim().length === 0) {
			this.setState({ customerSearchResults: [] });
			return;
		}

		bfetch(
			{
				auth: this.state.auth,
				r: {
					m: 'dict',
					f: 'search',
					r: {
						target: 'customers',
						filter: customerText
					}
				},
				a: true,
				e: true
			},
			successor(result => {
				if (this.state.customerText === customerText)
					this.setState({
						customerSearchResults: result.rows
					});
			}),
			failer(),
			starter()
		);
	}

	customerSelected = (order, link) => e => {
		bfetch(
			{
				auth: this.state.auth,
				method: 'PUT',
				refreshUrls: {
					...orderRefreshUrls.call(this),
					...ordersRefreshUrls.call(this)
				},
				r: {
					m: 'orders',
					f: 'push_customer',
					r: { order, link }
				}
			},
			successor(result => this.setState({
				customerLink: result.customerLink,
				customer: result.customer,
				customerInn: result.customerInn,
				isCustomerEdit: undefined,
				isCustomerChanging: undefined
			})),
			failer(error => this.setState({ isCustomerChanging: undefined })),
			starter(opts => this.setState({ isCustomerChanging: true }))
		);

		return prevent(e);
	}

	render(props, state) {
		const className = [
			props.className ? props.className : '',
			style.pr
		].join(' ');

		const {
			number,
			date,
			barcode,
			comment,
			totals,
			status,
			daddr
		} = props;

		let {
			customerLink,
			customer,
			customerInn
		} = props;

		if (state.customerLink)
			customerLink = state.customerLink;
		if (state.customer)
			customer = state.customer;
		if (state.customerInn)
			customerInn = state.customerInn;

		const name = `№ ${number} от ${dateFormatter(date)}, ${barcode}`;

		return (
			<div className={className}>
				<Elevation z={2} tag="div" className={style.el}>
					{props.n ?
						<Checkbox
							className={props.checkMarkClick ? style.cb : style.cbInvis}
							onChange={props.checkMarkClick}
							onClick={props.checkMarkClick}
							checked={state.checkMark}
						/> : undefined}
					<div className={style.titleBlock}>
						<a {...this.goOrder}>
							{props.n ?
								<Typography body1 className={style.n}>
									<strong>#{props.n}</strong>
									&nbsp;
								</Typography> : undefined}
							{name}
						</a>
						<div className={style.brc}>
							<Typography body2 className={style.sm}>
								Сумма: <strong>{`${totals}₽`}</strong>
							</Typography>
						</div>
						<div className={style.vt}>
							<Typography body2>
								Статус: <strong>{status}</strong>
							</Typography>
						</div>
					</div>
				</Elevation>
				{customerLink && !state.kiosk ?
					<Elevation z={1} tag="div" className={style.el}>
						<Typography body2>
							{`Контрагент: ${customer}, ИНН: ${customerInn}`}
						</Typography>
						<FormField className={state.isCustomerEdit ? style.ffe : ''}>
							{state.isCustomerEdit ?
								<TextField
									outerStyle={{ width: '100%' }}
									id={this.ids}
									autocomplete="off"
									label="Вводите текст ..."
									type="text"
									value={state.customerText}
									onInput={this.customerInput}
								/> : undefined}
							<label for={this.ids}>
								<Icon onClick={state.isCustomerEdit ? this.customerEditCancel : this.customerEdit}>
									{state.isCustomerEdit ? 'close' : 'edit'}
								</Icon>
							</label>
						</FormField>
						{state.isCustomerEdit && state.customerSearchResults ? state.customerSearchResults.map(v => {
							let n = v.fullName.trim();

							if (n.length === 0)
								n = v.name.trim();

							if (n.length === 0)
								n = v.inn.trim();

							return (
								<FormField key={v.link}>
									<Radio
										id={this.ids + v.link}
										disabled={state.isCustomerChanging}
										autocomplete="off"
										fullwidth
										checked={customerLink === v.link}
										value={v.link}
										name="customer"
										onChange={this.customerSelected(props.link, v.link)}
									/>
									<label for={this.ids + v.link}>
										{`${n}, ИНН: ${v.inn.trim()}`}
									</label>
								</FormField>);
						}) : undefined}
					</Elevation> : undefined}
				{comment ?
					<Elevation z={1} tag="div" className={style.el}>
						<Typography body2>
							{comment}
						</Typography>
					</Elevation> : undefined}
				{daddr ?
					<Elevation z={1} tag="div" className={style.el}>
						<Typography body2>
							{`Адрес доставки: ${daddr}`}
						</Typography>
					</Elevation> : undefined}
			</div>);
	}
}
//------------------------------------------------------------------------------
