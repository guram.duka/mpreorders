//------------------------------------------------------------------------------
import { route } from 'preact-router';
import Snackbar from 'preact-material-components/Snackbar';
import 'preact-material-components/Snackbar/style.css';
import LayoutGrid from 'preact-material-components/LayoutGrid';
import 'preact-material-components/LayoutGrid/style.css';
import 'preact-material-components/Typography/style.css';
import Component from '../../Component';
import store from '../../../lib/rstore';
import {
	headerTitleStorePath,
	headerSearchStorePath,
	kioskStorePath
} from '../../../const';
import { prevent, plinkRoute } from '../../../lib/util';
import {
	ordersDataPath,
	ordersCheckMarksPath,
	pull,
	clear
} from './loader';
import style from './style.scss';
import Card from './Card';
import Vab from '../../Material/VerticalActionBar';
//------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------------------
export default class Orders extends Component {
	storeConfig = [
		{ path: headerTitleStorePath, mount: 'Заказы' },
		{ path: headerSearchStorePath, mount: undefined },
		{ path: kioskStorePath, alias: 'kiosk' },
		{ path: 'auth', alias: 'auth', defaultValue: {} },
		{ path: ordersDataPath, alias: 'data', defaultValue: { rows: [] } },
		{ path: ordersCheckMarksPath, alias: 'checkMarks' }
	]

	willMount() {
		this.didSetState();
		pull.call(this);
	}

	didSetState() {
		const { auth } = this.state;

		if (!auth.authorized)
			route('/', true);
	}

	linkTo = path => this.__linkTo(path)
	__linkTo(path) {
		return { href: path, onClick: plinkRoute(path) };
	}

	goHome = this.linkTo('/')

	snackbarRef = e => this.__snackbarRef(e)
	__snackbarRef(e) {
		this.snackbar = e;
	}

	showError = e => this.__showError(e)
	__showError(e) {
		if (this.snackbar)
			this.snackbar.MDComponent.show({ message: e });
	}

	getOrder(link) {
		let { orders } = this;

		if (!orders)
			this.orders = orders = {};

		let order = orders[link];

		if (!order)
			this.orders[link] = order = {};

		return order;
	}

	handleCheckMarkClick = link => {
		const order = this.getOrder(link);
		let { cm } = order;

		if (!cm)
			order.cm = cm = e => {
				const p0 = ordersCheckMarksPath + '.' + link;
				const p1 = ordersCheckMarksPath + '.length';

				store.toggle(p0);

				if (store.get(p0))
					store.inc(p1);
				else {
					store.dec(p1);
					store.delete(p0);
				}

				let l = store.get(p1);

				if (!l)
					store.delete(p1);

				store.undef(ordersCheckMarksPath, l ? true : undefined);

				return prevent(e);
			};

		return cm;
	}

	setCheckMarks = e => this.__setCheckMarks(e)
	__setCheckMarks(e) {
		const { data } = this.state;

		store.delete(ordersCheckMarksPath);

		for (const row of data.rows)
			store.set(ordersCheckMarksPath + '.' + row.link, true);

		store.set(ordersCheckMarksPath + '.length', data.rows.length);
		store.set(ordersCheckMarksPath, true);

		return prevent(e);
	}

	resetCheckMarks = e => this.__resetCheckMarks(e)
	__resetCheckMarks(e) {
		store.delete(ordersCheckMarksPath);
		return prevent(e);
	}

	clearSelectedItems = e => {
		clear.call(this);
		return prevent(e);
	}

	render(props, state) {
		const { auth, data, kiosk } = state;

		if (!auth.authorized)
			return undefined;

		const vab = (
			<Vab fixed display={data.pkl}>
				{kiosk ?
					<Vab.Fab className={style.b} {...this.goHome}>
						<Vab.Fab.Icon>
							home
						</Vab.Fab.Icon>
					</Vab.Fab> : undefined}
				{state.checkMarks ?
					<Vab.Fab
						onClick={this.clearSelectedItems}
					>
						<Vab.Fab.Icon>
							delete
						</Vab.Fab.Icon>
					</Vab.Fab> : undefined}
				<Vab.Fab
					onClick={this.setCheckMarks}
				>
					<Vab.Fab.Icon>
						&#xE834;
					</Vab.Fab.Icon>
				</Vab.Fab>
				<Vab.Fab
					onClick={this.resetCheckMarks}
				>
					<Vab.Fab.Icon>
						&#xE835;
					</Vab.Fab.Icon>
				</Vab.Fab>
			</Vab>);

		let n = 0;

		return (
			<div class={style.orders}>
				<Snackbar ref={this.snackbarRef} class={style.snackbar} />
				<LayoutGrid>
					<LayoutGrid.Inner>
						{data.rows.map(row => row ? (
							<LayoutGrid.Cell {...this.cellEventsHandlers}>
								<Card {...row}
									n={++n}
									key={row.link}
									checkMarkClick={this.handleCheckMarkClick(row.link)}
									checkMarksStorePath={ordersCheckMarksPath}
								/>
							</LayoutGrid.Cell>) : undefined)}
					</LayoutGrid.Inner>
				</LayoutGrid>
				{vab}
			</div>
		);
	}
}
//------------------------------------------------------------------------------
