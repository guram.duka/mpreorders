﻿/*let re = new RegExp('^category\\.(.*)\\.order$', 'g');
re = /^category\.(.*)\.order$/g;
let s = 'category.1234.order';
console.log(re.exec(s));
console.log(s.replace(re, '$1'));
console.log(s.match(re));
console.log(s.search(re));
console.log(/^category\.(.*)\.order$/g);
console.log(new RegExp(re.source, re.flags));
console.log({ source: '1', flags: '2' });
s = 'searchOrderDirection';
console.log(s.replace(/([a-z\xE0-\xFF])([A-Z\xC0\xDF])/g, '$1.$2').toLowerCase());
s = [1,2,3];
console.log(s);
delete s[1];
console.log(JSON.stringify(s));
let obj = { first: 'Jane', last: 'Doe' };
let { first: f, last: l } = obj;
console.log(f, l);

s = [];

function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

while (s.length !== 10)
	s[s.length] = getRandomInt(0, 7);

console.log(s);
console.log(~~true, ~~false);
*/
/*let s = '/sw.js', r = /^\/(.*)(\.js|\.css)$/i;
console.log(s.search(r));
console.log(s.replace(r, '$1$2'));
s = '/sw1.css';
console.log(s.search(r));
console.log(s.replace(r, '$1$2'));*/
//------------------------------------------------------------------------------
// from https://github.com/aureooms/js-red-black-tree/
// modified by Guram Duka
//------------------------------------------------------------------------------
const RED = 0, BLACK = 1;
//------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------------------
/**
 * A black leaf node.
 *
 * @constructor
 * @param {Node} parent - The parent node in the tree.
 * @returns {Leaf}
 */
class Leaf {
	constructor(parent) {
		this.color = BLACK;
		this.parent = parent;
	}

	/**
     * Returns <code>true</code> if the <code>Leaf</code> object is a leaf. This
     * always returns <code>true</code>.
     *
     * @returns {Boolean}
     */
	isleaf() {
		return true;
	}
}
//------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------------------
/**
 * An internal node. This node can be red or black.
 *
 * @constructor
 * @param {Number} color - The color of the node.
 * @param {Key} key - The key of the node.
 * @returns {Node}
 */
class Node {
	constructor(color, key) {
		this.color = color;
		this.left = new Leaf(this);
		this.right = new Leaf(this);
		this.parent = null;
		this.key = key;
	}
	/**
     * Returns <code>true</code> if the <code>Node</code> object is a leaf. This
     * always returns <code>false</code>.
     *
     * @returns {Boolean}
     */
	isleaf() {
		return false;
	}
}
//------------------------------------------------------------------------------
/**
 * Computes the predecessor of the input node, in the subtree rooted at the
 * input node, when this predecessor is guaranteed to exist.
 *
 * @param {Node} node - The input node.
 * @returns {Node}
 */
function predecessor(node) {
	// assert( !node.left.isleaf() ) ;
	let pred = node.left;

	while (!pred.right.isleaf())
		pred = pred.right;

	return pred;
}
//------------------------------------------------------------------------------
/**
 * Walks the tree rooted at <code>A</code> down the only path that satisfies
 * the following property: if at a node <code>C</code> we make a left (resp.
 * right), then <code>B < C</code> (resp. <code>B >= C</code>). Once we hit the
 * end of the path, we can add node <code>B</code> at this position. By the
 * property of the path, the tree rooted at <code>A</code> is still a binary
 * search tree.
 * For our red-black tree, all that is left to do is fix the red-black tree
 * properties in case they have been violated by this insertion. This is fixed
 * by {@link insertCase1}.
 *
 * @param compare - The comparison function to use.
 * @param {Node} A - The root of the tree.
 * @param {Node} B - The node to insert.
 * @returns {Node} B - The node that has been inserted.
 */
function insert(compare, A, B) {
	for (; ;) {
		if (compare(B.key, A.key) < 0) {
			const node = A.left;

			if (node.isleaf()) {
				A.left = B;
				break;
			}

			A = node;
		}
		else {
			const node = A.right;

			if (node.isleaf()) {
				A.right = B;
				break;
			}

			A = node;
		}
	}

	B.parent = A;

	return B;
}
//------------------------------------------------------------------------------
/**
 * Computes the grandparent (parent of parent) of the input node.
 *
 * @param {Node} node - The input node.
 * @returns {Node}
 */
function grandparent(node) {
	// assert((node !== null) && (node.parent !== null));
	// We only call this function when node HAS a grandparent
	return node.parent.parent;
}
//------------------------------------------------------------------------------
/**
 * Computes the uncle of the input node when the grandparent is guaranteed to
 * exist.
 *
 * @param {Node} node - The input node.
 * @returns {Node}
 */
function uncle(node) {
	const g = grandparent(node);
	// assert( g !== null ) ;
	// this can never happen
	if (node.parent === g.left)
		return g.right.isleaf() ? null : g.right;

	return g.left.isleaf() ? null : g.left;
}
//------------------------------------------------------------------------------
/**
 * Rotate tree left. (see https://en.wikipedia.org/wiki/Tree_rotation)
 * /!\ This swaps the references to A and B.
 *
 *         A                B
 *        / \              / \
 *       a   B     ->     A   c
 *          / \          / \
 *         b   c        a   b
 *
 *
 * @param {Node} A - The root of the tree.
 *
 */
function rotateLeft(A) {
	const B = A.right;
	const a = A.left;
	const b = B.left;
	const c = B.right;

	[A.key, B.key] = [B.key, A.key];
	[A.color, B.color] = [B.color, A.color];

	A.left = B;
	A.right = c;

	B.left = a;
	B.right = b;

	a.parent = B;
	b.parent = B;
	c.parent = A;
}
//------------------------------------------------------------------------------
/**
 * Rotate tree right. (see https://en.wikipedia.org/wiki/Tree_rotation)
 * /!\ This swaps the references to A and B.
 *
 *         B                A
 *        / \              / \
 *       A   c     ->     a   B
 *      / \                  / \
 *     a   b                b   c
 *
 *
 * @param {Node} B - The root of the tree.
 *
 */
function rotateRight(B) {
	const A = B.left;
	const a = A.left;
	const b = A.right;
	const c = B.right;

	[A.key, B.key] = [B.key, A.key];
	[A.color, B.color] = [B.color, A.color];

	B.left = a;
	B.right = A;

	A.left = b;
	A.right = c;

	a.parent = B;
	b.parent = A;
	c.parent = A;
}
//------------------------------------------------------------------------------
/**
 * Preconditions:
 *   - n is red.
 *   - n is not the root of the tree.
 *   - n's parent is red.
 *   - n's uncle is black.
 *   - the path from n to its grandparent makes a left-left or right-right.
 *
 * @param {Node} n - The input node.
 */
function insertCase5(n) {
	const g = grandparent(n);

	// repaint n's parent black, n's grandparent red
	n.parent.color = BLACK;
	g.color = RED;

	/**
	 * If the path from g to n makes a left-left, {@link rotateRight} at g.
	 * We are done.
	 *
	 *             R                     B
	 *           /   \                 /   \
	 *         B       B            >R       R
	 *        / \     / \   -->     / \     / \
	 *      >R   =   -   -         =   =   =   B
	 *      / \                               / \
	 *     =   =                             -   -
	 */
	if (n === n.parent.left)
		rotateRight(g);
	/**
	 * If the path from g to n makes a right-right, {@link rotate_left} at g.
	 * We are done.
	 *
	 *             R                     B
	 *           /   \                 /   \
	 *         B       B             R      >R
	 *        / \     / \   -->     / \     / \
	 *       -   -   =  >R         B   =   =   =
	 *                  / \       / \
	 *                 =   =     -   -
	 */
	else
		rotateLeft(g);
}
//------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------------------
/**
 * Preconditions:
 *   - n is red.
 *   - n is not the root of the tree.
 *   - n's parent is red.
 *   - n's uncle is black.
 *
 * Here we fix the input subtree to pass the preconditions of {@link insertCase5}.
 *
 * @param {Node} n - The input node.
 */
function insertCase4(n) {
	const g = grandparent(n);

	/**
	 * If the path from g to n makes a left-right, change it to a left-left
	 * with {@link rotate_left}. Then call {@link insertCase5} on the old
	 * parent of n.
	 *
	 *             B                     B
	 *           /   \                 /   \
	 *         R       B             R       B
	 *        / \     / \   -->     / \     / \
	 *       =  >R   -   -        >R   =   -   -
	 *          / \               / \
	 *         =   =             =   =
	 */

	if ((n === n.parent.right) && (n.parent === g.left)) {
		rotateLeft(n.parent);

		/**
		 * rotate_left can be the below because of already having *g =  grandparent(n)
		 *
		 * saved_p=g.left, *saved_left_n=n.left;
		 * g.left=n;
		 * n.left=saved_p;
		 * saved_p.right=saved_left_n;
		 *
		 * and modify the parent's nodes properly
		 */

		// n = n.left; /!\ need to fix rotate, so that we can safely reference a node
	}

	/**
	 * If the path from g to n makes a right-left, change it to a right-right
	 * with {@link rotateRight}. Then call {@link insertCase5} on the old
	 * parent of n.
	 *
	 *             B                     B
	 *           /   \                 /   \
	 *         B       R             B       R
	 *        / \     / \   -->     / \     / \
	 *       -   -  >R   =         -   -   =  >R
	 *              / \                       / \
	 *             =   =                     =   =
	 */

	else if ((n === n.parent.left) && (n.parent === g.right)) {
		rotateRight(n.parent);

		/**
		 * rotateRight can be the below to take advantage of already having *g =  grandparent(n)
		 *
		 * saved_p=g.right, *saved_right_n=n.right;
		 * g.right=n;
		 * n.right=saved_p;
		 * saved_p.left=saved_right_n;
		 *
		 */

		// n = n.right ;
	}

	insertCase5(n);
}
//------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------------------
/**
 * Preconditions:
 *   - n is red.
 *   - n is not the root of the tree.
 *   - n's parent is red.
 *
 * @param {Node} n - The input node.
 */
function insertCase3(n) {
	const u = uncle(n);

	/**
	 * If n has a non-leaf uncle and this uncle is red then we simply
	 * repaint the parent and the uncle of n in black, the grandparent of
	 * n in red, then call insertCase1 on n's grandparent.
	 *
	 *             B                    >R
	 *           /   \                 /   \
	 *         R       R             B       B
	 *        / \     / \   -->     / \     / \
	 *      >R   -   -   -         R   -   -   -
	 *      / \                   / \
	 *     -   -                 -   -
	 */

	if ((u !== null) && (u.color === RED)) {
		n.parent.color = BLACK;
		u.color = BLACK;
		const g = grandparent(n);
		g.color = RED;
		insertCase1(g);
	}
	else
		insertCase4(n);
}
//------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------------------
/**
 * Preconditions:
 *   - n is red.
 *   - n is not the root of the tree.
 *
 * @param {Node} n - The input node.
 */
function insertCase2(n) {
	/**
	 * If the parent of n is black then we have nothing to do.
	 *
	 *         B
	 *        / \
	 *      >R   -
	 *      / \
	 *     -   -
	 */
	if (n.parent.color === BLACK)
		return;

	insertCase3(n);
}
//------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------------------
/**
 * Preconditions:
 *   - n is red.
 *
 * @param {Node} n - The input node.
 */
function insertCase1(n) {
	/**
	 * If n is the root of the tree, paint it black and we are done.
	 *
	 *      >R
	 *      / \
	 *     -   -
	 */
	if (n.parent === null)
		n.color = BLACK;
	else
		insertCase2(n);

}
//------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------------------
/**
 * Replaces node <code>A</code> by node <code>B</code>.
 *
 * @param {Node} A - The node to replace.
 * @param {Node} B - The replacement node.
 */
function replaceNode(A, B) {
	// assert( A.parent !== null ) ;
	// we never apply deleteOneChild on the root so we are safe

	if (A === A.parent.left)
		A.parent.left = B;
	else
		A.parent.right = B;

	B.parent = A.parent;
}
//------------------------------------------------------------------------------
/**
 * Computes the sibling of the input node.
 *
 * @param {Node} node - The input node.
 * @returns {Node}
 */
function sibling(node) {
	// assert((node !== null) && (node.parent !== null));
	// We only use this function when node HAS a sibling so this case can never
	// happen.
	return node === node.parent.left
		? node.parent.right
		: node.parent.left;
}
//------------------------------------------------------------------------------
/**
 * Preconditions:
 *   - n is black
 *   - all root-leaf paths going through n have a black height of b - 1
 *   - all other root-leaf paths have a black height of b
 *   - n is not the root
 *   - n's sibling is black
 *   - if n is a left child, the right child of n's sibling is red
 *   - if n is a right child, the left child of n's sibling is red
 *
 * @param {Node} n - The input node.
 */
function deleteCase6(n) {
	const s = sibling(n);

	/**
	 * Increment the black height of all root-leaf paths going through n by
	 * rotating at n's parent. This decrements the black height of all
	 * root-leaft paths going through n's sibling's right child.
     * We can repaint n's sibling's right child in black to fix this.
     * We are done.
	 *
	 *           ?                          ?
	 *        /     \                     /   \
	 *      >B        B                 B       B
	 *      / \      / \               / \     / \
	 *     -   -   =     R     -->   >B   =   =   B
	 *                  / \          / \         / \
	 *                 =   B        -   -       -   -
	 *                    / \
	 *                   -   -
	 */

	s.color = n.parent.color;
	n.parent.color = BLACK;

	if (n === n.parent.left) {
		s.right.color = BLACK;
		rotateLeft(n.parent);
	}
	// symmetric case
	else {
		s.left.color = BLACK;
		rotateRight(n.parent);
	}
}
//------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------------------
/**
 * Preconditions:
 *   - n is black
 *   - all root-leaf paths going through n have a black height of b - 1
 *   - all other root-leaf paths have a black height of b
 *   - n is not the root
 *   - n's sibling is black
 *   - at least one of n's sibling's children is red
 *
 * @param {Node} n - The input node.
 */
function deleteCase5(n) {
	const s = sibling(n);

	// The following statements just force the red n's sibling child to be on
	// the left of the left of the parent, or right of the right, so case 6
	// will rotate correctly.

	/**
	 *           ?                       ?
	 *         /   \                  /     \
	 *      >B       B              >B        B
	 *      / \     / \     -->     / \      / \
	 *     -   -  R     B          -   -   =     R
	 *           / \   / \                      / \
	 *          =   = -   -                    =   B
	 *                                            / \
	 *                                           -   -
	 */
	if ((n === n.parent.left) && (s.right.color === BLACK)) {
		s.color = RED;
		s.left.color = BLACK;
		rotateRight(s);
	}
	/**
	 *           ?                       ?
	 *         /   \                  /     \
	 *       B      >B               B       >B
	 *      / \     / \     -->     / \      / \
	 *    B     R  -   -          R     =   -   -
	 *   / \   / \               / \
	 *  -   - =   =             B   =
	 *                         / \
	 *                        -   -
	 */
	else if ((n === n.parent.right) && (s.left.color === BLACK)) {
		s.color = RED;
		s.right.color = BLACK;
		rotateLeft(s);
	}

	deleteCase6(n);
}
//------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------------------
/**
 * Preconditions:
 *   - n is black
 *   - all root-leaf paths going through n have a black height of b - 1
 *   - all other root-leaf paths have a black height of b
 *   - n is not the root
 *   - n's sibling is black
 *   - n's parent and n's sibling's children cannot all be black
 *
 * @param {Node} n - The input node.
 */
function deleteCase4(n) {
	const s = sibling(n);

	/**
	 * If n's parent is red and n's sibling's children are black, then swap n's
	 * parent and n's sibling color. All root-leaf paths going through n have
	 * now a black height of b. All other root-leaf paths have their black
	 * height unchanged. Red-black properties are respected. We are done.
	 *
	 *           R                       B
	 *         /   \                  /     \
	 *      >B       B              >B        R
	 *      / \     / \     -->     / \      / \
	 *     -   -  B     B          -   -   B     B
	 *           / \   / \                / \   / \
	 *          -   - -   -              -   - -   -
	 */
	// the parent color test is always true when coming from case 2
	if (
		(n.parent.color === RED) &&
		(s.left.color === BLACK) &&
		(s.right.color === BLACK)
	) {
		s.color = RED;
		n.parent.color = BLACK;
	}
	// Otherwise, go to case 5.
	else
		deleteCase5(n);
}
//------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------------------
/**
 * Preconditions:
 *   - n is black
 *   - all root-leaf paths going through n have a black height of b - 1
 *   - all other root-leaf paths have a black height of b
 *   - n is not the root
 *   - n's sibling is black
 *
 * @param {Node} n - The input node.
 */
function deleteCase3(n) {
	const s = sibling(n);

	/**
	 * If n's parent is black and n's sibling's children are black, then
     * repaint n's sibling red. Now all root-leaft paths going through n's
     * parent have a black height of b - 1. We recurse thus on n's parent.
	 *
	 *           B                      >B
	 *         /   \                  /     \
	 *      >B       B               B       R
	 *      / \     / \     -->    /   \    / \
	 *     -   -  B     B         -     - B     B
	 *           / \   / \               / \   / \
	 *          -   - -   -             -   - -   -
	 */
	if (
		(n.parent.color === BLACK) &&
		(s.left.color === BLACK) &&
		(s.right.color === BLACK)
	) {
		s.color = RED;
		deleteCase1(n.parent);
	}
	// Otherwise, go to case 4.
	else
		deleteCase4(n);
}
//------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------------------
/**
 * Preconditions:
 *   - n is black
 *   - all root-leaf paths going through n have a black height of b - 1
 *   - all other root-leaf paths have a black height of b
 *   - n is not the root
 *
 * @param {Node} n - The input node.
 */
function deleteCase2(n) {
	const s = sibling(n);

	/**
	 * If n's sibling is red, prepare for and go to case 4.
	 *
	 *           B                       B
	 *         /   \                  /     \
	 *      >B       R               R       B
	 *      / \     / \     -->    /   \    / \
	 *     -   -  B     B        >B     B  =   =
	 *           / \   / \       / \   / \
	 *          =   = =   =     -   - =   =
	 */
	if (s.color === RED) {
		n.parent.color = RED;
		s.color = BLACK;

		if (n === n.parent.left)
			rotateLeft(n.parent);
		else
			rotateRight(n.parent);

		deleteCase4(n);
	}
	// Otherwise, go to case 3.
	else
		deleteCase3(n);
}
//------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------------------
/**
 * Preconditions:
 *   - n is black
 *   - all root-leaf paths going through n have a black height of b - 1
 *   - all other root-leaf paths have a black height of b
 *
 * @param {Node} n - The input node.
 */
function deleteCase1(n) {
	// If n is the root, there is nothing to do: all paths go through n, and n
	// is black.
	if (n.parent !== null)
		deleteCase2(n);
}
//------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------------------
/**
 * Delete a node <code>n</code> that has at most a single non-leaf child.
 *
 * Precondition:
 *   - n has at most one non-leaf child.
 *   - if n has a non-leaf child, then it is its left child.
 *
 * @param {Node} n - The node to delete.
 */
function deleteOneChild(n) {
	// Precondition: n has at most one non-leaf child.
	// assert( n.right.isleaf() || n.left.isleaf());

	// const child = n.right.isleaf() ? n.left : n.right;
	// n.right is always a LEAF because either n is a subtree predecessor or it
	// is the only child of its parent by the red-black tree properties
	const child = n.left;

	// replace n with its left child
	replaceNode(n, child);

	// If n is black, deleting it reduces the black-height of every path going
	// through it by 1.
	if (n.color === BLACK) {

		// We can easily fix this when its left child is an
		// internal red node: change the color of the left child to black and
		// replace n with it.
		if (child.color === RED)
			child.color = BLACK;

		// Otherwise, there are more things to fix.
		else
			deleteCase1(child);
	}
	// else {
	//    If n is red then its child can only be black. Replacing n with its
	//    child suffices.
	// }
}
//------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------------------
/**
 * Search for the first node whose key equals <code>key</code>.
 *
 * @param {Function} compare - The comparison function.
 * @param {Node} root - The root of the tree to scan.
 * @param {Key} key - The key to search for.
 * @returns {Node}
 */
function search(compare, root, key) {
	for (; ;) {
		const d = compare(key, root.key);

		if (d === 0)
			return root;

		if (d < 0)
			root = root.left;
		else
			root = root.right;

		if (root.isleaf())
			return null;
	}
}
//------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------------------
/**
 * Traverses the tree rooted at <code>node</code> in order.
 *
 * @param {Node} node - The root of the tree.
 * @returns {Iterator}
 */
function* inordertraversal(node) {
	if (!node.left.isleaf()) {
		// Yield the nodes on the left recursively. Those nodes are all smaller
		// than (or equal to) the current node by the binary search tree
		// properties.
		yield* inordertraversal(node.left);
	}

	// Yield the current node.
	yield node.key;

	if (!node.right.isleaf()) {
		// Yield the nodes on the right recursively. Those nodes are all larger
		// than (or equal to) the current node by the binary search tree
		// properties.
		yield* inordertraversal(node.right);
	}
}
//------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------------------
/**
 * Yields all the keys in the tree rooted at <code>root</code> that lie in the
 * interval <code>[left, right[</code>, in order.
 *
 * @param {Function} compare - The comparison function.
 * @param {Node} root - The root of the tree.
 * @param {Key} left - The lower bound of the interval.
 * @param {Key} right - The upper bound of the interval.
 * @returns {Iterator}
 */
function* rangetraversal(compare, root, left, right) {
	if (compare(root.key, left) < 0) {
		// If the root lies to the left of the interval, we can discard the
		// entire left subtree.
		if (!root.right.isleaf())
			yield* rangetraversal(compare, root.right, left, right);
	}
	else if (compare(root.key, right) >= 0) {
		// If the root lies to the right of the interval, we can discard the
		// entire right subtree.
		if (!root.left.isleaf())
			yield* rangetraversal(compare, root.left, left, right);
	}
	else {
		// Otherwise just recurse on both subtrees and yield the root in
		// between.
		if (!root.left.isleaf())
			yield* rangetraversal(compare, root.left, left, right);

		yield root.key;

		if (!root.right.isleaf())
			yield* rangetraversal(compare, root.right, left, right);
	}
}
//------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------------------
/**
 * A RedBlackTree with key-only nodes.
 *
 */
class RedBlackTree {

	/**
	 * Constructs a new empty red-black tree.
	 *
	 * @param {Function} compare - The comparison function for node keys.
	 * @returns {RedBlackTree}
	 */
	constructor(compare) {
		if (compare !== undefined)
			this.compare = compare;

		this.root = null;
	}

	compare(a, b) {
		const c = a > b ? 1 : (a < b ? -1 : 0);

		if (c === undefined)
			throw Error('Invalid comparison');

		return c;
	}

	/**
	 * Adds a key to the tree.
	 *
	 * @param {Key} key - The key to add.
	 */
	add(key) {
		if (this.root === null) {
			this.root = new Node(BLACK, key);
		}
		else {
			const node = new Node(RED, key);
			insert(this.compare, this.root, node);
			insertCase2(node);
		}
	}

	/**
	 * Search for the input key in the tree.
	 * Returns the first node whose key equals the input key.
	 * If no such node exists, returns <code>null</code>.
	 *
	 * @param {Key} key - The input key.
	 * @returns {Node}
	 */
	_search(key) {
		if (this.root === null)
			return null;

		return search(this.compare, this.root, key);
	}

	/**
	 * Search for the input key in the tree. Returns the first node key found
	 * in this way (with {@link RedBlackTree#_search}. If no such key exists
	 * in the tree, returns <code>null</code>.
	 *
	 * @param {Key} key - The input key.
	 * @returns {Key}
	 */
	get(key) {
		const node = this._search(key);
		return node === null ? null : node.key;
	}

	/**
	 * Returns <code>true</code> if and only if the tree contains the input
	 * key.
	 *
	 * @param {Key} key - The input key.
	 * @returns {Boolean}
	 */
	has(key) {
		return this._search(key) !== null;
	}

	/**
	 * Deletes the input node from the tree.
	 *
	 * @param {Node} node - The input node to delete.
	 */
	_delete(node) {
		if (!node.left.isleaf()) {
			// replace node's key with predecessor's key
			const pred = predecessor(node);
			node.key = pred.key;
			// delete predecessor node
			// note: this node can only have one non-leaf child
			//       because the tree is a red-black tree
			deleteOneChild(pred);
		}
		else if (!node.right.isleaf()) {
			// replace node's key with successor's key
			// If there is no left child, then there can only be one right
			// child.
			const succ = node.right;
			node.key = succ.key;
			// delete successor node
			// note: this node can only have one non-leaf child
			//       because the tree is a red-black tree
			deleteOneChild(succ);
		}
		else if (node === this.root) {
			this.root = null;
		}
		else {
			deleteOneChild(node);
		}
	}

	/**
	 * Search for the first node of the tree whose key equals the input key
	 * (with {@link RedBlackTree#_search}), then delete that node
	 * (with {@link RedBlackTree#_delete}). If such a node is found and deleted
	 * then return <code>true</code>. Return <code>false</code> otherwise.
	 *
	 * @param {Key} key - The input key.
	 * @returns {Boolean} - Whether the key existed in the tree before removal.
	 */
	remove(key) {
		const node = this._search(key);

		if (node === null)
			return false;

		this._delete(node);

		return true;
	}

	/**
	 * Returns an in order iterator over the keys of the tree that lie in the
	 * interval [left, right[.
	 * @param {Key} left - The left bound of the interval.
	 * @param {Key} right - The right bound of the interval.
	 * @returns {Iterator}
	 */
	*range(left, right) {
		if (this.root !== null)
			yield* rangetraversal(this.compare, this.root, left, right);
	}

	/**
	 * Returns an in order iterator over the keys of the tree.
	 *
	 * @returns {Iterator}
	 */
	*items() {
		if (this.root !== null)
			yield* inordertraversal(this.root);
	}

	/**
	 * Same as {@link RedBlackTree#items}.
	 */
	[Symbol.iterator]() {
		return this.items();
	}

	/**
	 * Constructs a red-black tree from an input iterable.
	 *
	 * @param {Function} compare - The comparison function to use.
	 * @param {Iterbale} iterable - The input iterable.
	 * @returns {RedBlackTree}
	 */
	static from(compare, iterable) {
		const tree = new RedBlackTree(compare);

		for (const element of iterable)
			tree.add(element);

		return tree;
	}
}
//------------------------------------------------------------------------------
function sprintf () {
  var regex = /%%|%(\d+\$)?([\-+'#0 ]*)(\*\d+\$|\*|\d+)?(?:\.(\*\d+\$|\*|\d+))?([scboxXuideEfFgG])/g
  var a = arguments
  var i = 0
  var format = a[i++]

  var _pad = function (str, len, chr, leftJustify) {
    if (!chr) {
      chr = ' '
    }
    var padding = (str.length >= len) ? '' : new Array(1 + len - str.length >>> 0).join(chr)
    return leftJustify ? str + padding : padding + str
  }

  var justify = function (value, prefix, leftJustify, minWidth, zeroPad, customPadChar) {
    var diff = minWidth - value.length
    if (diff > 0) {
      if (leftJustify || !zeroPad) {
        value = _pad(value, minWidth, customPadChar, leftJustify)
      } else {
        value = [
          value.slice(0, prefix.length),
          _pad('', diff, '0', true),
          value.slice(prefix.length)
        ].join('')
      }
    }
    return value
  }

  var _formatBaseX = function (value, base, prefix, leftJustify, minWidth, precision, zeroPad) {
    // Note: casts negative numbers to positive ones
    var number = value >>> 0
    prefix = (prefix && number && {
      '2': '0b',
      '8': '0',
      '16': '0x'
    }[base]) || ''
    value = prefix + _pad(number.toString(base), precision || 0, '0', false)
    return justify(value, prefix, leftJustify, minWidth, zeroPad)
  }

  // _formatString()
  var _formatString = function (value, leftJustify, minWidth, precision, zeroPad, customPadChar) {
    if (precision !== null && precision !== undefined) {
      value = value.slice(0, precision)
    }
    return justify(value, '', leftJustify, minWidth, zeroPad, customPadChar)
  }

  // doFormat()
  var doFormat = function (substring, valueIndex, flags, minWidth, precision, type) {
    var number, prefix, method, textTransform, value

    if (substring === '%%') {
      return '%'
    }

    // parse flags
    var leftJustify = false
    var positivePrefix = ''
    var zeroPad = false
    var prefixBaseX = false
    var customPadChar = ' '
    var flagsl = flags.length
    var j
    for (j = 0; j < flagsl; j++) {
      switch( flags.charAt(j) ) {
        case ' ':
          positivePrefix = ' '
          break
        case '+':
          positivePrefix = '+'
          break
        case '-':
          leftJustify = true
          break
        case "'":
          customPadChar = flags.charAt(j + 1)
          break
        case '0':
          zeroPad = true
          customPadChar = '0'
          break
        case '#':
          prefixBaseX = true
          break
      }
    }

    // parameters may be null, undefined, empty-string or real valued
    // we want to ignore null, undefined and empty-string values
    if (!minWidth) {
      minWidth = 0
    } else if (minWidth === '*') {
      minWidth = +a[i++]
    } else if (minWidth.charAt(0) === '*') {
      minWidth = +a[minWidth.slice(1, -1)]
    } else {
      minWidth = +minWidth
    }

    // Note: undocumented perl feature:
    if (minWidth < 0) {
      minWidth = -minWidth
      leftJustify = true
    }

    if (!isFinite(minWidth)) {
      throw new Error('sprintf: (minimum-)width must be finite')
    }

    if (!precision) {
      precision = 'fFeE'.indexOf(type) > -1 ? 6 : (type === 'd') ? 0 : undefined
    } else if (precision === '*') {
      precision = +a[i++]
    } else if (precision.charAt(0) === '*') {
      precision = +a[precision.slice(1, -1)]
    } else {
      precision = +precision
    }

    // grab value using valueIndex if required?
    value = valueIndex ? a[valueIndex.slice(0, -1)] : a[i++]

    switch (type) {
      case 's':
        return _formatString(value + '', leftJustify, minWidth, precision, zeroPad, customPadChar)
      case 'c':
        return _formatString(String.fromCharCode(+value), leftJustify, minWidth, precision, zeroPad)
      case 'b':
        return _formatBaseX(value, 2, prefixBaseX, leftJustify, minWidth, precision, zeroPad)
      case 'o':
        return _formatBaseX(value, 8, prefixBaseX, leftJustify, minWidth, precision, zeroPad)
      case 'x':
        return _formatBaseX(value, 16, prefixBaseX, leftJustify, minWidth, precision, zeroPad)
      case 'X':
        return _formatBaseX(value, 16, prefixBaseX, leftJustify, minWidth, precision, zeroPad)
        .toUpperCase()
      case 'u':
        return _formatBaseX(value, 10, prefixBaseX, leftJustify, minWidth, precision, zeroPad)
      case 'i':
      case 'd':
        number = +value || 0
        // Plain Math.round doesn't just truncate
        number = Math.round(number - number % 1)
        prefix = number < 0 ? '-' : positivePrefix
        value = prefix + _pad(String(Math.abs(number)), precision, '0', false)
        return justify(value, prefix, leftJustify, minWidth, zeroPad)
      case 'e':
      case 'E':
      case 'f': // @todo: Should handle locales (as per setlocale)
      case 'F':
      case 'g':
      case 'G':
        number = +value
        prefix = number < 0 ? '-' : positivePrefix
        method = ['toExponential', 'toFixed', 'toPrecision']['efg'.indexOf(type.toLowerCase())]
        textTransform = ['toString', 'toUpperCase']['eEfFgG'.indexOf(type) % 2]
        value = prefix + Math.abs(number)[method](precision)
        return justify(value, prefix, leftJustify, minWidth, zeroPad)[textTransform]()
      default:
        return substring
    }
  }

  return format.replace(regex, doFormat)
}
//------------------------------------------------------------------------------
const msp = 1000000; // timer precision
//------------------------------------------------------------------------------
function ms() {
	//return (new Date).getTime();
	// don't forget set privacy.reduceTimerPrecision to false on about:config page
	const t = performance.now();
	const d = Math.trunc(t);
	const micro = d * 1000;
	const delta = t - d;
	const submicro = delta * 1000;
	return Math.trunc(micro + submicro);
}
//------------------------------------------------------------------------------
function ellapsedString(ms) {

	if( ms < 0 )
		ms = 0;

	let a		= Math.trunc(ms / msp);
	let days	= Math.trunc(a / (60 * 60 * 24));
	let hours	= Math.trunc(a / (60 * 60)) - days * 24;
	let mins	= Math.trunc(a / 60) - days * 24 * 60 - hours * 60;
	let secs	= a - days * 24 * 60 * 60 - hours * 60 * 60 - mins * 60;
	let msecs	= ms % msp;
	let s;

	if( days !== 0 )
		s = sprintf('%u:%02u:%02u:%02u.%06u', days, hours, mins, secs, msecs);
	else if( hours !== 0 )
		s = sprintf('%u:%02u:%02u.%06u', hours, mins, secs, msecs);
	else if( mins !== 0 )
		s = sprintf('%u:%02u.%06u', mins, secs, msecs);
	else if( secs !== 0 )
		s = sprintf('%u.%06u', secs, msecs);
	else
		s = sprintf('.%06u', msecs);

	return s;

}
//------------------------------------------------------------------------------
function consoleLog(...args) {
	let e = document.getElementById('console');

	if (!e) {
		e = document.createElement('pre');
		e.id = 'console';
		e.classList.add('console');
		document.body.appendChild(e);
	}

	e.innerHTML += args.join(' ') + '<br>';
}
//------------------------------------------------------------------------------
function randomInteger(min = 0, max = Number.MAX_SAFE_INTEGER) {
	return Math.floor(Math.random() * (max - min + 1)) + min;
}
//------------------------------------------------------------------------------
const benchItems = [];
//------------------------------------------------------------------------------
function doSetItems() {
	for (let i = 0; i < benchItems.length; i++)
		localStorage.setItem(`k${i}`, JSON.stringify(benchItems[i]));
}
//------------------------------------------------------------------------------
function doGetItems() {
	for (let i = 0; i < benchItems.length; i++)
		benchItems[i] = localStorage.getItem(`k${i}`);
}
//------------------------------------------------------------------------------
function doRemoveItems() {
	for (let i = 0; i < benchItems.length; i++)
		localStorage.removeItem(`k${i}`);
}
//------------------------------------------------------------------------------
/*function doSetItems() {
	// set stuff
	localStorage.setItem('mykey', JSON.stringify(data))
	// get stuff
	const data = JSON.parse(localStorage.getItem('mykey') || '{}')
}*/
//------------------------------------------------------------------------------
const benchTreeItems = [];
const benchTreeKeys = [];
const tree = new RedBlackTree();
//------------------------------------------------------------------------------
function doSeqInsertTreeItems() {
	for (let i = 0; i < benchTreeItems.length; i++)
		tree.add(i, benchTreeItems[i]);
}
//------------------------------------------------------------------------------
function doSeqRemoveTreeItems() {
	for (let i = 0; i < benchTreeItems.length; i++)
		tree.remove(i, benchTreeItems[i]);
}
//------------------------------------------------------------------------------
function doRndInsertTreeItems() {
	for (let i = 0; i < benchTreeItems.length; i++)
		tree.add(benchTreeKeys[i], benchTreeItems[i]);
}
//------------------------------------------------------------------------------
function doRndRemoveTreeItems() {
	for (let i = 0; i < benchTreeItems.length; i++)
		tree.remove(benchTreeKeys[i], benchTreeItems[i]);
}
//------------------------------------------------------------------------------
function rerun() {
	let e = document.getElementById('rerunb');

	if (!e) {
		e = document.createElement('input');
		e.id = 'rerunb';
		e.type = 'button';
		e.value = 'Re-run';
		e.onclick = rerun;
		document.body.appendChild(e);
	}

	const cacheId = '$__image_cache__#';
	let cache = document.getElementById(cacheId);

	if (!cache) {
		cache = document.createElement('style');
		cache.id = cacheId;
		document.head.appendChild(cache);
		cache.sheet.insertRule(
			`.console {
				line-height: 1.0;
			}`);
	}

	if (benchItems.length === 0)
		for (let i = 0; i < 1000; i++)
			benchItems.push(`#${randomInteger()}#${randomInteger()}#${randomInteger()}#${randomInteger()}#${randomInteger()}#${randomInteger()}$`);

	localStorage.clear();

	let start = ms();
	doSetItems();
	let stop = ms();
	let ellapsed = stop - start;
	let ops = (benchItems.length / ellapsed) * msp;
	consoleLog(`setItems: ${ellapsedString(ellapsed)}ms, ${sprintf('%.4f', ops)} ops`);
	
	start = ms();
	doGetItems();
	stop = ms();
	ellapsed = stop - start;
	ops = (benchItems.length / ellapsed) * msp;
	consoleLog(`getItems: ${ellapsedString(ellapsed)}ms, ${sprintf('%.4f', ops)} ops`);

	start = ms();
	doRemoveItems();
	stop = ms();
	ellapsed = stop - start;
	ops = (benchItems.length / ellapsed) * msp;
	consoleLog(`removeItems: ${ellapsedString(ellapsed)}ms, ${sprintf('%.4f', ops)} ops`);

	// In the following line, you should include the prefixes of implementations you want to test.
	window.indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
	// DON'T use "var indexedDB = ..." if you're not in a function.
	// Moreover, you may need references to some window.IDB* objects:
	window.IDBTransaction = window.IDBTransaction || window.webkitIDBTransaction || window.msIDBTransaction
		|| {READ_WRITE: "readwrite"}; // This line should only be needed if it is needed to support the object's constants for older browsers
	window.IDBKeyRange = window.IDBKeyRange || window.webkitIDBKeyRange || window.msIDBKeyRange;
	// (Mozilla has never prefixed these objects, so we don't need window.mozIDB*)
	if (!window.indexedDB)
    	window.alert('Your browser doesn\'t support a stable version of IndexedDB. Such and such feature will not be available.');

	//let db;
	let request = window.indexedDB.open('MyTestDatabase', 1);
	request.onerror = function(event) {
		alert('Why didn\'t you allow my web app to use IndexedDB?!');
	};

	request.onsuccess = function(event) {
		let db = event.target.result;
		db.onerror = function(event) {
			// Generic error handler for all errors targeted at this database's
			// requests!
			alert('Database error: ' + event.target.errorCode);
		};
	};

	// This is what our customer data looks like.
	const customerData = [
		{ ssn: '444-44-4444', name: 'Bill', age: 35, email: 'bill@company.com' },
		{ ssn: '555-55-5555', name: 'Donna', age: 32, email: 'donna@home.org' }
	];

	request.onupgradeneeded = function(event) {
		let db = event.target.result;

		if (event.oldVersion < 1) {
			// Version 1 is the first version of the database.

			// Create an objectStore to hold information about our customers. We're
			// going to use 'ssn' as our key path because it's guaranteed to be
			// unique - or at least that's what I was told during the kickoff meeting.
			let objectStore = db.createObjectStore('customers', { keyPath: 'ssn', autoIncrement : true });

			// Create an index to search customers by name. We may have duplicates
			// so we can't use a unique index.
			objectStore.createIndex('name', 'name', { unique: false });

			// Create an index to search customers by email. We want to ensure that
			// no two customers have the same email, so use a unique index.
			objectStore.createIndex('email', 'email', { unique: true });

			// Use transaction oncomplete to make sure the objectStore creation is 
			// finished before adding data into it.
			objectStore.transaction.oncomplete = function(event) {
				// Store values in the newly created objectStore.
				let customerObjectStore = db.transaction('customers', 'readwrite').objectStore('customers');
				customerData.forEach(function(customer) {
					customerObjectStore.add(customer);
				});
			};

			let object = db.createObjectStore('keysTree', { keyPath: 'id', autoIncrement : true });
			object.createIndex('name', 'name', { unique: false });
			object.createIndex('parent', 'parent', { unique: false });

			object = db.createObjectStore('keysValues', { keyPath: 'id', autoIncrement : false });
		}
	};

	const l = 500000;

	if (benchTreeItems.length === 0)
		for (let i = 0; i < l; i++) {
			benchTreeItems.push(`#${randomInteger()}$`);
			benchTreeKeys.push(i);
		}

	// shuffle
	for (let i = 0; i < l; i++) {
		const i1 = randomInteger(0, l - 1), i2 = randomInteger(0, l - 1);
		const v = benchTreeKeys[i1];
		benchTreeKeys[i1] = benchTreeKeys[i2];
		benchTreeKeys[i2] = v;
	}

	start = ms();
	doSeqInsertTreeItems();
	stop = ms();
	ellapsed = stop - start;
	ops = (benchTreeItems.length / ellapsed) * msp;
	consoleLog(`tree.seq.insert: ${ellapsedString(ellapsed)}ms, ${sprintf('%.4f', ops)} ops`);

	start = ms();
	doSeqRemoveTreeItems();
	stop = ms();
	ellapsed = stop - start;
	ops = (benchTreeItems.length / ellapsed) * msp;
	consoleLog(`tree.seq.remove: ${ellapsedString(ellapsed)}ms, ${sprintf('%.4f', ops)} ops`);

	start = ms();
	doRndInsertTreeItems();
	stop = ms();
	ellapsed = stop - start;
	ops = (benchTreeItems.length / ellapsed) * msp;
	consoleLog(`tree.rnd.insert: ${ellapsedString(ellapsed)}ms, ${sprintf('%.4f', ops)} ops`);

	start = ms();
	doRndRemoveTreeItems();
	stop = ms();
	ellapsed = stop - start;
	ops = (benchTreeItems.length / ellapsed) * msp;
	consoleLog(`tree.rnd.remove: ${ellapsedString(ellapsed)}ms, ${sprintf('%.4f', ops)} ops`);

}
//------------------------------------------------------------------------------
document.addEventListener('DOMContentLoaded', rerun);
//------------------------------------------------------------------------------
